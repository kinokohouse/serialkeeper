//
//  AppDelegate.h
//  Serialkeeper Relauncher
//
//  Created by Petros Loukareas on 19/01/2022.
//  Copyright © 2022 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SRAppDelegate : NSObject <NSApplicationDelegate>


@end

