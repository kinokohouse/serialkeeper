//
//  main.m
//  Serialkeeper Relauncher
//
//  Created by Petros Loukareas on 19/01/2022.
//  Copyright © 2022 Petros Loukareas. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
