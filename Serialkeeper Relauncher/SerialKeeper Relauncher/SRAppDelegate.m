//
//  AppDelegate.m
//  Serialkeeper Relauncher
//
//  Created by Petros Loukareas on 19/01/2022.
//  Copyright © 2022 Kinoko House. All rights reserved.
//

#import "SRAppDelegate.h"

@interface SRAppDelegate ()

@property (strong) NSTimer *timer;

@end


@implementation SRAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    sleep(1);
    NSArray *appArray = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"nl.kinoko-house.SerialKeeper"];
    if ([appArray count] > 0) {
        for (NSRunningApplication *app in appArray) {
            [app terminate];
        }
    }
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.25f target:self selector:@selector(checkAppTermination) userInfo:nil repeats:YES];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    
}

- (void)checkAppTermination {
    NSArray *appArray = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"nl.kinoko-house.SerialKeeper"];
    if ([appArray count] == 0) {
        NSString *path = [[[[[NSBundle mainBundle] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
        [[NSWorkspace sharedWorkspace] launchApplication:path];
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
        [NSApp terminate:nil];
    }
}


@end
