//
//  SKOutlineViewController.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 28-12-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "SKOutlineViewController.h"
#import "AppDelegate.h"

@interface SKOutlineViewController()

@property __unsafe_unretained IBOutlet SKTableViewController *tableViewController;

@end

@implementation SKOutlineViewController

- (id)init {
    _savedUUID = nil;
    _shakeList = @[@-10,@20,@-20,@20,@-20,@20,@-20,@10];
    _shakeCounter = 0;
    _shakeTimer = nil;
    _initDeferred = NO;
    _timingInterval = 0.05f;
    if (self = [super init]) {
        _firstOpen = YES;
        _firstSave = YES;
        _nonselect = NO;
        [self prepareAppSupportFolder];
    }
    return self;
}


#pragma mark NSOutlineView Data Source Methods

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
    if (!item) {
        return YES;
    } else {
        if ([[item class] isEqualTo:[SKApplication class]]) {
            return NO;
        } else {
            return YES;
        }
    }
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
    if (!item) {
        if (!_searchActive) {
            return [_softlist count];
        } else {
            return [_filteredlist count];
        }
    } else {
        if ([[item class] isEqualTo:[SKCategory class]]) {
            return [[(SKCategory *)item applications] count];
        } else {
            return 0;
        }
    }
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
    if (!item) {
        if (!_searchActive) {
            return [_softlist objectAtIndex:index];
        } else {
            return [_filteredlist objectAtIndex:index];
        }
    } else {
        if ([[item class] isEqualTo:[SKApplication class]]) {
            return nil;
        } else {
            return [[(SKCategory *)item applications] objectAtIndex:index];
        }
    }
    return self;
}


#pragma mark NSOutlineView Delegate Methods

- (id)outlineView:(NSOutlineView *)outlineView viewForTableColumn:(nullable NSTableColumn *)tableColumn item:(nonnull id)item {
    if ([[item class] isEqualTo:[SKCategory class]]) {
        SKCategoryCellView *categoryCell = [outlineView makeViewWithIdentifier:@"categoryCell" owner:self];
        [[categoryCell imageView] setWantsLayer:NO];
        [[categoryCell imageView] setImage:[[NSWorkspace sharedWorkspace] iconForFileType:NSFileTypeForHFSTypeCode(kGenericFolderIcon)]];
        [[categoryCell textField] setStringValue:[(SKCategory *)item category]];
        [[categoryCell countButton] setTitle:[NSString stringWithFormat:@"%lu",[[(SKCategory *)item applications] count]]];
        return categoryCell;
    } else {
        NSTableCellView *applicationCell = [outlineView makeViewWithIdentifier:@"applicationCell" owner:self];
        [[applicationCell imageView] setWantsLayer:NO];
        if (@available (macOS 10.9, *)) {
            [[applicationCell imageView] setImage:[[NSImage alloc] initWithData:[(SKApplication *)item icon]]];
        } else {
            NSSize iconSize = NSMakeSize(32, 32);
            NSImage *oldIcon = [[NSImage alloc] initWithData:[(SKApplication *)item icon]];
            NSImage *newIcon = [[NSImage alloc] initWithSize:iconSize];
            [newIcon lockFocus];
            [oldIcon setSize:iconSize];
            [[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
            [oldIcon drawAtPoint:NSZeroPoint fromRect:CGRectMake(0, 0, iconSize.width, iconSize.height) operation:NSCompositeCopy fraction:1.0];
            [newIcon unlockFocus];
            [[applicationCell imageView] setImage:newIcon];
        }
        [[applicationCell textField] setStringValue:[(SKApplication *)item product]];
        return applicationCell;
    }
}

- (void)outlineViewSelectionDidChange:(NSNotification *)notification {
    [self displayCurrentSelection];
    if ([[_outlineView itemAtRow:[_outlineView selectedRow]] class] == [SKApplication class]) {
        [_duplicateMenuItem setEnabled:YES];
        [_changeIconMenuItem setEnabled:YES];
        [_printSetupMenuItem setEnabled:YES];
        [_printMenuItem setEnabled:YES];
    } else {
        [_duplicateMenuItem setEnabled:NO];
        [_changeIconMenuItem setEnabled:NO];
        [_printSetupMenuItem setEnabled:NO];
        [_printMenuItem setEnabled:NO];
    }
    [self storeUUIDOfLastSelectedItem];
    _changeDetected = NO;
}

- (BOOL)selectionShouldChangeInOutlineView:(NSOutlineView *)outlineView {
    return [self updateIfChanged];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldCollapseItem:(id)item {
    return YES;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    return !_searchActive;
}

- (BOOL)updateIfChanged {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSInteger theIndex;
    NSInteger theParentIndex;
    if (!_changeDetected) return YES;
    id theObject = [self getCurrentlySelectedItem];
    if ([[theObject class] isEqual:[SKCategory class]]) {
        if (!_searchActive) {
            theIndex = [self getIndexOfCurrentlySelectedItem];
        } else {
            if ([theObject category] == nil) return NO;
            theIndex = [[self getCurrentlySelectedItem] originalIndex];
        }
        if (theIndex == NSNotFound) return NO;
        [[_softlist objectAtIndex:theIndex] setCategory:[[_categoryName stringValue] copy]];
        [[_softlist objectAtIndex:theIndex] setDescript:[[_categoryDescription string] copy]];
        [self sortCategoriesOnly];
        if (_searchActive) [self buildFilteredSoftwareList];
    } else {
        if (!_searchActive) {
            theIndex = [self getIndexOfCurrentlySelectedItem];
            theParentIndex = [self getIndexOfParentOfCurrentlySelectedItem];
        } else {
            if ([theObject product] == nil) return NO;
            theIndex = [theObject originalIndex];
            theParentIndex = [theObject parentIndex];
        }
        if (theIndex == NSNotFound || theParentIndex == NSNotFound) return NO;
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setProduct:[[_applicationProduct stringValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setVersion:[[_applicationVersion stringValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setDescript:[[_applicationDescription string] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setRegname:[[_applicationRegName stringValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setPurchasedate:[[_applicationPurchaseDate dateValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setRegistration:[[_applicationRegkey stringValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setEmail:[[_applicationEMail stringValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setSerial:[[_applicationSerial stringValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setCompany:[[_applicationCompany stringValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setWebsite:[[_applicationWebsite stringValue] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setNotes:[[_applicationNotes string] copy]];
        [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setAttachments:[[_tableViewController attachmentList] mutableCopy]];

        if (!_searchActive) {
            [[_outlineView parentForItem:theObject] sortApplications];
        } else {
            [[_softlist objectAtIndex:theParentIndex] sortApplications];
            [self buildFilteredSoftwareList];
        }
        if (!_searchActive) {
            [_outlineView reloadData];
        }
        [_outlineView scrollRowToVisible:[_outlineView selectedRow]];
        if ([appDelegate saveAfterEveryChange]) {
            [self saveDatabase];
        }
    }
    _changeDetected = NO;
    return YES;
}


#pragma mark NSOpenSavePanel Delegate Methods

- (BOOL)panel:(id)sender shouldEnableURL:(nonnull NSURL *)url {
    if (!_allowedExtensions) return YES;
    NSString *extension = [[url absoluteString] pathExtension];
    for (int i = 0; i < [_allowedExtensions count]; i++) {
        if ([extension isEqualToString:@""] || extension == nil) return YES;
        if ([extension caseInsensitiveCompare:[_allowedExtensions objectAtIndex:i]] == NSOrderedSame) return YES;
    }
    return NO;
}


#pragma mark NSTextFieldDelegate Delegate Methods

- (void)controlTextDidChange:(NSNotification *)notification {
    _changeDetected = YES;
}

- (void)controlTextDidEndEditing:(NSNotification *)notification {
    if (_changeDetected) {
        [self storeUUIDOfLastSelectedItem];
        [self updateIfChanged];
        [self reselectOriginalItem];
    }
}


#pragma mark NSTextViewDelegate Delegate Methods

- (void)textDidChange:(NSNotification *)notification {
    _changeDetected = YES;
}

- (void)textDidEndEditing:(NSNotification *)notification {
    if (_changeDetected) {
        [self storeUUIDOfLastSelectedItem];
        [self updateIfChanged];
        [self reselectOriginalItem];
    }
}


#pragma mark Deal with changes in a general manner

- (void)dealWithChanges {
    if (_changeDetected) {
        [self storeUUIDOfLastSelectedItem];
        [self updateIfChanged];
        [self reselectOriginalItem];
    }
}


#pragma mark Import Database Methods

- (void)importSerialKeeperDatabase {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoDatabaseImportWithSoftwareList:_softlist reselectItem:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid]];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Import Database", nil)];
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSString *thePath = [self showLoadDialogForFileExtensions:@[@"serialkeeper"]];
    if (!thePath) {
        return;
    }
    [_importDatabaseStartButton setTitle:NSLocalizedString(@"Stop", nil)];
    [_importSKDatabaseRadioButton setEnabled:NO];
    [_importRSDatabaseRadioButton setEnabled:NO];
    [_importDatabaseCancelButton setEnabled:NO];
    [_importDatabasePopUpButton setEnabled:NO];
    [_importDatabaseProgressIndicator setIndeterminate:YES];
    [_importDatabaseProgressText setStringValue:NSLocalizedString(@"Reading imported file...", nil)];
    id theObject = [NSKeyedUnarchiver unarchiveObjectWithFile:thePath];
    if (!theObject) {
        [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Could not import the database", nil) informativeText:NSLocalizedString(@"There was a problem importing the database. The previous version of the database has been left untouched. Please make sure that the file you are importing is a valid database file, and that it is not an encrypted copy of the contents of the data in the Application Support folder.", nil)];
        [appDelegate removeImportSheet];
    } else {
        if (_importDatabasePopUpIndex == 0) {
            _softlist = [theObject mutableCopy];
            [self sortDatabase];
            _changeDetected = NO;
            [appDelegate removeImportSheet];
            [_outlineView reloadData];
        } else {
            _categories = [theObject mutableCopy];
            [self mergeDatabase:theObject];
        }
    }
}

- (void)mergeDatabase:(id)object {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoDatabaseMergeWithSoftwareList:_softlist reselectItem:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid]];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Merge Database", nil)];
    _wasCancelled = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
        int i;
        int j;
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [[self importDatabaseProgressText] setStringValue:NSLocalizedString(@"Gathering information...", nil)];
            [[self importDatabaseProgressIndicator] setIndeterminate:YES];
        });
        NSMutableArray <SKApplication *> *applications = [[NSMutableArray alloc] init];
        NSMutableArray <SKApplication *> *filteredApps;
        NSMutableArray *appList = [[NSMutableArray alloc] init];
        for (i = 0; i < [[self softlist] count]; i++) {
            NSArray <SKApplication *> *catApps = [[[self softlist] objectAtIndex:i] applications];
            for (int j = 0; j < [catApps count]; j++) {
                [appList addObject:[[catApps objectAtIndex:j] product]];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [[self importDatabaseProgressText] setStringValue:NSLocalizedString(@"Checking for duplicates...", nil)];
            [[self importDatabaseProgressIndicator] setMinValue:0.0f];
            [[self importDatabaseProgressIndicator] setMaxValue:(double)[[self categories] count]];
            [[self importDatabaseProgressIndicator] setDoubleValue:0.0f];
            [[self importDatabaseProgressIndicator] setIndeterminate:NO];
        });
        for (i = 0; i < [[self categories] count]; i++) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [[self importDatabaseProgressIndicator] setDoubleValue:(double)(i + 1)];
            });
            applications = [[[[self categories] objectAtIndex:i] applications] mutableCopy];
            filteredApps = [[NSMutableArray alloc] init];
            for (int k = 0; k < [applications count]; k++) {
                BOOL appFound = NO;
                for (int m = 0; m < [appList count]; m++) {
                    if ([[appList objectAtIndex:m] isEqualToString:[[applications objectAtIndex:k] product]]) {
                        appFound = YES;
                        break;
                    }
                    if (appFound) break;
                }
                if (!appFound) {
                    [filteredApps addObject:[applications objectAtIndex:k]];
                }
            }
            [[[self categories] objectAtIndex:i] setApplications:[filteredApps mutableCopy]];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [[self importDatabaseProgressText] setStringValue:NSLocalizedString(@"Merging database...", nil)];
            [[self importDatabaseProgressIndicator] setMinValue:0.0f];
            [[self importDatabaseProgressIndicator] setMaxValue:(double)[[self softlist] count]];
            [[self importDatabaseProgressIndicator] setDoubleValue:0.0f];
        });
        for (i = 0; i < [[self categories] count]; i++) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [[self importDatabaseProgressIndicator] setDoubleValue:(double)(i + 1)];
            });
            BOOL categoryFound = NO;
            for (j = 0; j < [[self softlist] count]; j++) {
                if ([[[[self categories] objectAtIndex:i] category] isEqualToString:[[[self softlist] objectAtIndex:j] category]]) {
                    categoryFound = YES;
                    break;
                }
            }
            if (categoryFound) {
                if ([[[[self categories] objectAtIndex:i] applications] count] > 0) {
                    [[[[self softlist] objectAtIndex:j] applications] addObjectsFromArray:[[[self categories] objectAtIndex:i] applications]];
                }
            } else {
                if ([[[[self categories] objectAtIndex:i] applications] count] > 0) {
                    [[self softlist] addObject:[[self categories] objectAtIndex:i]];
                }
            }
        }
        [self sortDatabase];
        [self setChangeDetected:NO];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [appDelegate removeImportSheet];
            [[self outlineView] reloadData];
        });
    });
}

- (void)importRapidoSerialDatabase {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoDatabaseImportWithSoftwareList:_softlist reselectItem:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid]];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Import Database", nil)];
    NSString *theFile = [self showLoadDialogForFileExtensions:@[@"rapidoserial"]];
    if (!theFile) {
        _categories = nil;
        return;
    } else {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self continueRetrieving:theFile];
        });
    }
}

- (void)continueRetrieving:(NSString *)theFile {
    id theObject = [NSUnarchiver unarchiveObjectWithFile:theFile];
    if (!theObject) {
        [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Importing RapidoSerial database failed", nil) informativeText:NSLocalizedString(@"Something went wrong while importing the RapidoSerial database. Please make sure the database is neither password protected or encrypted, and that it is in RapidoSerial 2.0 format.", nil)];
        _categories = nil;
        return;
    }
    _firstOpen = NO;
    [self convertRapidoSerialDatabase:theObject];
}

- (void)convertRapidoSerialDatabase:(id)object {
    _wasCancelled = NO;
    [_importDatabaseStartButton setTitle:@"Stop"];
    [_importSKDatabaseRadioButton setEnabled:NO];
    [_importRSDatabaseRadioButton setEnabled:NO];
    [_importDatabaseCancelButton setEnabled:NO];
    [_importDatabasePopUpButton setEnabled:NO];
    [_importDatabaseProgressIndicator setIndeterminate:YES];
    [_importDatabaseProgressText setStringValue:NSLocalizedString(@"Parsing imported file...", nil)];
    [_importDatabaseStartButton setTarget:self];
    [_importDatabaseStartButton setAction:@selector(setMultiImportCancelFlag:)];
    _categories = [[NSMutableArray alloc] init];
    [_importDatabaseProgressIndicator setIndeterminate:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
        NSImage *iconImage;
        if ([object count] == 0) return;
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [[self importDatabaseProgressIndicator] setIndeterminate:YES];
            [[self importDatabaseProgressIndicator] setMinValue:0.0f];
            [[self importDatabaseProgressIndicator] setMaxValue:(double)[object count]];
            [[self importDatabaseProgressIndicator] setDoubleValue:0.0f];
            [[self importDatabaseProgressText] setStringValue:NSLocalizedString(@"Importing categories...", nil)];
        });
        for (int i = 0; i < [object count]; i++) {
            if ([self wasCancelled]) {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [appDelegate removeImportSheet];
                });
                [self setCategories:nil];
                return;
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [[self importDatabaseProgressIndicator] setDoubleValue:(double)(i + 1)];
                [[self importDatabaseProgressText] setStringValue:[NSString stringWithFormat:NSLocalizedString(@"Importing category %i of %li...", nil), (i + 1), [object count]]];
            });
            NSString *name = [[object objectAtIndex:i] objectForKey:@"name"];
            SKCategory *category = [[SKCategory alloc] initWithCategory:name];
            NSArray <NSDictionary *> *children = [[object objectAtIndex:i] objectForKey:@"children"];
            if ([children count] > 0) {
                if ([self wasCancelled]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        [appDelegate removeImportSheet];
                    });
                    [self setCategories:nil];
                    return;
                }
                for (int j = 0; j < [children count]; j++) {
                    NSString *product = [[[children objectAtIndex:j] objectForKey:@"name"] copy];
                    if (!product) product = @"";
                    NSData *inbetween = [[[children objectAtIndex:j] objectForKey:@"icon"] copy];
                    if (!inbetween) {
                        iconImage = [NSImage imageNamed:@"NSApplicationIcon"];
                    } else {
                        iconImage = [NSUnarchiver unarchiveObjectWithData:inbetween];
                    }
                    NSArray *testReps = [iconImage representations];
                    NSImage *newImage = [[NSImage alloc] init];
                    [newImage addRepresentation:[testReps objectAtIndex:0]];
                    NSData *icon = [newImage TIFFRepresentation];
                    NSString *version = [[[children objectAtIndex:j] objectForKey:@"version"] copy];
                    if (!version) version = @"";
                    NSString *company = [[[children objectAtIndex:j] objectForKey:@"company"] copy];
                    if (!company) company = @"";
                    NSString *website = [[[children objectAtIndex:j] objectForKey:@"website"] copy];
                    if (!website) website = @"";
                    SKApplication *application = [[SKApplication alloc] initWithProduct:product icon:icon version:version company:company website:website];
                    NSString *descript = [[[children objectAtIndex:j] objectForKey:@"description"] copy];
                    if (!descript) descript = @"";
                    NSString *email = [[[children objectAtIndex:j] objectForKey:@"email"] copy];
                    if (!email) email = @"";
                    NSString *notes = [[[children objectAtIndex:j] objectForKey:@"notes"] copy];
                    if (!notes) notes = @"";
                    NSString *regist = [[[children objectAtIndex:j] objectForKey:@"registration"] copy];
                    if (!regist) regist = @"";
                    NSString *regname = [[[children objectAtIndex:j] objectForKey:@"regName"] copy];
                    if (!regname) regname = @"";
                    NSString *serial = [[[children objectAtIndex:j] objectForKey:@"serial"] copy];
                    if (!serial) serial = @"";
                    NSDate *purchasedate = (NSDate *)[[[children objectAtIndex:j] objectForKey:@"purchaseDate"] copy];
                    if (!purchasedate) purchasedate = [NSDate dateWithTimeIntervalSinceNow:0];
                    NSMutableArray <SKAttachment *> *attachments = [[NSMutableArray alloc] init];
                    NSDictionary *documents = [[[children objectAtIndex:j] objectForKey:@"documents"] copy];
                    if (!documents) {
                        attachments = [@[] copy];
                    } else {
                        NSArray *keys = [documents allKeys];
                        if (![keys isEqualToArray:@[]]) {
                            for (int k = 0; k < [keys count]; k++) {
                                NSString *filename = [(NSString *)[keys objectAtIndex:k] lastPathComponent];
                                NSData *data = [documents objectForKey:[keys objectAtIndex:k]];
                                SKAttachment *attachment = [[SKAttachment alloc] initWithFilename:filename document:data];
                                [attachments addObject:[attachment copy]];
                            }
                        }
                    }
                    [application setDescript:descript];
                    [application setEmail:email];
                    [application setNotes:notes];
                    [application setRegistration:regist];
                    [application setRegname:regname];
                    [application setSerial:serial];
                    [application setPurchasedate:purchasedate];
                    [application setAttachments:[attachments copy]];
                    [category addApplication:[application copy]];
                }
            }
            [[self categories] addObject:[category copy]];
        }
        if ([self categories]) {
            if ([self importDatabasePopUpIndex] == 0) {
                [self setSoftlist:[[self categories] mutableCopy]];
                [self sortDatabase];
                [self setChangeDetected:NO];
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [appDelegate removeImportSheet];
                    [[self outlineView] reloadData];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [[self importDatabaseProgressIndicator] setIndeterminate:YES];
                    [[self importDatabaseProgressText] setStringValue:NSLocalizedString(@"Merging imported file with database...", nil)];
                    [self mergeDatabase:[self categories]];
                });
            }
        }
    });
}


#pragma mark IBActions and Methods that affect the application list

- (void)addSingleApplication:(id)object {
    NSString *fileReturned = [self showLoadDialogForFileExtensions:@[@"app", @"bundle", @"plugin", @"extension", @"component", @"saver"]];
    if (!fileReturned) return;
    NSString *theUUID = [[_outlineView itemAtRow:[_outlineView selectedRow]] uuid];
    SKApplication *addedApp = [self addApplicationWithPath:fileReturned toCategory:[_softlist objectAtIndex:_accessoryViewPopUpButtonIndex]];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationAdd:addedApp toCategory:[_softlist objectAtIndex:_accessoryViewPopUpButtonIndex] selectedUUID:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Application", nil)];
    [_outlineView reloadData];
    if ([_outlineView rowForItem:addedApp] == -1) [_outlineView expandItem:[_softlist objectAtIndex:_accessoryViewPopUpButtonIndex]];
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[_outlineView rowForItem:addedApp]] byExtendingSelection:NO];
    [_outlineView scrollRowToVisible:[_outlineView rowForItem:addedApp]];
}

- (void)addSomethingElse:(id)object {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoItemAdd:object toCategory:[_softlist objectAtIndex:[_customItemCategoryPopUpButton indexOfSelectedItem]] selectedUUID:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid]];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Item", nil)];
    [[_softlist objectAtIndex:[_customItemCategoryPopUpButton indexOfSelectedItem]] addApplication:object];
    [_outlineView reloadData];
    if ([_outlineView rowForItem:object] == -1) [_outlineView expandItem:[_softlist objectAtIndex:[_customItemCategoryPopUpButton indexOfSelectedItem]]];
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[_outlineView rowForItem:object]] byExtendingSelection:NO];
    [_outlineView scrollRowToVisible:[_outlineView selectedRow]];
}

- (void)createNewCategory {
    SKCategory *category = [[SKCategory alloc] initWithCategory:NSLocalizedString(@"New category", nil)];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoCategoryAdd:category selectedUUID:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid]];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Category", nil)];
    [_softlist addObject:category];
    [self sortCategoriesOnly];
    [_outlineView reloadData];
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[_outlineView rowForItem:category]] byExtendingSelection:NO];
    [_outlineView scrollRowToVisible:[_outlineView selectedRow]];
}

- (void)removeSelectedItem {
    _expansionState = [self getExpansionState];
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSString *msgText;
    NSString *infoText;
    NSInteger selRow;
    NSInteger origSelRow = -1;
    if (!_nonselect) {
        selRow = [_outlineView selectedRow];
    } else {
        selRow = _nonselectRow;
        origSelRow = [_outlineView selectedRow];
    }
    if (selRow == -1) return;
    id theSelection = [_outlineView itemAtRow:selRow];
    if ([theSelection class] == [SKCategory class]) {
        if ([_softlist count] == 1) {
            [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Cannot remove only category", nil) informativeText:NSLocalizedString(@"Categories do not have to contain any items, but you must have at least one category in your list.", nil)];
            return;
        }
    }
    NSImage *theImage = [[NSImage alloc] init];
    NSString *theName;
    theImage = [[NSWorkspace sharedWorkspace] iconForFileType:NSFileTypeForHFSTypeCode(kGenericFolderIcon)];
    if ([[appDelegate valueForKey:@"userConfirmationForDelete"] boolValue] == YES) {
        if ([theSelection class] == [SKApplication class]) {
            theName = [(SKApplication *)[_outlineView itemAtRow:selRow] product];
            msgText = [NSString stringWithFormat:NSLocalizedString(@"Remove application or item '%@'", nil), theName];
            infoText = [NSString stringWithFormat:NSLocalizedString(@"item '%@'", nil), theName];
            theImage = [[NSImage alloc] initWithData:[(SKApplication *)[_outlineView itemAtRow:selRow] icon]];
        } else {
            theName = [(SKCategory *)[_outlineView itemAtRow:selRow] category];
            msgText = [NSString stringWithFormat:NSLocalizedString(@"Remove category '%@'", nil), theName];
            infoText = [NSString stringWithFormat:NSLocalizedString(@"category '%@', and all the items it contains", nil), theName];
        }
        NSAlert *theAlert = [NSAlert alertWithMessageText:msgText defaultButton:NSLocalizedString(@"Cancel", nil) alternateButton:nil otherButton:NSLocalizedString(@"OK", nil) informativeTextWithFormat:NSLocalizedString(@"This will remove the %@. Are you sure you want to continue?", nil), infoText];
        [theAlert setIcon:theImage];
        NSModalResponse theResponse = [theAlert runModal];
        if (theResponse == NSAlertDefaultReturn) {
            return;
        }
    }
    if (!_searchActive) {
        if ([theSelection class] == [SKApplication class]) {
            SKCategory *theCategory = [_outlineView parentForItem:theSelection];
            [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationDelete:theSelection fromCategory:theCategory selectedUUID:[theSelection uuid]];
            [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove Application", nil)];
            [theCategory removeApplication:theSelection];
        } else {
            [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoCategoryDelete:[_softlist copy] expansionState:[_expansionState copy] selectedUUID:[[theSelection uuid] copy]];
            [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove Category", nil)];
            [_softlist removeObject:[_outlineView itemAtRow:selRow]];
        }
        [_outlineView reloadData];
    } else {
        if ([theSelection class] == [SKApplication class]) {
            SKCategory *theCategory = [_softlist objectAtIndex:[theSelection parentIndex]];
            [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationDelete:[[[_softlist objectAtIndex:[theSelection parentIndex]] applications] objectAtIndex:[theSelection originalIndex]] fromCategory:theCategory selectedUUID:[theSelection uuid]];
            [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove Application", nil)];
            [theCategory removeApplication:[[[_softlist objectAtIndex:[theSelection parentIndex]] applications] objectAtIndex:[theSelection originalIndex]]];
        } else {
            [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoCategoryDelete:[_softlist copy] expansionState:[_expansionState copy] selectedUUID:[[theSelection uuid] copy]];
            [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove Category", nil)];
            [_softlist removeObject:[_softlist objectAtIndex:[theSelection originalIndex]]];
        }
        [self buildFilteredSoftwareList];
    }
    if (!_nonselect) {
        [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:(selRow - 1)] byExtendingSelection:NO];
        [_outlineView scrollRowToVisible:selRow - 1];
    } else {
        [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:origSelRow] byExtendingSelection:NO];
    }
}

- (void)importAppFolderAppsWithCategoryIndex:(NSInteger)index {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoImportAppFolderWithSoftwareList:_softlist reselectItem:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid]];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Import App Folder", nil)];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [self setGatheredApps:[[NSMutableArray alloc] init]];
        int i;
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [[self importStartButton] setTarget:self];
            [[self importStartButton] setAction:@selector(setMultiImportCancelFlag:)];
        });
        [self setWasCancelled:NO];
        AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL doubledAppFound = NO;
        NSError *error;
        SKCategory *category;
        NSUInteger addedCounter = 0;
        NSArray *internalApps = @[@"App Store.app", @"Dock.app", @"Dashboard.app", @"Automator.app", @"Launchpad.app", @"Font Book.app", @"Mail.app", @"Safari.app", @"Safari Technology Preview.app", @"Contacts.app", @"Calendar.app", @"iCal.app", @"Photos.app", @"iPhoto.app", @"Dictionary.app", @"Xcode.app", @"QuickTime Player.app", @"iTunes.app", @"Chess.app", @"Time Machine.app", @"Preview.app", @"Image Capture.app", @"DVD Player.app", @"iBooks.app", @"iBooks Author.app", @"Maps.app", @"Messages.app", @"Mission Control.app", @"Photo Booth.app", @"Siri.app", @"System Preferences.app", @"TextEdit.app", @"Script Editor.app", @"Stickies.app", @"Calculator.app", @"FaceTime.app", @"Reminders.app", @"Notes.app", @"AirPort Utility.app", @"Bluetooth File Exchange.app", @"Boot Camp Assistant.app", @"ColorSync Utility.app", @"Console.app", @"Digital Color Meter.app", @"Disk Utility.app", @"Feedback Assistant.app", @"Grab.app", @"Grapher.app", @"Keychain Access.app", @"Migration Assistant.app", @"Script Editor.app", @"AppleScript Editor.app", @"System Information.app", @"Terminal.app", @"VoiceOver Utility.app", @"XQuartz.app", @"Stocks.app", @"Home.app"];
        NSString *fullpath;
        NSString *lastComponent;
        NSMutableArray *freshApplications = [[NSMutableArray alloc] init];
        NSString *appPath = [[[[NSFileManager defaultManager] URLsForDirectory:NSApplicationDirectory inDomains:NSLocalDomainMask] firstObject] path];
        if (index == 0) {
            category = [[SKCategory alloc] initWithCategory:NSLocalizedString(@"Imported Apps", nil)];
        } else {
            category = [[self softlist] objectAtIndex:(index - 2)];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [[self importProgressText] setStringValue:NSLocalizedString(@"Finding applications in Applications folder...", nil)];
        });
        if (![self importState]) {
            NSArray *files = [fileManager contentsOfDirectoryAtPath:[[[[NSFileManager defaultManager] URLsForDirectory:NSApplicationDirectory inDomains:NSLocalDomainMask] firstObject] path] error:&error];
            if (error) {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [appDelegate removeImportMultiSheet];
                    NSString *errorDescription = [NSString stringWithFormat:NSLocalizedString(@"An error occurred while adding applications from the Applications directory. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. We are very sorry for the inconvenience.", nil), [error code],[error localizedDescription]];
                    [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Error while importing applications", nil) informativeText:errorDescription];
                });
                return;
            }
            if (!files) {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [appDelegate removeImportMultiSheet];
                    NSString *errorDescription = NSLocalizedString(@"No applications were found in the Application folder. This could point to an internal error. Please contact the developer if the problem persists. We are very sorry for the inconvenience.", nil);
                    [SKUtilities showAlertWithMessageText:NSLocalizedString(@"No applications found", nil) informativeText:errorDescription];
                });
                return;
            }
            for (i = 0; i < [files count]; i++) {
                [[self gatheredApps] addObject:[NSString stringWithFormat:@"%@/%@", appPath, [files objectAtIndex:i]]];
            }
        } else {
            NSString *path = [[[[NSFileManager defaultManager] URLsForDirectory:NSApplicationDirectory inDomains:NSLocalDomainMask] firstObject] path];
            [self getAppsFromDirectory:path];
        }
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pathExtension == %@", @"app"];
        NSArray *apps = [[self gatheredApps] filteredArrayUsingPredicate:predicate];
        predicate = [NSPredicate predicateWithFormat:@"NOT (lastPathComponent IN %@)", internalApps];
        NSArray *filteredApps = [apps filteredArrayUsingPredicate:predicate];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [[self importProgressIndicator] setIndeterminate:NO];
            [[self importProgressIndicator] setMinValue:0.0f];
            [[self importProgressIndicator] setMaxValue:(double)[filteredApps count]];
            [[self importProgressIndicator] setDoubleValue:0.0f];
        });
        for (i = 0; i < [filteredApps count]; i++) {
            doubledAppFound = NO;
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [[self importProgressText] setStringValue:[NSString stringWithFormat:NSLocalizedString(@"Processing application %i of %lu (%lu added)...", nil), i, [filteredApps count], addedCounter]];
            });
            fullpath = [filteredApps objectAtIndex:i];
            lastComponent = [[fullpath lastPathComponent] stringByDeletingPathExtension];
            if ([self wasCancelled]) {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [appDelegate removeImportMultiSheet];
                });
                return;
            }
            for (int j = 0; j < [[self softlist] count]; j++) {
                for (int k = 0; k < [[[[self softlist] objectAtIndex:j] applications] count]; k++) {
                    if ([[[[[[self softlist] objectAtIndex:j] applications] objectAtIndex:k] product] isEqualToString:lastComponent]) {
                        doubledAppFound = YES;
                        break;
                    }
                    if (doubledAppFound) break;
                }
                if (doubledAppFound) break;
            }
            if (!doubledAppFound) {
                addedCounter++;
                SKApplication *newApp = [self addApplicationWithPath:fullpath toCategory:nil];
                if (newApp) [freshApplications addObject:[newApp copy]];
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [[self importProgressIndicator] setDoubleValue:(double)i];
            });
        }
        if ([freshApplications count] == 0) {
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [appDelegate removeImportMultiSheet];
                [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Nothing left to import", nil) informativeText:NSLocalizedString(@"All applications have apparently been imported already, so there's nothing left to do!", nil)];
            });
        } else {
            [category addMultipleApplications:freshApplications];
            if (index == 0) {
                [[self softlist] addObject:category];
                [self sortCategoriesOnly];
            }
            dispatch_async(dispatch_get_main_queue(), ^(void){
                [[self outlineView] reloadData];
                [[self outlineView] expandItem:category];
                [[self outlineView] selectRowIndexes:[NSIndexSet indexSetWithIndex:[[self outlineView] rowForItem:category]] byExtendingSelection:NO];
                [[self outlineView] scrollRowToVisible:[[self outlineView] selectedRow]];
                [appDelegate removeImportMultiSheet];
            });
        }
    });
}

- (void)getAppsFromDirectory:(NSString *)directory {
    int i;
    NSArray *theseAreNotDirectories = [[NSArray alloc] initWithObjects:@"app", @"bundle", @"plugin", @"framework", @"saver", @"png", @"jpg", @"txt", @"rtfd", @"rtf", @"tif", nil];
    NSPredicate *appPredicate = [NSPredicate predicateWithFormat:@"pathExtension == %@", @"app"];
    NSPredicate *dirPredicate = [NSPredicate predicateWithFormat:@"NOT (pathExtension IN %@)", theseAreNotDirectories];
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:nil];
    NSArray *appsInFiles = [files filteredArrayUsingPredicate:appPredicate];
    if (appsInFiles) {
        for (i = 0; i < [appsInFiles count]; i++) {
            [[self gatheredApps] addObject:[NSString stringWithFormat:@"%@/%@", directory, [appsInFiles objectAtIndex:i]]];
        }
    }
    NSArray *dirsInFiles = [files filteredArrayUsingPredicate:dirPredicate];
    if (dirsInFiles) {
        for (i = 0; i < [dirsInFiles count]; i++) {
            NSString *curdir = [NSString stringWithFormat:@"%@/%@", directory, [dirsInFiles objectAtIndex:i]];
            BOOL isDir = NO;
            BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:curdir isDirectory:&isDir];
            if (exists && isDir) {
                [self getAppsFromDirectory:curdir];
            }
        }
    }
}

- (IBAction)setMultiImportCancelFlag:(id)sender {
    [self setWasCancelled:YES];
}

- (IBAction)datePickerChanged:(id)sender {
    _changeDetected = YES;
}


#pragma mark Methods that involve the application list

- (void)displayCurrentSelection {
    id selectedItem = [_outlineView itemAtRow:[_outlineView selectedRow]];
    if ([[selectedItem class] isEqual:[SKCategory class]]) {
        SKCategory *theItem = (SKCategory *)selectedItem;
        [_applicationBox setHidden:YES];
        [_nothingBox setHidden:YES];
        [_categoryBox setHidden:NO];
        [_categoryName setStringValue:[theItem category]];
        [_categoryDescription setString:[theItem descript]];
    } else if ([[selectedItem class] isEqual:[SKApplication class]]) {
        SKApplication *theItem = (SKApplication *)selectedItem;
        [_categoryBox setHidden:YES];
        [_nothingBox setHidden:YES];
        [_applicationBox setHidden:NO];
        [_applicationIcon setImage:[[NSImage alloc] initWithData:[theItem icon]]];
        [_applicationProduct setStringValue:[theItem product]];
        [_applicationVersion setStringValue:[theItem version]];
        [_applicationDescription setString:[theItem descript]];
        [_applicationVersion setStringValue:[theItem version]];
        [_applicationRegkey setStringValue:[theItem registration]];
        [_applicationRegName setStringValue:[theItem regname]];
        [_applicationPurchaseDate setDateValue:[theItem purchasedate]];
        [_applicationEMail setStringValue:[theItem email]];
        [_applicationSerial setStringValue:[theItem serial]];
        [_applicationCompany setStringValue:[theItem company]];
        [_applicationWebsite setStringValue:[theItem website]];
        [_applicationNotes setString:[theItem notes]];
        [_tableViewController updateAttachmentList:[theItem attachments]];
    } else {
        return;
    }
}

- (NSMutableArray *)getExpansionState {
    NSMutableArray *expansionState = [[NSMutableArray alloc] init];
    for (int i = 0; i < [_softlist count]; i++) {
        id theItem = [_softlist objectAtIndex:i];
        BOOL theTest = [_outlineView isItemExpanded:theItem];
        [expansionState addObject:[NSNumber numberWithBool:theTest]];
    }
    return expansionState;
}

- (NSRect)getScrollviewState {
    return [[_scrollView contentView] documentVisibleRect];
}

- (NSInteger)getSelectedRow {
    return [_outlineView selectedRow];
}


#pragma mark App Support Folder and Database Creation related methods

- (void)prepareAppSupportFolder {
    _startTour = YES;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    BOOL isDir;
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    if (error) {
        NSString *errorDescription = [NSString stringWithFormat:NSLocalizedString(@"An error occurred while accessing the application support directory. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. The application will close after you press the OK button. We are very sorry for the inconvenience.", nil), [error code],[error localizedDescription]];
        [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Error while accessing application data", nil) informativeText:errorDescription];
        [[NSApplication sharedApplication] terminate:nil];
    }
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"SerialKeeper"];
    NSURL *URLToMyAppSupportDir = [NSURL fileURLWithPath:pathToMyAppSupportDir];
    BOOL folderExists = [fileManager fileExistsAtPath:pathToMyAppSupportDir isDirectory:&isDir];
    if (folderExists && !isDir) {
        [SKUtilities showAlertWithMessageText:NSLocalizedString(@"File found where a folder was expected", nil) informativeText:NSLocalizedString(@"There is a file with the name \"SerialKeeper\" in the \"Library/Application Support\" folder of your home folder. Please move this file to a different location and launch the application again. The application will close after you press the OK button.", nil)];
        [[NSApplication sharedApplication] terminate:nil];
    }
    if (!folderExists) {
        BOOL directoryCreated = [fileManager createDirectoryAtURL:URLToMyAppSupportDir withIntermediateDirectories:YES attributes:nil error:&error];
        if (!directoryCreated) {
            NSString *errorDescription = [NSString stringWithFormat:NSLocalizedString(@"An error occurred while creating the application support directory. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. The application will close after you press the OK button. We are very sorry for the inconvenience.", nil), [error code],[error localizedDescription]];
            [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Error while creating application support directory", nil) informativeText:errorDescription];
            [[NSApplication sharedApplication] terminate:nil];
        }
        [self createDummyDatabase];
    }
    NSString *pathToMyQLScratchFolder = [pathToMyAppSupportDir stringByAppendingPathComponent:@"QuickLook"];
    isDir = NO;
    BOOL qlFolderExists = [[NSFileManager defaultManager] fileExistsAtPath:pathToMyQLScratchFolder isDirectory:&isDir];
    if (!qlFolderExists) {
        BOOL qlFolderCreated = [[NSFileManager defaultManager] createDirectoryAtPath:pathToMyQLScratchFolder withIntermediateDirectories:YES attributes:nil error:&error];
        if (!qlFolderCreated) {
            NSString *errorDescription = [NSString stringWithFormat:NSLocalizedString(@"An error occurred while creating the QuickLook support folder. Details in the system language are below:\n\n%li: %@\n\nPlease contact the developer if the problem persists. The application will close after you press the OK button. We are very sorry for the inconvenience.", nil), [error code],[error localizedDescription]];
            [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Error while creating application support directory", nil) informativeText:errorDescription];
            [[NSApplication sharedApplication] terminate:nil];
        }
    } else {
        if (!isDir) {
            [SKUtilities showAlertWithMessageText:NSLocalizedString(@"File found where a folder was expected", nil) informativeText:NSLocalizedString(@"There is a file with the name \"QuickLook\" in folder \"Library/Application Support/SerialKeeper\" of your home folder. Please move this file to a different location and launch the application again. The application will close after you press the OK button.", nil)];
            [[NSApplication sharedApplication] terminate:nil];
        }
    }
    [_tableViewController setPathToMyQLScratchFolder:pathToMyQLScratchFolder];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"SoftwareList.serialkeeper"];
    isDir = NO;
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:pathToMyAppSupportFile isDirectory:&isDir];
    if (!fileExists) {
        [self createDummyDatabase];
    } else if (fileExists && isDir) {
        [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Folder found where a file was expected", nil) informativeText:NSLocalizedString(@"There is a folder with the name \"SoftwareList.serialkeeper\" in folder \"Library/Application Support/SerialKeeper\" of your home folder. Please move this folder to a different location and launch the application again. The application will close after you press the OK button.", nil)];
        [[NSApplication sharedApplication] terminate:nil];
    } else {
        [self getCardinalPrefs];
        [self loadDatabase];
        _startTour = NO;
    }
}

- (void)createDummyDatabase {
    SKCategory *dummyCategory = [[SKCategory alloc] initWithCategory:NSLocalizedString(@"Applications", nil)];
    SKApplication *app1 = [[SKApplication alloc] initWithProduct:@"Triggerfinger" icon:[[NSImage imageNamed:@"triggerfinger"] TIFFRepresentation] version:@"2.66 (46)" company:@"Kinoko House" website:@"https://bitbucket.org/kinokohouse/triggerfinger"];
    [app1 setEmail:@"your@email.com"];
    [app1 setSerial:NSLocalizedString(@"[no serial needed]", nil)];
    [app1 setRegname:NSLocalizedString(@"Your Name Here", nil)];
    [app1 setDescript:NSLocalizedString(@"Control iTunes, Music and Spotify applications, and get internet radio capabilities to boot!", nil)];
    [app1 setRegistration:NSLocalizedString(@"[no registration needed]", nil)];
    [app1 setNotes:NSLocalizedString(@"Available under an MIT license", nil)];
    [app1 setPurchasedate:[NSDate dateWithTimeIntervalSinceNow:0]];
    [dummyCategory addApplication:[app1 copy]];

    SKApplication *app2 = [[SKApplication alloc] initWithProduct:@"NoSleep" icon:[[NSImage imageNamed:@"nosleep"] TIFFRepresentation] version:@"1.12 (4)" company:@"Kinoko House" website:@"https://bitbucket.org/kinokohouse/nosleep"];
    [app2 setEmail:@"your@email.com"];
    [app2 setSerial:NSLocalizedString(@"[no serial needed]", nil)];
    [app2 setRegname:NSLocalizedString(@"Your Name Here", nil)];
    [app2 setDescript:NSLocalizedString(@"Gamers don't sleep, and neither should their computer.", nil)];
    [app2 setRegistration:NSLocalizedString(@"[no registration needed]", nil)];
    [app2 setNotes:NSLocalizedString(@"Available under an MIT license", nil)];
    [app2 setPurchasedate:[NSDate dateWithTimeIntervalSinceNow:0]];
    [dummyCategory addApplication:[app2 copy]];

    SKApplication *app3 = [[SKApplication alloc] initWithProduct:@"File Type & Creator Editor" icon:[[NSImage imageNamed:@"filespecedit"] TIFFRepresentation] version:@"1.0 (21)" company:@"Kinoko House" website:@"https://bitbucket.org/kinokohouse/file-spec-editor"];
    [app3 setEmail:@"your@email.com"];
    [app3 setSerial:NSLocalizedString(@"[no serial needed]", nil)];
    [app3 setRegname:NSLocalizedString(@"Your Name Here", nil)];
    [app3 setDescript:NSLocalizedString(@"A way to change ye olde File Type and Creator file information in this modern age.", nil)];
    [app3 setRegistration:NSLocalizedString(@"[no registration needed]", nil)];
    [app3 setNotes:NSLocalizedString(@"Available under an MIT license", nil)];
    [app3 setPurchasedate:[NSDate dateWithTimeIntervalSinceNow:0]];
    [dummyCategory addApplication:[app3 copy]];

    _softlist = [[NSMutableArray alloc] initWithObjects:[dummyCategory copy], nil];
    [self sortDatabase];
}

- (void)loadDatabase {
    _theObject = nil;
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"SerialKeeper"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"SoftwareList.serialkeeper"];
    @try {
        _theObject = [NSKeyedUnarchiver unarchiveObjectWithFile:pathToMyAppSupportFile];
    }
    @catch (NSException *e) {
        _theObject = nil;
    }
    if (!_theObject) {
        _initDeferred = YES;
        _isEncrypted = YES;
    } else {
        if (_usePassword) {
            _initDeferred = YES;
            _isEncrypted = NO;
        } else {
            _softlist = [_theObject mutableCopy];
            id testUUID = [[_softlist objectAtIndex:0] uuid];
            if (!testUUID) {
                [self resetUUIDsForDatabase];
            } else if ([testUUID isEqualToString:@""]) {
                [self resetUUIDsForDatabase];
            }
            _theObject = nil;
            [_outlineView reloadData];
        }
    }
}

- (void)resetUUIDsForDatabase {
    for (int i = 0; i < [_softlist count]; i++) {
        [[_softlist objectAtIndex:i] setUuid:[SKUtilities createUUID]];
        for (int j = 0; j < [[[_softlist objectAtIndex:i] applications] count]; j++) {
            [[[[_softlist objectAtIndex:i] applications] objectAtIndex:j] setUuid:[SKUtilities createUUID]];
        }
    }
}

- (void)loadDatabaseDeferred {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"SerialKeeper"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"SoftwareList.serialkeeper"];
    if (_isEncrypted) {
        _encryptedObject = [[NSData alloc] initWithContentsOfFile:pathToMyAppSupportFile];
    }
    [[appDelegate secureTextField] setStringValue:@""];
    [[appDelegate passwordSheet] makeFirstResponder:[appDelegate secureTextField]];
    [NSApp beginSheet:[appDelegate passwordSheet] modalForWindow:[appDelegate window] modalDelegate:nil didEndSelector:nil contextInfo:nil];
}

- (void)continueDeferredLoad {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSError *error = nil;
    if (_isEncrypted) {
        NSData *decryptedObject = [RNDecryptor decryptData:_encryptedObject withPassword:[_secureTextField stringValue] error:&error];
        if (error) {
            NSBeep();
            [self shakeThisWindow:[appDelegate passwordSheet]];
        } else {
            [appDelegate setPassword:[_secureTextField stringValue]];
            [NSApp endSheet:[appDelegate passwordSheet]];
            [[appDelegate passwordSheet] orderOut:self];
            _softlist = [[NSKeyedUnarchiver unarchiveObjectWithData:decryptedObject] mutableCopy];
            id testUUID = [[_softlist objectAtIndex:0] uuid];
            if (!testUUID) {
                [self resetUUIDsForDatabase];
            } else if ([testUUID isEqualToString:@""]) {
                [self resetUUIDsForDatabase];
            }
            [_outlineView reloadData];
            [appDelegate continueFinishLaunchingFromDeferredLoad];
        }
    } else {
        if ([[[[appDelegate secureTextField] stringValue] MD5String] isNotEqualTo:_userHash]) {
            NSBeep();
            [self shakeThisWindow:[appDelegate passwordSheet]];
        } else {
            [appDelegate setPassword:[[appDelegate secureTextField] stringValue]];
            [NSApp endSheet:[appDelegate passwordSheet]];
            [[appDelegate passwordSheet] orderOut:self];
            _softlist = [_theObject mutableCopy];
            id testUUID = [[_softlist objectAtIndex:0] uuid];
            if (!testUUID) {
                [self resetUUIDsForDatabase];
            } else if ([testUUID isEqualToString:@""]) {
                [self resetUUIDsForDatabase];
            }
            [_outlineView reloadData];
            [appDelegate continueFinishLaunchingFromDeferredLoad];
        }
    }
}

- (void)saveDatabase {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"SerialKeeper"];
    NSString *pathToMyAppSupportFile = [pathToMyAppSupportDir stringByAppendingPathComponent:@"SoftwareList.serialkeeper"];
    if ([appDelegate useEncryption]) {
        NSData *dataToEncrypt = [NSKeyedArchiver archivedDataWithRootObject:_softlist];
        error = nil;
        NSData *encryptedData = [RNEncryptor encryptData:dataToEncrypt withSettings:kRNCryptorAES256Settings password:[appDelegate password] error:&error];
        if (error) {
            [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Problem encrypting the database", nil) informativeText:[NSString stringWithFormat:NSLocalizedString(@"There was a problem encrypting the database; details are below:\n\n%@\n\n. The previous version of the database is still as it was. Please try to relauch the application, or restore an earlier backup of your database if the problem persists, or contact the developer for further help. The application will close after you press the OK button. We are very sorry for the inconvenience.", nil), [error localizedDescription]]];
            [[NSApplication sharedApplication] terminate:nil];
        }
        success = [encryptedData writeToFile:pathToMyAppSupportFile atomically:YES];
        if (!success) {
            [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Could not save the database", nil) informativeText:NSLocalizedString(@"There was an unspecified problem saving the database. The previous version of the database should still be in order. Please try to relauch the application, or restore an earlier backup of your database if the problem persists, or contact the developer for further help. The application will close after you press the OK button. We are very sorry for the inconvenience.", nil)];
            [[NSApplication sharedApplication] terminate:nil];
        }
    } else {
        success = [NSKeyedArchiver archiveRootObject:_softlist toFile:pathToMyAppSupportFile];
        if (!success) {
            [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Could not save the database", nil) informativeText:NSLocalizedString(@"There was an unspecified problem saving the database. The previous version of the database should still be in order. Please try to relauch the application, or restore an earlier backup of your database if the problem persists, or contact the developer for further help. The application will close after you press the OK button. We are very sorry for the inconvenience.", nil)];
            [[NSApplication sharedApplication] terminate:nil];
        }
    }
    [[NSWorkspace sharedWorkspace] setIcon:[NSImage imageNamed:@"SKDatabaseIcon"] forFile:pathToMyAppSupportFile options:0];
}

- (SKApplication *)addApplicationWithPath:(NSString *)path toCategory:(SKCategory *)category {
    NSImage *genericIcon = [SKUtilities iconWithSystemReference:@"GenericApplicationIcon"];
    if ([[path pathExtension] isNotEqualTo:@"app"]) genericIcon = [[NSWorkspace sharedWorkspace] iconForFileType:@"kext"];
    if ([[path pathExtension] isEqualToString:@"saver"]) genericIcon = [[NSWorkspace sharedWorkspace] iconForFileType:@"saver"];
    NSString *infopath = [NSString stringWithFormat:@"%@/Contents/info.plist", path];
    NSString *versionpath = [NSString stringWithFormat:@"%@/Contents/version.plist", path];
    NSFileManager *filemanager = [NSFileManager defaultManager];
    NSImage *icon;
    NSData *icondata;
    BOOL repFound;
    BOOL infoFileExists = [filemanager fileExistsAtPath:infopath];
    if (!infoFileExists) {
        return nil;
    }
    BOOL versionFileExists = [filemanager fileExistsAtPath:versionpath];
    NSMutableDictionary *infodict = [NSMutableDictionary dictionaryWithContentsOfFile:infopath];
    NSString *appname = [[path lastPathComponent] stringByDeletingPathExtension];
    if (!appname) appname = NSLocalizedString(@"<unknown>", nil);
    NSString *version = [infodict valueForKey:@"CFBundleShortVersionString"];
    if (!version) version = @"?.?";
    NSBundle *appBundle = [NSBundle bundleWithPath:path];
    NSString *iconname = [infodict valueForKey:@"CFBundleIconFile"];
    if (!iconname) {
        icon = [genericIcon copy];
    } else {
        icon = [appBundle imageForResource:iconname];
        NSArray *reps = [icon representations];
        NSImage *newImage = [[NSImage alloc] init];
        repFound = NO;
        for (int p = 0; p < [reps count]; p++) {
            if ([(NSImageRep *)[reps objectAtIndex:p] pixelsWide] == 256) {
                repFound = YES;
                [newImage addRepresentation:[reps objectAtIndex:p]];
                break;
            }
        }
        if (!repFound) {
            [newImage addRepresentation:[reps objectAtIndex:0]];
        }
        if (!newImage) {
            icon = [genericIcon copy];
        } else {
            icon = [newImage copy];
        }
    }
    icondata = [icon TIFFRepresentation];
    NSString *ident = [infodict valueForKey:@"CFBundleIdentifier"];
    if (!ident) ident = NSLocalizedString(@"com.unknown", nil);
    NSArray <NSString *> *idarray = [ident componentsSeparatedByString:@"."];
    if ([idarray count] < 2) {
        idarray = @[NSLocalizedString(@"com", nil),NSLocalizedString(@"unknown", nil)];
    }
    NSString *company = [[idarray objectAtIndex:1] capitalizedString];
    NSString *website = [NSString stringWithFormat:@"https://www.%@.%@", [idarray objectAtIndex:1], [idarray objectAtIndex:0]];
    if (versionFileExists) {
        NSMutableDictionary *versiondict = [NSMutableDictionary dictionaryWithContentsOfFile:versionpath];
        NSString *newversion = [versiondict valueForKey:@"CFBundleShortVersionString"];
        if (newversion) {
            version = [newversion copy];
        }
    }
    SKApplication *newApp = [[SKApplication alloc] initWithProduct:appname icon:icondata version:version company:company website:website];
    if (category) {
        [category addApplication:newApp];
    }
    return newApp;
}


#pragma mark Database Sorting Methods

- (void)sortDatabase {
    for (NSUInteger i = [_softlist count]; i > 0; i--) {
        SKCategory *theCategory = [_softlist objectAtIndex:(i - 1)];
        NSArray *appArray = [[theCategory applications] copy];
        NSSortDescriptor *appSorter = [[NSSortDescriptor alloc] initWithKey:@"product" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        [theCategory setApplications:[[appArray sortedArrayUsingDescriptors:@[appSorter]] mutableCopy]];
        [_softlist removeObjectAtIndex:(i - 1)];
        [_softlist addObject:[theCategory copy]];
    }
    NSArray *sortArray = [_softlist copy];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    _softlist = [[sortArray sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
}

- (void)sortCategoriesOnly {
    if (_searchActive) {
        NSArray *sortArray = [_filteredlist copy];
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        _filteredlist = [[sortArray sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
    }
    NSArray *sortArray = [_softlist copy];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"category" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    _softlist = [[sortArray sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
}


#pragma mark Interwebs related IBActions

- (IBAction)openCompanyURL:(id)sender {
    NSError *error = nil;
    NSString *theURLAsAString = [_applicationWebsite stringValue];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"://" options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:theURLAsAString options:0 range:NSMakeRange(0, [theURLAsAString length])];
    if ([theURLAsAString isEqualToString:@""]) {
        return;
    } else if (numberOfMatches == 0) {
        theURLAsAString = [NSString stringWithFormat:@"https://%@", theURLAsAString];
    }
    NSURL *url = [NSURL URLWithString:theURLAsAString];
    if (url) {
        [[NSWorkspace sharedWorkspace] openURL:url];
    }
}


#pragma mark Export Document IBActions

- (void)exportSelectedDocuments {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *theFileName = nil;
    NSString *fullpath;
    NSString *currentExt;
    NSString *currentPath;
    NSData *theData = [[NSData alloc] init];
    if ([_tableViewController selectedRow] == -1) return;
    NSIndexSet *selectedRows = [_applicationTableView selectedRowIndexes];
    if ([selectedRows count] == 1) {
        theFileName = [[_tableViewController getCurrentlySelectedObject] filename];
        theData = [[_tableViewController getCurrentlySelectedObject] document];
        NSArray *theResult = [self showSaveDialogWithFilename:theFileName message:[NSString stringWithFormat:NSLocalizedString(@"Save attachment \"%@\" as...", nil),theFileName] title:NSLocalizedString(@"Save attachment", nil)];
        if (theResult != nil) {
            NSURL *theURL = [theResult objectAtIndex:0];
            fullpath = [NSString stringWithFormat:@"%@", [theURL path]];
            [theData writeToFile:fullpath atomically:YES];
            _firstSave = NO;
        }
    } else {
        NSString *savepath = [self getDirectoryForAttachmentExport];
        NSArray *theArray = [[_tableViewController attachmentList] objectsAtIndexes:selectedRows];
        for (int i = 0; i < [theArray count]; i++) {
            fullpath = [NSString stringWithFormat:@"%@/%@", savepath, [(SKAttachment *)[theArray objectAtIndex:i] filename]];
            while ([fileManager fileExistsAtPath:fullpath]) {
                currentExt = [fullpath pathExtension];
                currentPath = [fullpath stringByDeletingPathExtension];
                fullpath = [SKUtilities getNameForCopy:currentPath];
                fullpath = [NSString stringWithFormat:@"%@.%@", fullpath, currentExt];
            }
            theData = [(SKAttachment *)[theArray objectAtIndex:i] document];
            [theData writeToFile:fullpath atomically:YES];
            _firstSave = NO;
        }
    }
}


#pragma mark File Storage and Retrieval methods

- (NSArray *)showSaveDialogWithFilename:(NSString *)defaultFilename message:(NSString *)message title:(NSString *)title {
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    [savePanel setMessage:message];
    [savePanel setTitle:title];
    if (_firstSave) {
        NSURL *theDirectory = [NSURL URLWithString:[NSString stringWithFormat:@"%@/Desktop/", NSHomeDirectory()]];
        [savePanel setDirectoryURL:theDirectory];
    }
    [savePanel setCanCreateDirectories:YES];
    [savePanel setNameFieldLabel:NSLocalizedString(@"Save file as:", nil)];
    [savePanel setPrompt:NSLocalizedString(@"Save", nil)];
    [savePanel setNameFieldStringValue:defaultFilename];
    NSInteger theResult = [savePanel runModal];
    if (theResult == NSOKButton) {
        return [[NSArray alloc] initWithObjects:[savePanel URL], [savePanel nameFieldStringValue], nil];
    } else {
        return nil;
    }
}

- (NSString *)showLoadDialogForFileExtensions:(NSArray <NSString *> *)allowedExtensions {
    NSOpenPanel *filePanel = [NSOpenPanel openPanel];
    [filePanel setTitle:@"Load file"];
    [filePanel setAccessoryView:nil];
    if (class_getProperty([filePanel class], "accessoryViewDisclosed")) {
        [filePanel setValue:@NO forKey:@"accessoryViewDisclosed"];
    }
    if (allowedExtensions) {
        if ([allowedExtensions count] == 1) {
            [filePanel setMessage:[NSString stringWithFormat:NSLocalizedString(@"Please select .%@ file:", nil), [allowedExtensions objectAtIndex:0]]];
        } else {
            if ([[allowedExtensions objectAtIndex:0] isEqualToString:@"app"]) {
                [filePanel setAccessoryView:_accessoryView];
                [self compileCategoryMenuForOpenPanelAccessoryView];
                if (class_getProperty([filePanel class], "accessoryViewDisclosed")) {
                    [filePanel setValue:@YES forKey:@"accessoryViewDisclosed"];
                }
                [filePanel setMessage:NSLocalizedString(@"Please select item to add:", nil)];
            } else {
                [filePanel setMessage:NSLocalizedString(@"Please select image file:", nil)];
            }
        }
    } else {
        [filePanel setMessage:NSLocalizedString(@"Please select document to add:", nil)];
    }
    [filePanel setCanChooseDirectories:YES];
    [filePanel setCanChooseFiles:YES];
    _allowedExtensions = allowedExtensions;
    [filePanel setDelegate:self];
    if (_firstOpen) {
        if (!allowedExtensions || [allowedExtensions count] == 1 || [[allowedExtensions objectAtIndex:0] isNotEqualTo:@"app"]) {
            [filePanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSUserDirectory inDomains:NSLocalDomainMask] firstObject]];
        } else {
            [filePanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSApplicationDirectory inDomains:NSLocalDomainMask] firstObject]];
        }
    }
    NSModalResponse buttonReturned = [filePanel runModal];
    if (buttonReturned == NSModalResponseCancel) {
        return nil;
    }
    if ([filePanel URLs] != nil) {
        _firstOpen = false;
        return [[[filePanel URLs] firstObject] path];
    } else {
        return nil;
    }
}

- (void)compileCategoryMenuForOpenPanelAccessoryView {
    [[_accessoryView accessoryViewPopUpButton] removeAllItems];
    for (int i = 0; i < [_softlist count]; i++) {
        NSMenuItem *newItem = [[NSMenuItem alloc] initWithTitle:[[_softlist objectAtIndex:i] category] action:@selector(accessoryViewPopUpButtonChanged:) keyEquivalent:@""];
        [newItem setTarget:self];
        [[[_accessoryView accessoryViewPopUpButton] menu] addItem:[newItem copy]];
    }
    id selection = [self getCurrentlySelectedItem];
    if ([selection class] == [SKApplication class]) {
        selection = [_outlineView parentForItem:selection];
    }
    [[_accessoryView accessoryViewPopUpButton] selectItemAtIndex:[_softlist indexOfObject:selection]];
    _accessoryViewPopUpButtonIndex = [_softlist indexOfObject:selection];
}

- (IBAction)accessoryViewPopUpButtonChanged:(id)sender {
    _accessoryViewPopUpButtonIndex = [[_accessoryView accessoryViewPopUpButton] indexOfSelectedItem];
}

- (NSString *)getDirectoryForAttachmentExport {
    NSOpenPanel *filePanel = [NSOpenPanel openPanel];
    [filePanel setTitle:NSLocalizedString(@"Save multiple attachments", nil)];
    [filePanel setMessage:NSLocalizedString(@"Save attachments to this directory:", nil)];
    [filePanel setCanChooseFiles:NO];
    [filePanel setCanChooseDirectories:YES];
    [filePanel setAllowsMultipleSelection:NO];
    if (_firstOpen) {
        [filePanel setDirectoryURL:[[[NSFileManager defaultManager] URLsForDirectory:NSUserDirectory inDomains:NSLocalDomainMask] firstObject]];
    }
    NSModalResponse buttonReturned = [filePanel runModal];
    if (buttonReturned == NSModalResponseCancel) {
        return nil;
    }
    if ([filePanel URLs] != nil) {
        return [[[filePanel URLs] firstObject] path];
    } else {
        return nil;
    }
}

- (id)getCurrentlySelectedItem {
    return [_outlineView itemAtRow:[_outlineView selectedRow]];
}

- (NSInteger)getIndexOfCurrentlySelectedItem {
    id theSelection = [self getCurrentlySelectedItem];
    if ([[theSelection class] isEqual:[SKApplication class]]) {
        NSUInteger indexOfParent = [_softlist indexOfObject:[_outlineView parentForItem:theSelection]];
        return [[[_softlist objectAtIndex:indexOfParent] applications] indexOfObject:theSelection];
    } else {
        return [_softlist indexOfObject:[_outlineView itemAtRow:[_outlineView selectedRow]]];
    }
}

- (id)getParentOfCurrentlySelectedItem {
    id theSelection = [self getCurrentlySelectedItem];
    return [_outlineView parentForItem:theSelection];
}

- (NSInteger)getIndexOfParentOfCurrentlySelectedItem {
    id theSelection = [self getCurrentlySelectedItem];
    return [_softlist indexOfObject:[_outlineView parentForItem:theSelection]];
}


#pragma mark OutlineView Drag and Drop Methods

- (BOOL)outlineView:(NSOutlineView *)outlineView writeItems:(NSArray *)items toPasteboard:(NSPasteboard *)pasteboard {
    [self dealWithChanges];
    [self quickSaveCurrentSelection];
    if ([[items objectAtIndex:0] class] == [SKCategory class]) return NO;
    _draggedItem = [items objectAtIndex:0];
    if (_searchActive) {
        NSUInteger theParentIndex = [_draggedItem parentIndex];
        NSUInteger theOriginalIndex = [_draggedItem originalIndex];
        SKCategory *draggedParent = [_outlineView parentForItem:_draggedItem];
        SKCategory *origDraggedParent = [_softlist objectAtIndex:[draggedParent originalIndex]];
        _draggedItem = [[origDraggedParent applications] objectAtIndex:[_draggedItem originalIndex]];
        [_draggedItem setOriginalIndex:theOriginalIndex];
        [_draggedItem setParentIndex:theParentIndex];
    }
    [pasteboard declareTypes:@[@"nl.kinoko-house.SKOutlineDragItem", NSFilesPromisePboardType] owner:self];
    [pasteboard setPropertyList:@[@"pdf"] forType:NSFilesPromisePboardType];
    return YES;
}

- (NSDragOperation)outlineView:(NSOutlineView *)outlineView validateDrop:(id<NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(NSInteger)index {
    NSString *theType = [[[[[info draggingPasteboard] pasteboardItems] objectAtIndex:0] types] objectAtIndex:0];
    if (item == nil) {
         return NSDragOperationNone;
    } else if (index == -1) {
        if ([info draggingSourceOperationMask] == NSDragOperationCopy) {
            if ([theType isEqualToString:@"nl.kinoko-house.SKOutlineDragItem"]){
                [[NSCursor dragCopyCursor] set];
            } else {
                [[NSCursor closedHandCursor] set];
            }
        } else if ([info draggingSourceOperationMask] == NSDragOperationMove) {
            [[NSCursor closedHandCursor] set];
        }
        if ([[item class] isEqual:[SKApplication class]]) {
            return NSDragOperationNone;
        } else if ([info draggingSourceOperationMask] == NSDragOperationCopy) {
            return NSDragOperationCopy;
        } else {
            return NSDragOperationMove;
        }
    } else {
        if ([info draggingSourceOperationMask] == NSDragOperationCopy) {
            if ([theType isEqualToString:@"nl.kinoko-house.SKOutlineDragItem"]){
                [[NSCursor dragCopyCursor] set];
            } else {
                [[NSCursor closedHandCursor] set];
            }
            return NSDragOperationCopy;
        } else if ([info draggingSourceOperationMask] == NSDragOperationMove) {
            [[NSCursor closedHandCursor] set];
            return NSDragOperationMove;
        }
    }
    // if all else fails, do a move
    [[NSCursor closedHandCursor] set];
    return NSDragOperationMove;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView acceptDrop:(id<NSDraggingInfo>)info item:(id)item childIndex:(NSInteger)index {
    BOOL isCopyOperation = NO;
    [[NSCursor arrowCursor] set];
    BOOL ourObjectIsOnPasteBoard = [[[[[info draggingPasteboard] pasteboardItems] objectAtIndex:0] types] containsObject:@"nl.kinoko-house.SKOutlineDragItem"];
    [self dealWithChanges];
    if ([[_outlineView parentForItem:_draggedItem] isEqual:item] && ourObjectIsOnPasteBoard && [info draggingSourceOperationMask] == NSDragOperationMove) {
        return NO;
    }
    if ([info draggingSourceOperationMask] == NSDragOperationCopy) {
        isCopyOperation = YES;
        if (ourObjectIsOnPasteBoard && [info draggingSourceOperationMask] == NSDragOperationCopy) {
            NSString *selection = [[_outlineView itemAtRow:[_outlineView selectedRow]] uuid];
            if (!_searchActive) {
                SKApplication *app = [_outlineView itemAtRow:[_outlineView rowForItem:_draggedItem]];
                SKCategory *category = [_outlineView parentForItem:app];
                if ([[_outlineView parentForItem:_draggedItem] isEqual:item]) {
                    SKApplication *newApp = [self duplicateItemAtRow:[_outlineView rowForItem:_draggedItem]];
                    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoRegularDupeApp:newApp toCategory:category selectedItem:selection];
                    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drag Application", nil)];
                    return YES;
                }
            } else {
                NSInteger origCatNumber = [_draggedItem parentIndex];
                NSInteger origAppNumber = [_draggedItem originalIndex];
                SKCategory *dupCat = [_softlist objectAtIndex:origCatNumber];
                SKApplication *dupApp = [[[dupCat applications] objectAtIndex:origAppNumber] copy];
                SKCategory *newCat = [_softlist objectAtIndex:[item originalIndex]];
                [dupApp setUuid:[SKUtilities createUUID]];
                [dupApp setProduct:[SKUtilities getNameForCopy:[dupApp product]]];
                _savedUUID = [dupApp uuid];
                [newCat addApplication:dupApp];
                [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoSearchDupeApp:dupApp toCategory:newCat selectedItem:selection];
                [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drag Application", nil)];
                [self buildFilteredSoftwareList];
                [self reselectItemAfterUndo];
                return YES;
            }
        }
    }
    SKCategory *oldCategory;
    SKCategory *newCategory;
    SKApplication *copiedApp;
    SKApplication *newApp = nil;
    SKApplication *addedApp = nil;
    NSPasteboard *pboard = [info draggingPasteboard];
    if ([pboard availableTypeFromArray:@[@"nl.kinoko-house.SKOutlineDragItem"]]) {
        if (!_searchActive) {
            newCategory = item;
            oldCategory = [_outlineView parentForItem:_draggedItem];
            if ([newCategory isEqualTo:oldCategory]) return NO;
            if (!isCopyOperation) {
                [oldCategory removeApplication:_draggedItem];
            }
        } else {
            newCategory = [_softlist objectAtIndex:[item originalIndex]];
            oldCategory = [_softlist objectAtIndex:[_draggedItem parentIndex]];
            if ([newCategory isEqualTo:oldCategory]) return NO;
            if (!isCopyOperation) {
                [oldCategory removeApplication:[[[_softlist objectAtIndex:[_draggedItem parentIndex]] applications] objectAtIndex:[_draggedItem originalIndex]]];
            }
        }
        copiedApp = [_draggedItem copy];
        if (isCopyOperation) {
            [copiedApp setUuid:[SKUtilities createUUID]];
            [copiedApp setProduct:[SKUtilities getNameForCopy:[copiedApp product]]];
            if (!_searchActive) {
                [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoRegularDupeApp:copiedApp toCategory:newCategory selectedItem:[_draggedItem uuid]];
            } else {
                [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoSearchDupeApp:newApp toCategory:newCategory selectedItem:[_draggedItem uuid]];
            }
            [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drag Application", nil)];
        } else {
            if (!_searchActive) {
                [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoRegularMoveApp:_draggedItem withCopiedApp:copiedApp toCategory:newCategory fromCategory:oldCategory];
            } else {
                [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoSearchMoveApp:_draggedItem withCopiedApp:copiedApp toCategory:newCategory fromCategory:oldCategory];
            }
            [[_outlineView undoManager] setActionName:NSLocalizedString(@"Move Application", nil)];
        }
        [newCategory addApplication:copiedApp];
        if (_searchActive) [self buildFilteredSoftwareList];
        [_outlineView reloadData];
        if (!_searchActive) {
            if ([_outlineView rowForItem:copiedApp] == -1) [_outlineView expandItem:newCategory];
            [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[_outlineView rowForItem:copiedApp]] byExtendingSelection:NO];
            [_outlineView scrollRowToVisible:[_outlineView rowForItem:copiedApp]];
        } else {
            NSString *myUUID = [copiedApp uuid];
            SKApplication *myApp = [self findObjectWithUUID:myUUID];
            [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[_outlineView rowForItem:myApp]] byExtendingSelection:NO];
            [_outlineView scrollRowToVisible:[_outlineView rowForItem:myApp]];
        }
        return YES;
    } else if ([pboard availableTypeFromArray:@[NSFilenamesPboardType]]) {
        if (!_searchActive) {
            SKApplication *firstAppAdded = nil;
            NSArray <NSString *> *allowedExtensions = @[@"app", @"bundle", @"plugin", @"extension", @"component", @"saver"];
            NSArray *fileNames = [pboard propertyListForType:NSFilenamesPboardType];
            NSArray *availableFiles = [fileNames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension IN[c] %@", allowedExtensions]];
            if (!availableFiles) {
                return NO;
            }
            NSMutableArray *newAppArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < [availableFiles count]; i++) {
                newApp = [self addApplicationWithPath:[availableFiles objectAtIndex:i] toCategory:(SKCategory *)item];
                if (newApp) {
                    addedApp = newApp;
                    [newAppArray addObject:[addedApp copy]];
                    if (firstAppAdded == nil) firstAppAdded = addedApp;
                }
            }
            if (addedApp == nil) {
                return NO;
            };
            [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationDragAdd:newAppArray toCategory:item selectedUUID:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid]];
            if ([newAppArray count] < 2) {
                [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drop Application", nil)];
            } else {
                [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drop Applications", nil)];
            }
            [_outlineView reloadData];
            if ([_outlineView rowForItem:addedApp] == -1) [_outlineView expandItem:item];
            [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[_outlineView rowForItem:firstAppAdded]] byExtendingSelection:NO];
            [_outlineView scrollRowToVisible:[_outlineView rowForItem:firstAppAdded]];
            return YES;
        } else {
            return NO;
        }
    }
    return NO;
}

- (NSArray <NSString *> *)outlineView:(NSOutlineView *)outlineView namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination forDraggedItems:(NSArray <SKApplication *> *)items {
    [self dealWithChanges];
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if ([appDelegate usePassword] && [appDelegate preventDetailDrag]) {
        NSBeep();
        return @[];
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *draggedFilenames = [[NSMutableArray alloc] init];
    NSString *path = [dropDestination path];
    NSString *currentExt;
    NSString *currentPath;
    NSString *fullpath;
    SKApplication *theItem = [items firstObject];
    fullpath = [NSString stringWithFormat:@"%@/%@.pdf", path, [theItem product]];
    while ([fileManager fileExistsAtPath:fullpath]) {
        currentExt = [fullpath pathExtension];
        currentPath = [fullpath stringByDeletingPathExtension];
        fullpath = [SKUtilities getNameForCopy:currentPath];
        fullpath = [NSString stringWithFormat:@"%@.%@", fullpath, currentExt];
    }
    [draggedFilenames addObject:[fullpath lastPathComponent]];
    [self preparePrintViewWithObject:theItem];
    [_bottomLine setStringValue:NSLocalizedString(@"Dragged and dropped from SerialKeeper by Kinoko House", nil)];
    NSData *theData = [_printView dataWithPDFInsideRect:[_printView bounds]];
    [theData writeToFile:fullpath atomically:YES];
    return draggedFilenames;
}

- (void)quickSaveCurrentSelection {
    NSInteger theIndex;
    NSInteger theParentIndex;
    if (_changeDetected) {
        id theObject = [self getCurrentlySelectedItem];
        if ([[theObject class] isEqual:[SKCategory class]]) {
            if (!_searchActive) {
                theIndex = [self getIndexOfCurrentlySelectedItem];
            } else {
                if ([theObject category] == nil) return;
                theIndex = [[self getCurrentlySelectedItem] originalIndex];
            }
            if (theIndex == NSNotFound) return;
            [[_softlist objectAtIndex:theIndex] setCategory:[[_categoryName stringValue] copy]];
            [[_softlist objectAtIndex:theIndex] setDescript:[[_categoryDescription string] copy]];
            [self sortCategoriesOnly];
            if (_searchActive) [self buildFilteredSoftwareList];
        } else {
            if (!_searchActive) {
                theIndex = [self getIndexOfCurrentlySelectedItem];
                theParentIndex = [self getIndexOfParentOfCurrentlySelectedItem];
            } else {
                if ([theObject product] == nil) return;
                theIndex = [theObject originalIndex];
                theParentIndex = [theObject parentIndex];
            }
            if (theIndex == NSNotFound || theParentIndex == NSNotFound) return;
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setProduct:[[_applicationProduct stringValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setVersion:[[_applicationVersion stringValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setDescript:[[_applicationDescription string] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setRegname:[[_applicationRegName stringValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setPurchasedate:[[_applicationPurchaseDate dateValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setRegistration:[[_applicationRegkey stringValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setEmail:[[_applicationEMail stringValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setSerial:[[_applicationSerial stringValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setCompany:[[_applicationCompany stringValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setWebsite:[[_applicationWebsite stringValue] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setNotes:[[_applicationNotes string] copy]];
            [(SKApplication *)[[[_softlist objectAtIndex:theParentIndex] applications] objectAtIndex:theIndex] setAttachments:[[_tableViewController attachmentList] mutableCopy]];
        }
        if (!_searchActive) {
            [_outlineView reloadData];
            [_outlineView scrollRowToVisible:[_outlineView selectedRow]];
        }
        _changeDetected = NO;
    }
}


#pragma mark Search Field Methods

- (void)searchFieldHasChanged {
    [self storeUUIDOfLastSelectedItem];
    NSString *myUUID = [_uuidOfLastSelectedItem copy];
    [self updateIfChanged];
    if ([[_searchField stringValue] isEqualToString:@""]) {
        if (_searchActive) {
            _searchActive = NO;
            [_addApplicationMenuItem setEnabled:YES];
            [_addCategoryMenuItem setEnabled:YES];
            [_importMenuItem setEnabled:YES];
            [_exportMenuItem setEnabled:YES];
            [_softlistSegmentedControl setEnabled:YES forSegment:0];
            [_softlistSegmentedControl setEnabled:YES forSegment:1];
            [_addCategoryToolbarItem setEnabled:YES];
            [_addApplicationToolbarItem setEnabled:YES];
            [_outlineView reloadData];
            [self restoreExpansionState];
            _uuidOfLastSelectedItem = myUUID;
            [self reselectOriginalItem];
        }
    } else {
        if (!_searchActive) {
            [self saveExpansionState];
        }
        _searchActive = YES;
        [_addApplicationMenuItem setEnabled:NO];
        [_addCategoryMenuItem setEnabled:NO];
        [_importMenuItem setEnabled:NO];
        [_exportMenuItem setEnabled:NO];
        [_softlistSegmentedControl setEnabled:NO forSegment:0];
        [_softlistSegmentedControl setEnabled:NO forSegment:1];
        [_addCategoryToolbarItem setEnabled:NO];
        [_addApplicationToolbarItem setEnabled:NO];
        [self buildFilteredSoftwareList];
        _uuidOfLastSelectedItem = myUUID;
        [self reselectOriginalItem];
    }
}

- (void)buildFilteredSoftwareList {
    BOOL thereWereHits = NO;
    [self storeUUIDOfLastSelectedItem];
    NSString *searchString = [_searchField stringValue];
    _filteredlist = [[NSMutableArray alloc] init];
    for (int i = 0; i < [_softlist count]; i++) {
        NSMutableArray *newAppArray = [[NSMutableArray alloc] init];
        SKCategory *origCategory = [_softlist objectAtIndex:i];
        NSMutableArray *appArray = [[origCategory applications] mutableCopy];
        for (int j = 0; j < [appArray count]; j++) {
            BOOL foundFlag = NO;
            SKApplication *currentApp = [appArray objectAtIndex:j];
            if ([[currentApp product] contains:searchString]) foundFlag = YES;
            if ([[currentApp descript] contains:searchString]) foundFlag = YES;
            if ([[currentApp email] contains:searchString]) foundFlag = YES;
            if ([[currentApp company] contains:searchString]) foundFlag = YES;
            if ([[currentApp notes] contains:searchString]) foundFlag = YES;
            if (foundFlag) {
                [currentApp setOriginalIndex:j];
                [currentApp setParentIndex:i];
                [newAppArray addObject:[currentApp copy]];
            }
        }
        SKCategory *newCategory = [[SKCategory alloc] init];
        [newCategory setUuid:[origCategory uuid]];
        [newCategory setCategory:[origCategory category]];
        [newCategory setOriginalIndex:i];
        if ([newAppArray count] > 0) {
            [newCategory setApplications:newAppArray];
            thereWereHits = YES;
        } else {
            [newCategory setApplications:[@[] mutableCopy]];
        }
        [_filteredlist addObject:newCategory];
    }
    if (!thereWereHits) {
        _filteredlist = [@[] mutableCopy];
        [_categoryBox setHidden:YES];
        [_applicationBox setHidden:YES];
        [_nothingBox setHidden:NO];
    }
    [_outlineView reloadData];
    for (int k = 0; k < [_filteredlist count]; k++) {
        [_outlineView expandItem:[_filteredlist objectAtIndex:k]];
    }
    [self reselectOriginalItem];
}

- (void)saveExpansionState {
    _expansionState = [self getExpansionState];
}

- (void)restoreExpansionState {
    if (!_expansionState) return;
    if ([_expansionState count] < 1) return;
    for (int i = 0; i < [_softlist count]; i++) {
        if ([[_expansionState objectAtIndex:i] boolValue] == YES) {
            [_outlineView expandItem:[_softlist objectAtIndex:i]];
        } else {
            [_outlineView collapseItem:[_softlist objectAtIndex:i]];
        }
    }
}

- (void)unsetSearchMode {
    if ([[_searchField stringValue] isEqualToString:@""]) return;
    [_searchField setStringValue:@""];
}

- (id)findObjectWithUUID:(NSString *)uuid {
    NSMutableArray <SKCategory *> *theList = _softlist;
    if (_searchActive) theList = _filteredlist;
    for (int i = 0; i < [theList count]; i++) {
        if ([[[theList objectAtIndex:i] uuid] isEqualToString:uuid]) {
            return [theList objectAtIndex:i];
        }
        for (int j = 0; j < [[[theList objectAtIndex:i] applications] count]; j++) {
            if ([[[[[theList objectAtIndex:i] applications] objectAtIndex:j] uuid] isEqualToString:uuid]) {
                return [[[theList objectAtIndex:i] applications] objectAtIndex:j];
            }
        }
    }
    return nil;
}

- (void)storeUUIDOfLastSelectedItem {
    NSString *theUUID = [[_outlineView itemAtRow:[_outlineView selectedRow]] uuid];
    if (theUUID) {
        _uuidOfLastSelectedItem = theUUID;
    }
}

- (void)reselectOriginalItem {
    id theObject = [self findObjectWithUUID:_uuidOfLastSelectedItem];
    if (!theObject) {
        _backhandedUUID = [_uuidOfLastSelectedItem copy];
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(backhandedReselect) userInfo:nil repeats:NO];
        return;
    }
    [self handleObjectReselection:theObject];
}

- (void)backhandedReselect {
    id theObject = [self findObjectWithUUID:_backhandedUUID];
    if (!theObject) {
        [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
        [self storeUUIDOfLastSelectedItem];
        if (!_nonselect) [_outlineView scrollRowToVisible:[_outlineView selectedRow]];
    }
    [self handleObjectReselection:theObject];
}

- (void)reselectItemAfterUndo {
    id theObject = [self findObjectWithUUID:_savedUUID];
    if (!theObject) {
        [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
        [self storeUUIDOfLastSelectedItem];
        if (!_nonselect) [_outlineView scrollRowToVisible:[_outlineView selectedRow]];
    }
    [self handleObjectReselection:theObject];
}
- (void)handleObjectReselection:(id)theObject {
    NSInteger theRow = [_outlineView rowForItem:theObject];
    if (theRow == -1) {
        [_outlineView expandItem:[_softlist objectAtIndex:[theObject parentIndex]]];
    }
    theRow = [_outlineView rowForItem:theObject];
    if (theRow == -1) {
        [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
        [self storeUUIDOfLastSelectedItem];
    } else {
        [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[_outlineView rowForItem:theObject]] byExtendingSelection:NO];
    }
    if (!_nonselect) [_outlineView scrollRowToVisible:[_outlineView selectedRow]];
}


#pragma mark Print Related Methods

- (BOOL)preparePrintViewWithCurrentSelection {
    if ([[[_outlineView itemAtRow:[_outlineView selectedRow]] class] isEqual:[SKCategory class]]) return NO;
    SKApplication *currentSelection = [_outlineView itemAtRow:[_outlineView selectedRow]];
    [self preparePrintViewWithObject:currentSelection];
    return YES;
}

- (void)preparePrintViewWithObject:(SKApplication *)object {
    [_printIconView setImage:[[NSImage alloc] initWithData:[object icon]]];
    [_printProduct setStringValue:[object product]];
    [_printVersion setStringValue:[object version]];
    [_printDescription setStringValue:[object descript]];
    [_printRegKey setStringValue:[object registration]];
    [_printRegName setStringValue:[object regname]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    [_printPurchaseDate setStringValue:[formatter stringFromDate:[object purchasedate]]];
    [_printEmail setStringValue:[object email]];
    [_printSerial setStringValue:[object serial]];
    [_printCompany setStringValue:[object company]];
    [_printWebsite setStringValue:[object website]];
    [_printNotes setStringValue:[object notes]];
}


#pragma mark Contextual Menu Related Methods

- (void)removeItemAtRow:(NSInteger)theRow {
    NSUInteger theSelectedRow = [_outlineView selectedRow];
    if (theSelectedRow == theRow) {
        [self removeSelectedItem];
    } else {
        _nonselect = YES;
        _nonselectRow = theRow;
        [self removeSelectedItem];
        _nonselect = NO;
        if (theSelectedRow > theRow) {
            [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:(theSelectedRow - 1)] byExtendingSelection:NO];
        }
    }
}

- (SKApplication *)duplicateItemAtRow:(NSInteger)theRow {
    NSInteger selection = [_outlineView selectedRow];
    NSUInteger catNumber;
    SKApplication *appCopy;
    if (theRow == selection) {
        SKApplication *application = [_outlineView itemAtRow:selection];
        SKCategory *category = [_outlineView parentForItem:application];
        catNumber = [category originalIndex];
        appCopy = [application copy];
        NSString *newUUID = [SKUtilities createUUID];
        NSString *newName = [SKUtilities getNameForCopy:[appCopy product]];
        [appCopy setUuid:[newUUID copy]];
        [appCopy setProduct:[newName copy]];
        [category addApplication:appCopy];
        _savedUUID = [appCopy uuid];
        [_outlineView reloadData];
        [self reselectItemAfterUndo];
    } else {
        [self storeUUIDOfLastSelectedItem];
        SKApplication *application = [_outlineView itemAtRow:theRow];
        SKCategory *category = [_outlineView parentForItem:application];
        catNumber = [category originalIndex];
        appCopy = [application copy];
        [category addApplication:appCopy];
        NSString *newUUID = [SKUtilities createUUID];
        NSString *newName = [SKUtilities getNameForCopy:[appCopy product]];
        [appCopy setUuid:[newUUID copy]];
        [appCopy setProduct:[newName copy]];
        [_outlineView reloadData];
        _uuidOfLastSelectedItem = newUUID;
        _nonselect = YES;
        [self reselectOriginalItem];
        _nonselect = NO;
    }
    if (_searchActive) {
        [[_softlist objectAtIndex:catNumber] addApplication:appCopy];
    }
    return appCopy;
}

- (void)selectItemAtRow:(NSInteger)theRow {
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:theRow] byExtendingSelection:NO];
}

- (void)editItemAtRow:(NSInteger)theRow {
    if ([_outlineView selectedRow] != theRow) {
        [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:theRow] byExtendingSelection:NO];
    }
    [_outlineView editColumn:0 row:theRow withEvent:nil select:YES];
}

- (void)shakeThisWindow:(NSWindow *)aWindow {
    _windowToShake = aWindow;
    [self shakeItUp];
}

- (void)shakeItUp {
    if (!_shakeTimer) {
        NSInteger posDelta = [[_shakeList objectAtIndex:_shakeCounter] integerValue];
        NSRect theFrame = [_windowToShake frame];
        theFrame.origin.x += posDelta;
        [_windowToShake setFrame:theFrame display:YES];
        _shakeCounter++;
        _shakeTimer = [NSTimer scheduledTimerWithTimeInterval:_timingInterval target:self selector:(@selector(shakeItUp)) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_shakeTimer forMode:NSModalPanelRunLoopMode];
    } else {
        NSInteger posDelta = [[_shakeList objectAtIndex:_shakeCounter] integerValue];
        NSRect theFrame = [_windowToShake frame];
        theFrame.origin.x += posDelta;
        [_windowToShake setFrame:theFrame display:YES];
        _shakeCounter++;
        if (_shakeCounter == [_shakeList count]) {
            [_shakeTimer invalidate];
            _shakeTimer = nil;
            _shakeCounter = 0;
            return;
        }
    }
}


#pragma mark OutlineView row editing


- (IBAction)editingJustEnded:(id)sender {
    NSString *newName = [sender stringValue];
    id theItem = [_outlineView itemAtRow:[_outlineView selectedRow]];
    if ([[theItem class] isEqual:[SKCategory class]]) {
        [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoCategoryRename:theItem withPreviousName:_previousItemName wasRenamedTo:newName];
        [[_outlineView undoManager] setActionName:NSLocalizedString(@"Rename Category", nil)];
        [theItem setCategory:newName];
        [_categoryName setStringValue:newName];
        [self sortCategoriesOnly];
        if (_searchActive) {
            for (SKCategory *cat in _softlist) {
                if ([[cat uuid] isEqualToString:[theItem uuid]]) {
                    theItem = cat;
                    [cat setCategory:newName];
                    [self sortCategoriesOnly];
                    break;
                }
            }
        }
    } else {
        [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationRename:theItem withPreviousName:_previousItemName wasRenamedTo:newName];
        [[_outlineView undoManager] setActionName:NSLocalizedString(@"Rename Application", nil)];
        [theItem setProduct:newName];
        SKCategory *theCategory;
        NSString *parentUUID = [[_outlineView parentForItem:theItem] uuid];
        [_applicationProduct setStringValue:newName];
        [theCategory sortApplications];
        if (_searchActive) {
            for (SKCategory *cat in _softlist) {
                if ([[cat uuid] isEqualToString:parentUUID]) {
                    theCategory = cat;
                    for (SKApplication *app in [theCategory applications]) {
                        if ([[app uuid] isEqualToString:[theItem uuid]]) {
                            [app setProduct:newName];
                            [theCategory sortApplications];
                            break;
                        }
                    }
                    break;
                }
            }
        }
        [(SKCategory *)[_outlineView parentForItem:theItem] sortApplications];
    }
    _changeDetected = YES;
    [_outlineView reloadData];
    [self storeUUIDOfLastSelectedItem];
    [self updateIfChanged];
    [self reselectOriginalItem];
}


#pragma mark Undo Manager Methods

- (void)undoRegularMoveApp:(SKApplication *)origApp withCopiedApp:(SKApplication *)copiedApp toCategory:(SKCategory *)newCategory fromCategory:(SKCategory *)oldCategory {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoRegularMoveApp:origApp withCopiedApp:copiedApp toCategory:newCategory fromCategory:oldCategory];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Move Item", nil)];
    [oldCategory addApplication:origApp];
    [_outlineView expandItem:oldCategory];
    [newCategory removeApplicationWithUUID:[copiedApp uuid]];
    [_outlineView reloadData];
    _savedUUID = [origApp uuid];
    [self reselectItemAfterUndo];
}

- (void)redoRegularMoveApp:(SKApplication *)origApp withCopiedApp:(SKApplication *)copiedApp toCategory:(SKCategory *)newCategory fromCategory:(SKCategory *)oldCategory {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoRegularMoveApp:origApp withCopiedApp:copiedApp toCategory:newCategory fromCategory:oldCategory];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Move Item", nil)];
    [oldCategory removeApplicationWithUUID:[origApp uuid]];
    [newCategory addApplication:copiedApp];
    [_outlineView expandItem:newCategory];
    [_outlineView reloadData];
    _savedUUID = [copiedApp uuid];
    [self reselectItemAfterUndo];
}

- (void)undoSearchMoveApp:(SKApplication *)origApp withCopiedApp:(SKApplication *)copiedApp toCategory:(SKCategory *)newCategory fromCategory:(SKCategory *)oldCategory {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoSearchMoveApp:origApp withCopiedApp:copiedApp toCategory:newCategory fromCategory:oldCategory];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Move Item", nil)];
    [oldCategory addApplication:origApp];
    [_outlineView expandItem:oldCategory];
    [newCategory removeApplicationWithUUID:[copiedApp uuid]];
    [self buildFilteredSoftwareList];
    [_outlineView reloadData];
    _savedUUID = [origApp uuid];
    [self reselectItemAfterUndo];
}

- (void)redoSearchMoveApp:(SKApplication *)origApp withCopiedApp:(SKApplication *)copiedApp toCategory:(SKCategory *)newCategory fromCategory:(SKCategory *)oldCategory {
   [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoSearchMoveApp:origApp withCopiedApp:copiedApp toCategory:newCategory fromCategory:oldCategory];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Move Item", nil)];
    [oldCategory removeApplicationWithUUID:[origApp uuid]];
    [newCategory addApplication:copiedApp];
    [self buildFilteredSoftwareList];
    [_outlineView reloadData];
    _savedUUID = [copiedApp uuid];
    [self reselectItemAfterUndo];
}

- (void)undoRegularDupeApp:(SKApplication *)application toCategory:(SKCategory *)category selectedItem:(NSString *)theUUID {
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoRegularDupeApp:application toCategory:category selectedItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Duplicate Item", nil)];
    [category removeApplicationWithUUID:[application uuid]];
    [_outlineView expandItem:category];
    [_outlineView reloadData];
    [self reselectItemAfterUndo];
}

- (void)redoRegularDupeApp:(SKApplication *)application toCategory:(SKCategory *)category selectedItem:(NSString *)theUUID {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoRegularDupeApp:application toCategory:category selectedItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Duplicate Item", nil)];
    [category addApplication:application];
    [_outlineView expandItem:category];
    _savedUUID = [application uuid];
    [_outlineView reloadData];
    [self reselectItemAfterUndo];
}

- (void)undoSearchDupeApp:(SKApplication *)application toCategory:(SKCategory *)category selectedItem:(NSString *)theUUID {
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoSearchDupeApp:application toCategory:category selectedItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Duplicate Item", nil)];
    [category removeApplicationWithUUID:[application uuid]];
    [self buildFilteredSoftwareList];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoSearchDupeApp:(SKApplication *)application toCategory:(SKCategory *)category selectedItem:(NSString *)theUUID {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoSearchDupeApp:application toCategory:category selectedItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Duplicate Item", nil)];
    [category addApplication:application];
    _savedUUID = [application uuid];
    [self buildFilteredSoftwareList];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoApplicationDragAdd:(NSMutableArray *)appArray toCategory:(SKCategory *)category selectedUUID:(NSString *)theUUID {
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoApplicationDragAdd:(NSMutableArray *)appArray toCategory:category selectedUUID:theUUID];
    if ([appArray count] < 2) {
        [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drop Application", nil)];
    } else {
        [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drop Applications", nil)];
    }
    for (SKApplication *app in appArray) {
        [category removeApplicationWithUUID:[app uuid]];
    }
    [_outlineView expandItem:category];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoApplicationDragAdd:(NSMutableArray *)appArray toCategory:(SKCategory *)category selectedUUID:(NSString *)theUUID {
    _savedUUID = [(SKApplication *)[appArray objectAtIndex:0] uuid];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationDragAdd:(NSMutableArray *)appArray toCategory:category selectedUUID:theUUID];
    if ([appArray count] < 2) {
        [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drop Application", nil)];
    } else {
        [[_outlineView undoManager] setActionName:NSLocalizedString(@"Drop Applications", nil)];
    }
    [category addMultipleApplications:appArray];
    [_outlineView expandItem:category];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoImportAppFolderWithSoftwareList:(NSMutableArray *)softwareList reselectItem:(NSString *)theUUID {
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoImportAppFolderWithSoftwareList:(NSMutableArray *)softwareList reselectItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Import App Folder", nil)];
    _softlist = softwareList;
    [_outlineView expandItem:nil];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoImportAppFolderWithSoftwareList:(NSMutableArray *)softwareList reselectItem:(NSString *)theUUID{
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoImportAppFolderWithSoftwareList:(NSMutableArray *)softwareList reselectItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Import App Folder", nil)];
    _softlist = softwareList;
    [_outlineView expandItem:nil];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoDatabaseMergeWithSoftwareList:(NSMutableArray *)softwareList reselectItem:(NSString *)theUUID {
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoDatabaseMergeWithSoftwareList:_softlist reselectItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Merge Database", nil)];
    _softlist = softwareList;
    [_outlineView expandItem:nil];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoDatabaseMergeWithSoftwareList:(NSMutableArray *)softwareList reselectItem:(NSString *)theUUID{
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoDatabaseMergeWithSoftwareList:_softlist reselectItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Merge Database", nil)];
    _softlist = softwareList;
    [_outlineView expandItem:nil];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoDatabaseImportWithSoftwareList:(NSMutableArray *)softwareList reselectItem:(NSString *)theUUID {
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoDatabaseImportWithSoftwareList:_softlist reselectItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Import Database", nil)];
    _softlist = softwareList;
    [_outlineView expandItem:nil];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoDatabaseImportWithSoftwareList:(NSMutableArray *)softwareList reselectItem:(NSString *)theUUID{
    _savedUUID = theUUID;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoDatabaseImportWithSoftwareList:_softlist reselectItem:theUUID];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Import Database", nil)];
    _softlist = softwareList;
    [_outlineView expandItem:nil];
    [_outlineView reloadData];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoCategoryRename:(SKCategory *)category withPreviousName:(NSString *)previousName wasRenamedTo:(NSString *)newName {
    _savedUUID = [category uuid];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoCategoryRename:category withPreviousName:_previousItemName wasRenamedTo:newName];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Rename Category", nil)];
    [category setCategory:previousName];
    [self sortCategoriesOnly];
    if (_searchActive) {
        for (SKCategory *cat in _softlist) {
            if ([[cat uuid] isEqualToString:[category uuid]]) {
                [cat setCategory:previousName];
                [self sortCategoriesOnly];
                [self buildFilteredSoftwareList];
                break;
            }
        }
    } else {
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoCategoryRename:(SKCategory *)category withPreviousName:(NSString *)previousName wasRenamedTo:(NSString *)newName {
    _savedUUID = [category uuid];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoCategoryRename:category withPreviousName:_previousItemName wasRenamedTo:newName];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Rename Category", nil)];
    [category setCategory:newName];
    [self sortCategoriesOnly];
    if (_searchActive) {
        for (SKCategory *cat in _softlist) {
            if ([[cat uuid] isEqualToString:[category uuid]]) {
                [cat setCategory:newName];
                [self sortCategoriesOnly];
                [self buildFilteredSoftwareList];
                break;
            }
        }
    } else {
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoApplicationRename:(SKApplication *)application withPreviousName:(NSString *)previousName wasRenamedTo:(NSString *)newName {
    _savedUUID = [application uuid];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoApplicationRename:application withPreviousName:_previousItemName wasRenamedTo:newName];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Rename Application", nil)];
    SKCategory *category = [_outlineView parentForItem:application];
    [application setProduct:previousName];
    [category sortApplications];
    if (_searchActive) {
        NSString *parentUUID = [[_outlineView parentForItem:application] uuid];
        SKCategory *theCategory;
        for (SKCategory *cat in _softlist) {
            if ([[cat uuid] isEqualToString:parentUUID]) {
                theCategory = cat;
                for (SKApplication *app in [theCategory applications]) {
                    if ([[app uuid] isEqualToString:[application uuid]]) {
                        [app setProduct:previousName];
                        [theCategory sortApplications];
                        break;
                    }
                }
                break;
            }
        }
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView expandItem:category];
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoApplicationRename:(SKApplication *)application withPreviousName:(NSString *)previousName wasRenamedTo:(NSString *)newName {
    _savedUUID = [application uuid];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationRename:application withPreviousName:_previousItemName wasRenamedTo:newName];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Rename Application", nil)];
    SKCategory *category = [_outlineView parentForItem:application];
    [application setProduct:newName];
    [category sortApplications];
    if (_searchActive) {
        NSString *parentUUID = [[_outlineView parentForItem:application] uuid];
        SKCategory *theCategory;
        for (SKCategory *cat in _softlist) {
            if ([[cat uuid] isEqualToString:parentUUID]) {
                theCategory = cat;
                for (SKApplication *app in [theCategory applications]) {
                    if ([[app uuid] isEqualToString:[application uuid]]) {
                        [app setProduct:newName];
                        [theCategory sortApplications];
                        break;
                    }
                }
                break;
            }
        }
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView expandItem:category];
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoApplicationAdd:(SKApplication *)application toCategory:(SKCategory *)category selectedUUID:(NSString *)uuid {
    _savedUUID = uuid;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoApplicationAdd:application toCategory:category selectedUUID:uuid];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Application", nil)];
    [category removeApplication:application];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView expandItem:category];
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoApplicationAdd:(SKApplication *)application toCategory:(SKCategory *)category selectedUUID:(NSString *)uuid {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationAdd:application toCategory:category selectedUUID:uuid];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Application", nil)];
    [category addApplication:application];
    _savedUUID = [application uuid];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView expandItem:category];
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoItemAdd:(SKApplication *)item toCategory:(SKCategory *)category selectedUUID:(NSString *)uuid {
    _savedUUID = uuid;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoItemAdd:item toCategory:category selectedUUID:uuid];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Item", nil)];
    [category removeApplication:item];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView expandItem:category];
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoItemAdd:(SKApplication *)item toCategory:(SKCategory *)category selectedUUID:(NSString *)uuid {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoItemAdd:item toCategory:category selectedUUID:uuid];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Item", nil)];
    [category addApplication:item];
    _savedUUID = [item uuid];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView expandItem:category];
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}
- (void)undoCategoryAdd:(SKCategory *)category selectedUUID:(NSString *)uuid {
    _savedUUID = uuid;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoCategoryAdd:category selectedUUID:uuid];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Category", nil)];
    [_softlist removeObject:category];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoCategoryAdd:(SKCategory *)category selectedUUID:(NSString *)uuid {
    _savedUUID = [category uuid];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoCategoryAdd:category selectedUUID:uuid];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Add Category", nil)];
    [_softlist addObject:category];
    [self sortCategoriesOnly];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView reloadData];
    }
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoCategoryDelete:(NSMutableArray *)softlist expansionState:(NSMutableArray *)expansionState selectedUUID:(NSString *)uuid {
    _expansionState = [self getExpansionState];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoCategoryDelete:[_softlist copy] expansionState:[_expansionState copy] selectedUUID:[uuid copy]];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove Category", nil)];
    _softlist = [softlist mutableCopy];
    _expansionState = [expansionState mutableCopy];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView reloadData];
        [self restoreExpansionState];
    }
    _savedUUID = [uuid copy];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoCategoryDelete:(NSMutableArray *)softlist expansionState:(NSMutableArray *)expansionState selectedUUID:(NSString *)uuid {
    _expansionState = [self getExpansionState];
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoCategoryDelete:[_softlist copy] expansionState:[_expansionState copy] selectedUUID:[uuid copy]];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove Category", nil)];
    _softlist = [softlist mutableCopy];
    _expansionState = [expansionState mutableCopy];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView reloadData];
        [self restoreExpansionState];
    }
    _savedUUID = [uuid copy];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)undoApplicationDelete:(SKApplication *)application fromCategory:(SKCategory *)category selectedUUID:(NSString *)uuid {
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] redoApplicationDelete:application fromCategory:category selectedUUID:uuid];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove Application", nil)];
    [category addApplication:application];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView expandItem:category];
        [_outlineView reloadData];
    }
    _savedUUID = [uuid copy];
    [self performSelector:@selector(reselectItemAfterUndo) withObject:nil afterDelay:0.05f];
}

- (void)redoApplicationDelete:(SKApplication *)application fromCategory:(SKCategory *)category selectedUUID:(NSString *)uuid {
    NSUInteger newSelRow;
    [[[_outlineView undoManager] prepareWithInvocationTarget:self] undoApplicationDelete:application fromCategory:category selectedUUID:uuid];
    [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove Application", nil)];
    NSUInteger selRow = [_outlineView selectedRow];
    if ([[(SKApplication *)[_outlineView itemAtRow:selRow] uuid] isEqualToString:uuid]) {
        newSelRow = selRow - 1;
        if (newSelRow < 0) newSelRow = 0;
    } else {
        newSelRow = selRow;
    }
    [category removeApplication:application];
    if (_searchActive) {
        [self buildFilteredSoftwareList];
    } else {
        [_outlineView expandItem:category];
        [_outlineView reloadData];
    }
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:newSelRow] byExtendingSelection:NO];
    [_outlineView scrollRowToVisible:newSelRow];
}


#pragma mark Pre-read Preferences

- (void)getCardinalPrefs {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *basicDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @NO,                         @"usePassword",
                                   @NO,                         @"useEncryption",
                                   @"",                         @"userHash",
                                   nil];
    [userDefaults registerDefaults:basicDefaults];
    _usePassword = [userDefaults boolForKey:@"usePassword"];
    _useEncryption = [userDefaults boolForKey:@"useEncryption"];
    _userHash = [userDefaults stringForKey:@"userHash"];
    if (_useEncryption && !_usePassword) _usePassword = YES;
    if ([_userHash isEqualToString:@""] && !_useEncryption) {
        _usePassword = NO;
    }
}

@end
