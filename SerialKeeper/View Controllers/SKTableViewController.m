//
//  SKTableViewController.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 07-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKTableViewController.h"
#import "SKTableView.h"
#import "AppDelegate.h"


unsigned long long const LargeFileSize = 2048192;

@interface SKTableViewController()

@property __unsafe_unretained IBOutlet SKTableView *tableView;
@property __unsafe_unretained IBOutlet SKOutlineView *outlineView;
@property __unsafe_unretained IBOutlet SKOutlineViewController *outlineViewController;

@property (strong) NSMutableArray <NSURL *> *qlSelection;

@end

@implementation SKTableViewController

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_attachmentList count];
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    return [_attachmentList objectAtIndex:row];
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row {
    NSTableCellView *cell;
    AppDelegate *appDelegate = [NSApp delegate];
    if ([appDelegate preventQuickLookForAttachments]) {
        cell = [tableView makeViewWithIdentifier:@"defaultCell" owner:self];
    } else {
        cell = [tableView makeViewWithIdentifier:@"quickLookCell" owner:self];
    }
    [[cell textField] setStringValue:[(SKAttachment *)[_attachmentList objectAtIndex:row] filename]];
    NSString *extension = [[(SKAttachment *)[_attachmentList objectAtIndex:row] filename] pathExtension];
    [[cell imageView] setImage:[[NSWorkspace sharedWorkspace] iconForFileType:extension]];
    return cell;
}

- (void)updateAttachmentList:(NSMutableArray *)updatedAttachmentList {
    _attachmentList = [updatedAttachmentList copy];
    [_tableView reloadData];
}

- (NSInteger)selectedRow {
    return [_tableView selectedRow];
}

- (SKAttachment *)getCurrentlySelectedObject {
    return [_attachmentList objectAtIndex:[_tableView selectedRow]];
}

- (void)addAttachmentExternal:(SKAttachment *)attachment {
    NSIndexSet *selectedRows = [_tableView selectedRowIndexes];
    SKApplication *application = [_outlineView itemAtRow:[_outlineView selectedRow]];
    SKCategory *category = [_outlineView parentForItem:application];
    [[[_tableView undoManager] prepareWithInvocationTarget:self] undoAddAttachmentsForApplication:application fromCategory:category withUUID:[application uuid] andAttachmentList:_attachmentList withPreviousSelection:selectedRows];
    [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Document", nil)];
    NSMutableArray *newArray = [_attachmentList mutableCopy];
    [newArray addObject:attachment];
    _attachmentList = [newArray mutableCopy];
    [_tableView reloadData];
}

- (void)addAttachment:(SKAttachment *)attachment {
    NSMutableArray *newArray = [_attachmentList mutableCopy];
    [newArray addObject:attachment];
    _attachmentList = [newArray mutableCopy];
}

- (void)removeCurrentlySelectedAttachments {
    if ([_tableView selectedRow] < 0) return;
    NSIndexSet *selectedRows = [_tableView selectedRowIndexes];
    NSMutableIndexSet *setToRemove = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [_attachmentList count])];
    [setToRemove removeIndexes:selectedRows];
    NSMutableArray *removedItems = [_attachmentList mutableCopy];
    [removedItems removeObjectsAtIndexes:setToRemove];
    id appDelegate = [NSApp delegate];
    SKAttachment *attachment = [_attachmentList objectAtIndex:[selectedRows firstIndex]];
    NSString *theFullName = [attachment filename];
    NSString *theShortName = [theFullName truncate];
    NSImage *theImage = [[NSImage alloc] init];
    theImage = [[NSWorkspace sharedWorkspace] iconForFileType:[theFullName pathExtension]];
    NSString *msgText = [NSString stringWithFormat:NSLocalizedString(@"Remove document '%@'", nil), theShortName];
    NSString *infText = [NSString stringWithFormat:NSLocalizedString(@"This will remove document '%@'. Are you sure you want to continue?", nil), theFullName];
    if ([selectedRows count] > 1) {
        msgText = NSLocalizedString(@"Remove documents", nil);
        infText = NSLocalizedString(@"This will remove the selected documents. Are you sure you want to continue?", nil);
    }
    if ([[appDelegate valueForKey:@"userConfirmationForDelete"] boolValue] == YES) {
        NSAlert *theAlert = [NSAlert alertWithMessageText:msgText defaultButton:NSLocalizedString(@"Cancel", nil) alternateButton:nil otherButton:NSLocalizedString(@"OK", nil) informativeTextWithFormat:@"%@", infText];
        [theAlert setIcon:theImage];
        NSModalResponse theResponse = [theAlert runModal];
        if (theResponse == NSAlertDefaultReturn) {
            return;
        }
    }
    SKApplication *application = [_outlineView itemAtRow:[_outlineView selectedRow]];
    SKCategory *category = [_outlineView parentForItem:application];
    NSString *uuid = [application uuid];
    NSMutableArray *newArray = [_attachmentList mutableCopy];
    [[[_tableView undoManager] prepareWithInvocationTarget:self] undoRemoveAttachmentsForApplication:application fromCategory:category withUUID:uuid andAttachmentList:[newArray copy] withPreviousSelection:[selectedRows copy]];
    if ([selectedRows count] > 1) {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Documents", nil)];
    } else {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Document", nil)];
    }

    [[[_tableView undoManager] prepareWithInvocationTarget:self] undoRemoveAttachmentsForApplication:application fromCategory:category withUUID:[application uuid] andAttachmentList:_attachmentList withPreviousSelection:selectedRows
     ];
    if ([removedItems count] == 1) {
        [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove document", nil)];
    } else {
        [[_outlineView undoManager] setActionName:NSLocalizedString(@"Remove documents", nil)];

    }
    [newArray removeObjectsAtIndexes:selectedRows];
    _attachmentList = [newArray mutableCopy];
    [_tableView reloadData];
}


#pragma mark NSTableView Drag and Drop Methods

- (BOOL)tableView:(NSTableView *)tableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard {
    NSString *currentFileExt;
    NSMutableArray *extArray = [[NSMutableArray alloc] init];
    _draggedItems = [_attachmentList objectsAtIndexes:rowIndexes];
    [pboard declareTypes:@[NSFilesPromisePboardType] owner:self];
    for (int i = 0; i < [_draggedItems count]; i++) {
        currentFileExt = [[[_draggedItems objectAtIndex:i] filename] pathExtension];
        if ([currentFileExt isNotEqualTo: @""] || currentFileExt != nil) {
            [extArray addObject:currentFileExt];
        }
    }
    [pboard setPropertyList:extArray forType:NSFilesPromisePboardType];
    return YES;
}

- (NSDragOperation)tableView:(NSTableView *)tableView validateDrop:(id<NSDraggingInfo>)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)dropOperation {
    NSPasteboard *pboard = [info draggingPasteboard];
    if ([pboard availableTypeFromArray:@[NSFilenamesPboardType]]) {
        if (dropOperation == NSTableViewDropAbove) {
            return NSDragOperationCopy;
        }
    }
    return NSDragOperationNone;
}

- (BOOL)tableView:(NSTableView *)tableView acceptDrop:(id<NSDraggingInfo>)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)dropOperation {
    NSIndexSet *selectedRows = [_tableView selectedRowIndexes];
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    unsigned long long fileSize;
    NSString *message;
    NSString *inform;
    NSString *attname;
    NSData *attachment;
    NSPasteboard *pboard = [info draggingPasteboard];
    NSArray *fileNames = [pboard propertyListForType:NSFilenamesPboardType];
    BOOL overlyLarge = NO;
    for (int i = 0; i < [fileNames count]; i++) {
        fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:[fileNames objectAtIndex:i] error:nil] fileSize];
        if (fileSize > LargeFileSize) overlyLarge = YES;
    }
    if (overlyLarge == YES) {
        if ([fileNames count] == 1) {
            message = NSLocalizedString(@"Add large attachment", nil);
            inform = [NSString stringWithFormat:NSLocalizedString(@"You are trying to add an attachment that is larger than %llu bytes. Are you sure you want to do this?", nil), LargeFileSize];
        } else {
            message = @"Add large attachments";
            inform = [NSString stringWithFormat:NSLocalizedString(@"The attachments that you are trying to add include one or more files that are larger than %llu bytes. Are you sure you want to do this?", nil), LargeFileSize];
        }
        NSAlert *theAlert = [NSAlert alertWithMessageText:message defaultButton:NSLocalizedString(@"OK", nil) alternateButton:nil otherButton:NSLocalizedString(@"Cancel", nil) informativeTextWithFormat:@"%@", inform];
        NSModalResponse theResponse = [theAlert runModal];
        if (theResponse == NSAlertOtherReturn) {
            return NO;
        }
    }
    SKApplication *application = [_outlineView itemAtRow:[_outlineView selectedRow]];
    SKCategory *category = [_outlineView parentForItem:application];
    [[[_tableView undoManager] prepareWithInvocationTarget:self] undoAddAttachmentsForApplication:application fromCategory:category withUUID:[application uuid] andAttachmentList:_attachmentList withPreviousSelection:selectedRows];
    if ([fileNames count] == 1) {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Document", nil)];
    } else {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Documents", nil)];
    }
    for (int j = 0; j < [fileNames count]; j++) {
        attname = [[fileNames objectAtIndex:j] lastPathComponent];
        attachment = [[NSData alloc] initWithContentsOfFile:[fileNames objectAtIndex:j]];
        SKAttachment *theAttachment = [[SKAttachment alloc] initWithFilename:attname document:attachment];
        [self addAttachment:theAttachment];
    }
    [_tableView reloadData];
    [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:([_tableView numberOfRows] - 1)] byExtendingSelection:NO];
    [_tableView scrollRowToVisible:[_tableView selectedRow]];
    if (!appDelegate) return NO;
    SEL selector = NSSelectorFromString(@"setChangeDetected");
    IMP imp = [appDelegate methodForSelector:selector];
    void (*func)(id, SEL) = (void *)imp;
    func(appDelegate, selector);
    return YES;
}

- (NSArray <NSString *> *)tableView:(NSTableView *)tableView namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination forDraggedRowsWithIndexes:(NSIndexSet *)indexSet {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if ([appDelegate usePassword] && [appDelegate preventDetailDrag]) {
        NSBeep();
        return @[];
    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *draggedFilenames = [[NSMutableArray alloc] init];
    NSString *path = [dropDestination path];
    NSString *currentExt;
    NSString *currentPath;
    NSString *fullpath;
    NSData *theData;
    for (int i = 0; i < [_draggedItems count]; i++) {
        fullpath = [NSString stringWithFormat:@"%@/%@", path, [[_draggedItems objectAtIndex:i] filename]];
        while ([fileManager fileExistsAtPath:fullpath]) {
            currentExt = [fullpath pathExtension];
            currentPath = [fullpath stringByDeletingPathExtension];
            fullpath = [SKUtilities getNameForCopy:currentPath];
            fullpath = [NSString stringWithFormat:@"%@.%@", fullpath, currentExt];
        }
        [draggedFilenames addObject:[[[_draggedItems objectAtIndex:i] filename] copy]];
        theData = [[_draggedItems objectAtIndex:i] document];
        [theData writeToFile:fullpath atomically:YES];
    }
    return draggedFilenames;
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSUInteger theSelection = [[_tableView selectedRowIndexes] firstIndex];
    if (theSelection == NSNotFound) {
        [appDelegate disableRemoveMenuItem];
    } else {
        [appDelegate enableRemoveMenuItem];
    }
    [[self tableView] updateQLPanelIfNecessary];
}


#pragma mark Undo Manager

- (void)undoRemoveAttachmentsForApplication:(SKApplication *)application fromCategory:(SKCategory *)category withUUID:(NSString *)uuid andAttachmentList:(NSMutableArray *)attachmentList withPreviousSelection:(NSIndexSet *)previousSelection {
    NSString *currentlySelectedUUID = [[_outlineView itemAtRow:[_outlineView selectedRow]] uuid];
    if (![currentlySelectedUUID isEqualToString:uuid]) {
        [_outlineView expandItem:category];
        [_outlineViewController setSavedUUID:uuid];
        [_outlineViewController reselectItemAfterUndo];
    }
    [[[_tableView undoManager] prepareWithInvocationTarget:self] redoRemoveAttachmentsForApplication:application fromCategory:category withUUID:uuid andAttachmentList:[_attachmentList copy] previousSelectionToRemember:[previousSelection copy]];
    if ([previousSelection count] > 1) {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Documents", nil)];
    } else {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Document", nil)];
    }
    NSArray *params = [[NSArray alloc] initWithObjects:attachmentList, previousSelection, nil];
    [self performSelector:@selector(continueUndoRemove:) withObject:params afterDelay:0.05f];
}

- (void)continueUndoRemove:(NSArray *)params {
    NSMutableArray *attachmentList = [[params objectAtIndex:0] mutableCopy];
    NSIndexSet *previousSelection = [params objectAtIndex:1];
    _attachmentList = [attachmentList copy];
    [_tableView reloadData];
    [_tableView selectRowIndexes:[previousSelection copy] byExtendingSelection:NO];
    [_tableView scrollRowToVisible:[previousSelection firstIndex]];
    [_outlineViewController setChangeDetected:YES];
}

- (void)redoRemoveAttachmentsForApplication:(SKApplication *)application fromCategory:(SKCategory *)category withUUID:(NSString *)uuid andAttachmentList:(NSMutableArray *)attachmentList previousSelectionToRemember:(NSIndexSet *)previousSelection {
    NSString *currentlySelectedUUID = [[_outlineView itemAtRow:[_outlineView selectedRow]] uuid];
    if (![currentlySelectedUUID isEqualToString:uuid]) {
        [_outlineView expandItem:category];
        [_outlineViewController setSavedUUID:uuid];
        [_outlineViewController reselectItemAfterUndo];
    }
    [[[_tableView undoManager] prepareWithInvocationTarget:self] undoRemoveAttachmentsForApplication:application fromCategory:category withUUID:uuid andAttachmentList:[_attachmentList copy] withPreviousSelection:[previousSelection copy]];
    if ([previousSelection count] > 1) {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Documents", nil)];
    } else {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Remove Document", nil)];
    }
    [self performSelector:@selector(continueRedoRemove:) withObject:attachmentList afterDelay:0.05f];
}

- (void)continueRedoRemove:(NSMutableArray *)attachmentList {
    _attachmentList = [attachmentList copy];
    [_tableView reloadData];
    [_outlineViewController setChangeDetected:YES];
}

- (void)undoAddAttachmentsForApplication:(SKApplication *)application fromCategory:(SKCategory *)category withUUID:(NSString *)uuid andAttachmentList:(NSMutableArray *)attachmentList withPreviousSelection:(NSIndexSet *)previousSelection {
    NSString *currentlySelectedUUID = [[_outlineView itemAtRow:[_outlineView selectedRow]] uuid];
    if (![currentlySelectedUUID isEqualToString:uuid]) {
        [_outlineView expandItem:category];
        [_outlineViewController setSavedUUID:uuid];
        [_outlineViewController reselectItemAfterUndo];
    }
    [[[_tableView undoManager] prepareWithInvocationTarget:self] redoAddAttachmentsForApplication:application fromCategory:category withUUID:[application uuid] andAttachmentList:_attachmentList withPreviousSelection:[previousSelection copy]];
    if ([previousSelection count] > 1) {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Add Documents", nil)];
    } else {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Add Document", nil)];
    }
    NSArray *params = [[NSArray alloc] initWithObjects:attachmentList, previousSelection, nil];
    [self performSelector:@selector(continueUndoAdd:) withObject:params afterDelay:0.05f];
}

- (void)continueUndoAdd:(NSArray *)params {
    NSMutableArray *attachmentList = [[params objectAtIndex:0] mutableCopy];
    NSIndexSet *previousSelection = [params objectAtIndex:1];
    _attachmentList = [attachmentList copy];
    [_tableView reloadData];
    [_tableView selectRowIndexes:[previousSelection copy] byExtendingSelection:NO];
    [_outlineViewController setChangeDetected:YES];
}

- (void)redoAddAttachmentsForApplication:(SKApplication *)application fromCategory:(SKCategory *)category withUUID:(NSString *)uuid andAttachmentList:(NSMutableArray *)attachmentList withPreviousSelection:(NSIndexSet *)previousSelection {
    NSString *currentlySelectedUUID = [[_outlineView itemAtRow:[_outlineView selectedRow]] uuid];
    if (![currentlySelectedUUID isEqualToString:uuid]) {
        [_outlineView expandItem:category];
        [_outlineViewController setSavedUUID:uuid];
        [_outlineViewController reselectItemAfterUndo];
    }
    [[[_tableView undoManager] prepareWithInvocationTarget:self] undoAddAttachmentsForApplication:application fromCategory:category withUUID:uuid andAttachmentList:[_attachmentList copy] withPreviousSelection:[previousSelection copy]];
    if ([previousSelection count] > 1) {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Add Documents", nil)];
    } else {
        [[_tableView undoManager] setActionName:NSLocalizedString(@"Add Document", nil)];
    }
    NSArray *params = [[NSArray alloc] initWithObjects:attachmentList, previousSelection, nil];
    [self performSelector:@selector(continueRedoAdd:) withObject:params afterDelay:0.05f];
}

- (void)continueRedoAdd:(NSMutableArray *)params {
    _attachmentList = [[params objectAtIndex:0] mutableCopy];
    NSIndexSet *previousSelection = [params objectAtIndex:1];
    [_tableView reloadData];
    [_tableView selectRowIndexes:[previousSelection copy] byExtendingSelection:NO];
    [_tableView scrollRowToVisible:[previousSelection lastIndex]];
    [_outlineViewController setChangeDetected:YES];
}

- (void)clearUndoQueueForAttachmentlist {
    [[_tableView undoManager] removeAllActions];
}


#pragma mark QLPreviewPanel Data Source

- (NSInteger)numberOfPreviewItemsInPreviewPanel:(QLPreviewPanel *)panel {
    return [[_tableView selectedRowIndexes] count];
}

- (id <QLPreviewItem>)previewPanel:(QLPreviewPanel *)panel previewItemAtIndex:(NSInteger)index {
    return [_qlSelection objectAtIndex:index];
}

- (NSURL *)exportQLFileToTempDir:(SKAttachment *)attachment {
    NSError *error = nil;
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"SerialKeeper"];
    NSString *pathToMyQLTempFileDir = [pathToMyAppSupportDir stringByAppendingPathComponent:@"QuickLook"];
    BOOL folderExists = [fileManager fileExistsAtPath:pathToMyQLTempFileDir isDirectory:&isDir];
    if (isDir == NO || !folderExists) {
        return nil;
    }
    NSString *path = [pathToMyQLTempFileDir stringByAppendingPathComponent:[attachment filename]];
    NSString *currentExt;
    NSString *currentPath;
    NSData *theData;
    while ([fileManager fileExistsAtPath:path]) {
        currentExt = [path pathExtension];
        currentPath = [path stringByDeletingPathExtension];
        path = [SKUtilities getNameForCopy:currentPath];
        path = [NSString stringWithFormat:@"%@.%@", path, currentExt];
    }
    theData = [attachment document];
    [theData writeToFile:path atomically:YES];
    return [NSURL fileURLWithPath:path];
}

#pragma mark QLPreviewPanel Utilities

- (void)initiateQLPanel {
    NSURL *url;
    if (_qlSelection) {
        [_qlSelection removeAllObjects];
    } else {
        _qlSelection = [[NSMutableArray alloc] init];
    }
    NSIndexSet *selectedRows = [_tableView selectedRowIndexes];
    if ([selectedRows count] == 0) return;
    NSInteger currentRow = [selectedRows firstIndex];
    do {
        SKAttachment *attachment = [_attachmentList objectAtIndex:currentRow];
        url = [self exportQLFileToTempDir:attachment];
        [_qlSelection addObject:url];
        currentRow = [selectedRows indexGreaterThanIndex:currentRow];
    } while (currentRow != NSNotFound);
}

- (void)cleanUpQLPanel {
    NSURL *url;
    for (url in _qlSelection) {
        NSString *path = [url path];
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
    if (_qlSelection) {
        [_qlSelection removeAllObjects];
    } else {
        _qlSelection = [[NSMutableArray alloc] init];
    }
}

- (void)deleteAllQLFilesIfPresent {
    NSError *error = nil;
    NSString *file;
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"SerialKeeper"];
    NSString *pathToMyQLTempFileDir = [pathToMyAppSupportDir stringByAppendingPathComponent:@"QuickLook"];
    BOOL folderExists = [fileManager fileExistsAtPath:pathToMyQLTempFileDir isDirectory:&isDir];
    if (isDir == YES && folderExists) {
        NSDirectoryEnumerator *enumerator = [fileManager enumeratorAtPath:pathToMyQLTempFileDir];
        while (file = [enumerator nextObject]) {
            NSError *error = nil;
            [fileManager removeItemAtPath:[pathToMyQLTempFileDir stringByAppendingPathComponent:file] error:&error];
        }
    }
}

- (IBAction)togglePreviewPanel:(id)sender {
    [[_tableView window] makeFirstResponder:_tableView];
    NSUInteger row = [_tableView rowForView:[sender superview]];
    NSIndexSet *selection = [_tableView selectedRowIndexes];
    if ((![selection containsIndex:row]) || [selection count] == 0) {
        [_tableView selectRowIndexes:[NSIndexSet indexSetWithIndex:row] byExtendingSelection:NO];
    }
    [_tableView togglePreviewPanel:sender];
}

@end
