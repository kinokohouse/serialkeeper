//
//  SKOutlineViewController.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 28-12-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <objc/message.h>

#import "SKIconView.h"
#import "SKCategory.h"
#import "SKApplication.h"
#import "SKAttachment.h"
#import "SKCategoryCellView.h"
#import "SKTableViewController.h"
#import "SKAccessoryView.h"
#import "NSString+StringSearch.h"
#import "NSString+MD5.h"
#import "RNEncryptor.h"
#import "RNDecryptor.h"

@interface SKOutlineViewController : NSObject <NSOutlineViewDataSource, NSOutlineViewDelegate, NSOpenSavePanelDelegate, NSTextFieldDelegate, NSTextViewDelegate>


#pragma mark Instance Variables

@property BOOL firstOpen;
@property BOOL firstSave;
@property BOOL changeDetected;
@property BOOL wasCancelled;
@property BOOL importState;
@property (strong) NSArray <NSString *> *allowedExtensions;
@property (retain) NSMutableArray *softlist;
@property (retain) NSMutableArray *filteredlist;
@property (strong) id draggedItem;
@property (assign) NSInteger accessoryViewPopUpButtonIndex;
@property BOOL searchActive;
@property (strong) NSMutableArray *expansionState;
@property (assign) BOOL startTour;
@property (assign) BOOL initDeferred;
@property (assign) BOOL isEncrypted;
@property (strong) NSData *encryptedObject;
@property (strong) NSMutableArray *gatheredApps;
@property (assign) NSUInteger importDatabasePopUpIndex;
@property (strong) NSMutableArray *categories;
@property (strong) NSString *uuidOfLastSelectedItem;
@property (strong) NSString *backhandedUUID;
@property (assign) BOOL nonselect;
@property (assign) NSInteger nonselectRow;
@property (strong) id theObject;


#pragma mark Undo Manager

@property (strong) NSString *savedUUID;
@property (strong) NSString *previousItemName;

#pragma mark IBOutlets for own objects

@property __unsafe_unretained IBOutlet NSOutlineView *outlineView;
@property __unsafe_unretained IBOutlet NSScrollView *scrollView;


#pragma mark IBOutlets for other shared objects

@property __unsafe_unretained IBOutlet NSWindow *window;
@property __unsafe_unretained IBOutlet SKAccessoryView *accessoryView;
@property __unsafe_unretained IBOutlet NSPopUpButton *customItemCategoryPopUpButton;
@property __unsafe_unretained IBOutlet NSSegmentedControl *softlistSegmentedControl;
@property __unsafe_unretained IBOutlet NSButton *importFromSubfolders;
@property __unsafe_unretained IBOutlet NSTextField *importDatabaseProgressText;
@property __unsafe_unretained IBOutlet NSProgressIndicator *importDatabaseProgressIndicator;
@property __unsafe_unretained IBOutlet NSButton *importDatabaseStartButton;
@property __unsafe_unretained IBOutlet NSButton *importDatabaseCancelButton;
@property __unsafe_unretained IBOutlet NSPopUpButton *importDatabasePopUpButton;
@property __unsafe_unretained IBOutlet NSButton *importSKDatabaseRadioButton;
@property __unsafe_unretained IBOutlet NSButton *importRSDatabaseRadioButton;
@property __unsafe_unretained IBOutlet NSMenuItem *addApplicationMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *addCategoryMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *importMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *exportMenuItem;

#pragma mark IBOutlets for Category Box

@property __unsafe_unretained IBOutlet NSBox *categoryBox;
@property __unsafe_unretained IBOutlet NSTextField *categoryName;
@property (assign) IBOutlet NSTextView *categoryDescription;


#pragma mark IBOutlets for Application Box

@property __unsafe_unretained IBOutlet NSBox *applicationBox;
@property __unsafe_unretained IBOutlet SKIconView *applicationIcon;
@property __unsafe_unretained IBOutlet NSTextField *applicationProduct;
@property __unsafe_unretained IBOutlet NSTextField *applicationVersion;
@property (assign) IBOutlet NSTextView *applicationDescription;
@property __unsafe_unretained IBOutlet NSTextField *applicationRegkey;
@property __unsafe_unretained IBOutlet NSTextField *applicationRegName;
@property __unsafe_unretained IBOutlet NSDatePicker *applicationPurchaseDate;
@property __unsafe_unretained IBOutlet NSTextField *applicationEMail;
@property __unsafe_unretained IBOutlet NSTextField *applicationSerial;
@property __unsafe_unretained IBOutlet NSTextField *applicationCompany;
@property __unsafe_unretained IBOutlet NSTextField *applicationWebsite;
@property (assign) IBOutlet NSTextView *applicationNotes;
@property __unsafe_unretained IBOutlet NSTableView *applicationTableView;


#pragma mark IBOutlets for Nothing Box

@property __unsafe_unretained IBOutlet NSBox *nothingBox;


#pragma mark IBOutlets for Multi Import Box

@property __unsafe_unretained IBOutlet NSTextField *importProgressText;
@property __unsafe_unretained IBOutlet NSProgressIndicator *importProgressIndicator;
@property __unsafe_unretained IBOutlet NSButton *importStartButton;


#pragma mark IBOutlets for Toolbar

@property __unsafe_unretained IBOutlet NSTextField *searchField;
@property __unsafe_unretained IBOutlet NSToolbarItem *addCategoryToolbarItem;
@property __unsafe_unretained IBOutlet NSToolbarItem *addApplicationToolbarItem;

#pragma mark Menu IBOutlets

@property __unsafe_unretained IBOutlet NSMenuItem *duplicateMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *changeIconMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *printSetupMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *printMenuItem;


#pragma mark Password Sheet IBOutlets

@property __unsafe_unretained IBOutlet NSSecureTextField *secureTextField;


#pragma mark Print View IBOutlets

@property __unsafe_unretained IBOutlet NSView *printView;
@property __unsafe_unretained IBOutlet SKIconView *printIconView;
@property __unsafe_unretained IBOutlet NSTextField *printProduct;
@property __unsafe_unretained IBOutlet NSTextField *printVersion;
@property __unsafe_unretained IBOutlet NSTextField *printDescription;
@property __unsafe_unretained IBOutlet NSTextField *printRegKey;
@property __unsafe_unretained IBOutlet NSTextField *printRegName;
@property __unsafe_unretained IBOutlet NSTextField *printPurchaseDate;
@property __unsafe_unretained IBOutlet NSTextField *printEmail;
@property __unsafe_unretained IBOutlet NSTextField *printSerial;
@property __unsafe_unretained IBOutlet NSTextField *printCompany;
@property __unsafe_unretained IBOutlet NSTextField *printWebsite;
@property __unsafe_unretained IBOutlet NSTextField *printNotes;
@property __unsafe_unretained IBOutlet NSTextField *bottomLine;


#pragma mark Wrong Password Feedback

@property (strong) NSArray *shakeList;
@property (assign) NSUInteger shakeCounter;
@property (strong) NSTimer *shakeTimer;
@property (assign) float timingInterval;
@property (strong) NSWindow *windowToShake;


#pragma mark Methods for Creating and Removing Items

- (void)addSingleApplication:(id)object;
- (void)addSomethingElse:(id)object;
- (void)createNewCategory;
- (void)removeSelectedItem;


#pragma mark Properties for previewing cardinal preferences

@property (assign) BOOL usePassword;
@property (assign) BOOL useEncryption;
@property (strong) NSString *userHash;


#pragma mark Utility Methods

- (void)displayCurrentSelection;
- (NSMutableArray *)getExpansionState;
- (NSRect)getScrollviewState;
- (void)saveDatabase;
- (void)importSerialKeeperDatabase;
- (void)importRapidoSerialDatabase;
- (void)exportSelectedDocuments;
- (NSString *)showLoadDialogForFileExtensions:(NSArray <NSString *> *)allowedExtension;
- (id)getCurrentlySelectedItem;
- (NSInteger)getIndexOfCurrentlySelectedItem;
- (id)getParentOfCurrentlySelectedItem;
- (NSInteger)getIndexOfParentOfCurrentlySelectedItem;
- (BOOL)updateIfChanged;
- (void)importAppFolderAppsWithCategoryIndex:(NSInteger)index;
- (void)setMultiImportCancelFlag:(id)sender;
- (void)restoreExpansionState;
- (void)searchFieldHasChanged;
- (void)unsetSearchMode;
- (void)loadDatabaseDeferred;
- (void)continueDeferredLoad;
- (BOOL)preparePrintViewWithCurrentSelection;
- (void)preparePrintViewWithObject:(SKApplication *)object;
- (void)dealWithChanges;
- (void)buildFilteredSoftwareList;
- (void)storeUUIDOfLastSelectedItem;
- (id)findObjectWithUUID:(NSString *)uuid;
- (void)reselectOriginalItem;
- (void)reselectItemAfterUndo;
- (void)quickSaveCurrentSelection;
- (void)removeItemAtRow:(NSInteger)theRow;
- (SKApplication *)duplicateItemAtRow:(NSInteger)theRow;
- (void)selectItemAtRow:(NSInteger)theRow;
- (void)editItemAtRow:(NSInteger)theRow;
- (void)shakeThisWindow:(NSWindow *)aWindow;

@end
