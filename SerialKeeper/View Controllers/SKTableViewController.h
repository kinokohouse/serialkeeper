//
//  SKTableViewController.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 07-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>

#import "SKCategory.h"
#import "SKAttachment.h"
#import "SKOutlineViewController.h"
#import "NSString+StringSearch.h"

@interface SKTableViewController : NSViewController <NSTableViewDataSource, NSTableViewDelegate, QLPreviewPanelDataSource>

@property (retain) NSMutableArray *attachmentList;
@property (strong) NSArray <SKAttachment *> *draggedItems;
@property (strong) NSString *pathToMyQLScratchFolder;


- (NSInteger)selectedRow;
- (SKAttachment *)getCurrentlySelectedObject;
- (void)addAttachmentExternal:(SKAttachment *)attachment;
- (void)updateAttachmentList:(NSMutableArray *)updatedAttachmentList;
- (void)removeCurrentlySelectedAttachments;
- (void)initiateQLPanel;
- (void)cleanUpQLPanel;
- (void)deleteAllQLFilesIfPresent;
- (IBAction)togglePreviewPanel:(id)sender;

@end
