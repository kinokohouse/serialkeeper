//
//  SKItemPrinter.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 23-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "SKOutlineViewController.h"

@interface SKItemPrinter : NSObject

@property __unsafe_unretained IBOutlet NSView *printView;
@property __unsafe_unretained IBOutlet NSTextField *bottomLine;
@property __unsafe_unretained IBOutlet NSWindow *window;
@property __unsafe_unretained IBOutlet SKOutlineViewController *outlineViewController;

- (IBAction)pagesetup:(id)sender;
- (IBAction)print:(id)sender;

@end
