//
//  AppDelegate.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 25-12-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import "AppDelegate.h"
#import "NSString+MD5.h"

@interface AppDelegate () <NSSplitViewDelegate>

#pragma mark General IBOutlets
@property __unsafe_unretained IBOutlet NSToolbar *toolBar;
@property __unsafe_unretained IBOutlet NSToolbarItem *searchSpot;
@property __unsafe_unretained IBOutlet NSView *searchView;
@property __unsafe_unretained IBOutlet NSTextField *searchField;
@property __unsafe_unretained IBOutlet NSToolbarItem *addCategory;
@property __unsafe_unretained IBOutlet NSToolbarItem *addApplication;
@property __unsafe_unretained IBOutlet SKIconView *icon;
@property __unsafe_unretained IBOutlet SKIconView *categoryIcon;
@property __unsafe_unretained IBOutlet SKIconView *nothingIcon;
@property __unsafe_unretained IBOutlet SKIconView *multiIcon;
@property __unsafe_unretained IBOutlet SKIconView *introIcon;
@property __unsafe_unretained IBOutlet SKBox *categoryBox;
@property __unsafe_unretained IBOutlet SKBox *applicationBox;
@property __unsafe_unretained IBOutlet SKOutlineViewController *outlineViewController;
@property __unsafe_unretained IBOutlet SKTableViewController *tableViewController;
@property __unsafe_unretained IBOutlet NSDatePicker *datePicker;
@property __unsafe_unretained IBOutlet SKOutlineView *outlineView;
@property __unsafe_unretained IBOutlet NSScrollView *scrollView;
@property __unsafe_unretained IBOutlet NSTableView *tableView;
@property __unsafe_unretained IBOutlet NSButton *importFromSubfolders;

#pragma mark Add Item related IBOutlets
@property __unsafe_unretained IBOutlet NSWindow *addItemSheet;
@property __unsafe_unretained IBOutlet SKIconView *addItemIcon;
@property __unsafe_unretained IBOutlet NSButton *addSingleAppRadioButton;
@property __unsafe_unretained IBOutlet NSButton *addAllAppsRadioButton;
@property __unsafe_unretained IBOutlet NSButton *addAnotherItemRadioButton;

#pragma mark Add Item related properties
@property (strong) NSString *selectedAddItemRadioButton;

#pragma mark Import related IBOutlets
@property __unsafe_unretained IBOutlet NSWindow *importSheet;
@property __unsafe_unretained IBOutlet NSWindow *importMultiSheet;
@property __unsafe_unretained IBOutlet NSPopUpButton *categoryPopUpButton;
@property __unsafe_unretained IBOutlet NSTextField *importProgressText;
@property __unsafe_unretained IBOutlet NSProgressIndicator *importProgressIndicator;
@property __unsafe_unretained IBOutlet NSButton *importCancelButton;
@property __unsafe_unretained IBOutlet NSButton *importStartButton;
@property __unsafe_unretained IBOutlet SKIconView *importMultiIcon;
@property __unsafe_unretained IBOutlet SKIconView *importIcon;
@property __unsafe_unretained IBOutlet NSButton *importSKDatabaseRadioButton;
@property __unsafe_unretained IBOutlet NSButton *importRSDatabaseRadioButton;
@property __unsafe_unretained IBOutlet SKAccessoryView *accessoryView;
@property __unsafe_unretained IBOutlet NSPopUpButton *customItemCategoryPopUpButton;
@property __unsafe_unretained IBOutlet NSImageView *customIconImageWell;
@property __unsafe_unretained IBOutlet NSImageView *changeIconImageWell;
@property __unsafe_unretained IBOutlet NSWindow *changeIconSheet;
@property __unsafe_unretained IBOutlet NSProgressIndicator *importDatabaseProgressIndicator;
@property __unsafe_unretained IBOutlet NSButton *importDatabaseStartButton;
@property __unsafe_unretained IBOutlet NSButton *importDatabaseCancelButton;
@property __unsafe_unretained IBOutlet NSTextField *importDatabaseProgressText;
@property __unsafe_unretained IBOutlet NSPopUpButton *importDatabasePopUpButton;

#pragma mark Import related properties
@property (strong) NSString *selectedImportRadioButton;
@property (assign) NSInteger selectedCategoryIndex;

#pragma mark Export related IBOutlets
@property __unsafe_unretained IBOutlet NSWindow *exportSheet;
@property __unsafe_unretained IBOutlet SKIconView *exportIcon;
@property __unsafe_unretained IBOutlet NSButton *exportSKDatabaseRadioButton;
@property __unsafe_unretained IBOutlet NSButton *exportCSVRadioButton;

#pragma mark Export related properties
@property (strong) NSString *selectedExportRadioButton;

#pragma mark First Time Tour related IBOutlets
@property __unsafe_unretained IBOutlet NSWindow *introWindow;
@property __unsafe_unretained IBOutlet NSPopover *tour1View;
@property __unsafe_unretained IBOutlet NSTextField *tour1text1;
@property __unsafe_unretained IBOutlet NSTextField *tour1text2;
@property __unsafe_unretained IBOutlet NSTextField *tour1text3;
@property __unsafe_unretained IBOutlet NSPopover *tour2View;
@property __unsafe_unretained IBOutlet NSPopover *tour3View;
@property __unsafe_unretained IBOutlet NSTextField *tour3text2;

#pragma mark Menu IBOutlets
@property __unsafe_unretained IBOutlet NSMenuItem *skMainMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *fileMainMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *editMainMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *windowMainMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *helpMainMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *duplicateMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *changeIconMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *printSetupMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *printMenuItem;
@property __unsafe_unretained IBOutlet NSMenuItem *removeMenuItem;

#pragma mark Visual Theme support
@property __unsafe_unretained IBOutlet NSSegmentedControl *softwareListSegmentedControl;
@property __unsafe_unretained IBOutlet NSSegmentedControl *attachmentListSegmentedControl;
@property (strong) NSDistributedNotificationCenter *dnc;

#pragma mark Window and SplitView resizing IBOutlets
@property __unsafe_unretained IBOutlet NSImageView *dragbarImage;

#pragma mark Window and SplitView resizing properties
@property (strong) NSNotificationCenter *nc;
@property (assign) BOOL isResizing;

@end


@implementation AppDelegate


#pragma mark Application Delegate methods


- (void)applicationWillFinishLaunching:(NSNotification *)notification {
    if (@available(macOS 10.11, *)) {
        [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"NSFullScreenMenuItemEverywhere": @NO}];
    }
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    _alreadyAskedOnce = NO;
    _isResizing = NO;
    NSImage *image = [NSImage imageNamed:@"I8Folder"];
    [image setTemplate:YES];
    [_softwareListSegmentedControl setImage:image forSegment:0];
    image = [NSImage imageNamed:@"I8Add"];
    [image setTemplate:YES];
    [_softwareListSegmentedControl setImage:image forSegment:1];
    [_attachmentListSegmentedControl setImage:image forSegment:0];
    image = [NSImage imageNamed:@"I8Remove"];
    [image setTemplate:YES];
    [_softwareListSegmentedControl setImage:image forSegment:2];
    [_attachmentListSegmentedControl setImage:image forSegment:1];
    image = [NSImage imageNamed:@"I8Retrieve"];
    [image setTemplate:YES];
    [_attachmentListSegmentedControl setImage:image forSegment:2];
    image = [NSImage imageNamed:@"I8Bars"];
    [image setTemplate:YES];
    [_dragbarImage setImage:image];
    if (@available(macOS 10.9, *)) {
        [_outlineView setSelectionHighlightStyle:NSTableViewSelectionHighlightStyleSourceList];
        [_outlineView setRowHeight:36.0f];
        [_outlineView setIntercellSpacing:NSMakeSize(0.0f, 1.0f)];
    } else {
        [_scrollView setDrawsBackground:YES];
    }
    _previousPrefs = [[NSMutableDictionary alloc] init];
    _nc = [NSNotificationCenter defaultCenter];
    [_nc addObserver:self selector:@selector(splitViewWasResized) name:NSSplitViewDidResizeSubviewsNotification object:nil];
    [_addApplication setImage:[SKUtilities iconWithSystemReference:@"GenericApplicationIcon"]];
    [_categoryBox setHidden:YES];
    [_searchSpot setView:_searchView];
    [_searchSpot setMinSize:NSMakeSize(192, 32)];
    [_searchSpot setLabel:NSLocalizedString(@"Find item", nil)];
    [_searchSpot setAutovalidates:NO];
    [_searchSpot setEnabled:YES];
    [self getAndSetWindowSizePrefsOnly];
    [self splitViewWasResized];
    [self allowInteraction];
    if ([_outlineViewController initDeferred]) {
        [_window makeKeyAndOrderFront:self];
        [_outlineViewController loadDatabaseDeferred];
    } else {
        [self getPrefs];
        _currentlyActiveLanguage = _preferredLanguage;
        [self continueFinishLaunching];
    }
}

- (void)continueFinishLaunchingFromDeferredLoad {
    [self getPrefs];
    _currentlyActiveLanguage = _preferredLanguage;
    _useEncryption = [_outlineViewController isEncrypted];
    [_useEncryptionCheckBox setState:_useEncryption];
    _usePassword = YES;
    [_usePasswordCheckBox setState:_usePassword];
    [self continueFinishLaunching];
}

- (void)continueFinishLaunching {
    NSImage *image;
    [_window setStyleMask:[_window styleMask] | NSWindowStyleMaskResizable];
    [_outlineViewController setInitDeferred:NO];
    [_addCategory setImage:[SKUtilities iconWithSystemReference:@"GenericFolderIcon"]];
    [_icon setImage:[NSImage imageNamed:@"SKAppIcon"]];
    [_categoryIcon setImage:[SKUtilities iconWithSystemReference:@"GenericFolderIcon"]];
    [_nothingIcon setImage:[NSImage imageNamed:@"SKAppIcon"]];
    [_multiIcon setImage:[NSImage imageNamed:@"SKAppIcon"]];
    [_addItemIcon setImage:[NSImage imageNamed:@"SKAppIcon"]];
    [_introIcon setImage:[NSImage imageNamed:@"SKAppIcon"]];
    [_importIcon setImage:[NSImage imageNamed:@"SKAppIcon"]];
    [_exportIcon setImage:[NSImage imageNamed:@"SKAppIcon"]];
    [_importMultiIcon setImage:[NSImage imageNamed:@"SKAppIcon"]];
    _selectedAddItemRadioButton = @"000";
    [_outlineView registerForDraggedTypes:[NSArray arrayWithObjects:
                                           NSFilenamesPboardType, NSFilesPromisePboardType,
                                           @"nl.kinoko-house.SKOutlineDragItem", nil]];
    [_tableView registerForDraggedTypes:[NSArray arrayWithObjects:
                                         NSFilenamesPboardType, NSFilesPromisePboardType, nil]];
    [_outlineView setDraggingSourceOperationMask:NSDragOperationCopy forLocal:NO];
    [_tableView setDraggingSourceOperationMask:NSDragOperationCopy forLocal:NO];
    [_outlineViewController displayCurrentSelection];
    NSArray *extArray = [NSArray arrayWithObjects:@"app", @"", @"framework", @"saver", nil];
    NSArray *outArray = [NSArray arrayWithObjects:@"GenericApplicationIcon", @"GenericDocumentIcon", @"KEXT", @"", nil];
    for (int i = 2; i < 6; i++) {
        if (@available(macOS 10.9, *)) {
            image = [[NSWorkspace sharedWorkspace] iconForFileType:[extArray objectAtIndex:(i - 2)]];
        } else {
            if ([[outArray objectAtIndex:(i - 2)] isEqualToString:@""]) {
                image = [[NSWorkspace sharedWorkspace] iconForFileType:[extArray objectAtIndex:(i - 2)]];
            } else {
                image = [SKUtilities iconWithSystemReference:[outArray objectAtIndex:(i - 2)]];
            }
        }
        NSImage *otherImage = [image copy];
        [image setSize:NSMakeSize(12, 12)];
        [otherImage setSize:NSMakeSize(16, 16)];
        [[_iconPopUpButton itemAtIndex:i] setImage:[image copy]];
        [[_changeIconPopUpButton itemAtIndex:i] setImage:[otherImage copy]];
    }
    [_outlineViewController setSearchActive:NO];
    if ([_outlineViewController startTour] == YES) {
        CGFloat xPos = NSWidth([[_window screen] visibleFrame])/2 - NSWidth([_window frame])/2;
        CGFloat yPos = NSHeight([[_window screen] visibleFrame])/2 - NSHeight([_window frame])/2;
        [_window setFrame:NSMakeRect(xPos, yPos, NSWidth([_window frame]), NSHeight([_window frame])) display:YES];
        [self startTour];
    } else {
        if (![_outlineViewController initDeferred]) {
            [_window makeKeyAndOrderFront:self];
        }
    }
    [_tableViewController deleteAllQLFilesIfPresent];
    [self scrollAndReselect];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag {
    if (!flag) {
        [_window makeKeyAndOrderFront:nil];
    }
    return YES;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    if ([_outlineViewController searchActive]) {
        [_outlineViewController unsetSearchMode];
    }
    if (![_outlineViewController initDeferred]) {
        [self savePrefs];
        [_outlineViewController updateIfChanged];
        [_outlineViewController saveDatabase];
    }
    [_dnc removeObserver:self name:@"AppleInterfaceThemeChangedNotification" object:nil];
    [_nc removeObserver:self name:NSSplitViewDidResizeSubviewsNotification object:nil];
    [_tableViewController deleteAllQLFilesIfPresent];
}


#pragma mark Preferences related methods

- (void)storePreviousPrefs {
    if (!_password) {
        [_previousPrefs setObject:@"" forKey:@"password"];
    } else {
        [_previousPrefs setObject:[_password copy] forKey:@"password"];
    }
    [_previousPrefs setObject:[NSNumber numberWithBool:_usePassword] forKey:@"usePassword"];
    [_previousPrefs setObject:[NSNumber numberWithBool:_useEncryption] forKey:@"useEncryption"];
    if (_password) {
        _userHash = [_password MD5String];
    } else {
        _userHash = @"";
    }
    [_previousPrefs setObject:_userHash forKey:@"userHash"];
    [_previousPrefs setObject:[NSNumber numberWithInt:_preferredLanguage] forKey:@"preferredLanguage"];
    [_previousPrefs setObject:[NSNumber numberWithBool:_preventDetailDrag] forKey:@"preventDetailDrag"];
    [_previousPrefs setObject:[NSNumber numberWithBool:_userConfirmationForDelete] forKey:@"userConfirmationForDelete"];
    [_previousPrefs setObject:[NSNumber numberWithBool:_preventQuickLookForAttachments] forKey:@"preventQuickLookForAttachments"];
    [_previousPrefs setObject:[NSNumber numberWithBool:_saveAfterEveryChange] forKey:@"saveAfterEveryChange"];
    [_previousPrefs setObject:[_customLabelRegkey copy] forKey:@"customLabelRegkey"];
    [_previousPrefs setObject:[_customLabelName copy] forKey:@"customLabelName"];
    [_previousPrefs setObject:[_customLabelEmail copy] forKey:@"customLabelEmail"];
    [_previousPrefs setObject:[_customLabelSerial copy] forKey:@"customLabelSerial"];
}

- (void)restorePreviousPrefs {
    _password = [[_previousPrefs objectForKey:@"password"] copy];
    if ([_password isEqualToString:@""]) _password = nil;
    _usePassword = [[_previousPrefs objectForKey:@"usePassword"] boolValue];
    _useEncryption = [[_previousPrefs objectForKey:@"useEncryption"] boolValue];
    _userHash = [_previousPrefs objectForKey:@"userHash"];
    _preferredLanguage = [[_previousPrefs objectForKey:@"preferredLanguage"] intValue];
    _preventDetailDrag = [[_previousPrefs objectForKey:@"preventDetailDrag"] boolValue];
    _userConfirmationForDelete = [[_previousPrefs objectForKey:@"userConfirmationForDelete"] boolValue];
    _preventQuickLookForAttachments = [[_previousPrefs objectForKey:@"preventQuickLookForAttachments"] boolValue];
    _saveAfterEveryChange = [[_previousPrefs objectForKey:@"saveAfterEveryChange"] boolValue];
    _customLabelRegkey = [[_previousPrefs objectForKey:@"customLabelRegkey"] copy];
    _customLabelName = [[_previousPrefs objectForKey:@"customLabelName"] copy];
    _customLabelEmail = [[_previousPrefs objectForKey:@"customLabelEmail"] copy];
    _customLabelSerial = [[_previousPrefs objectForKey:@"customLabelSerial"] copy];
    [self applyPrefs];
}

- (IBAction)showPrefSheet:(id)sender {
    if (!_interactionAllowed) return;
    [_outlineViewController dealWithChanges];
    [self storePreviousPrefs];
    [_preventDetailDragCheckBox setEnabled:_usePassword];
    [_preventDetailDragBlurb setEnabled:_usePassword];
    [_preventQuickLookForAttachmentsCheckBox setEnabled:_usePassword];
    [_preventQuickLookForAttachmentsBlurb setEnabled:_usePassword];
    [NSApp beginSheet:_prefSheet modalForWindow:_window modalDelegate:nil didEndSelector:nil contextInfo:nil];
}

- (IBAction) prefsOKClicked:(id)sender {
    int prefLang = (int)[_languageSelectionPopUpButton indexOfSelectedItem];
    if ((prefLang != _currentlyActiveLanguage) && !_alreadyAskedOnce) {
        NSAlert *restartAlert = [NSAlert alertWithMessageText:NSLocalizedString(@"Selected Language Has Changed", nil) defaultButton:NSLocalizedString(@"Restart Now", nil) alternateButton:NSLocalizedString(@"Cancel", nil) otherButton:NSLocalizedString(@"Do Not Restart", nil) informativeTextWithFormat:NSLocalizedString(@"You have set the application language to a different language than is currently set, but this requires a relaunch of the application. Do you wish to restart to have the new setting take effect immediately?\n", nil)];
        NSModalResponse response = [restartAlert runModal];
        if (response == 1) {
            [self setPrefs];
            [self removePrefSheet];
            [self restartNow];
        } else if (response == -1) {
            _alreadyAskedOnce = YES;
            [self setPrefs];
            [self removePrefSheet];
        } else {
            return;
        }
    } else {
        [self setPrefs];
        [self removePrefSheet];
    }
}

- (IBAction)prefsCancelClicked:(id)sender {
    [self restorePreviousPrefs];
    [self removePrefSheet];
}

- (IBAction)checkAskedOnce:(id)sender {
    int selection = (int)[(NSPopUpButton *)sender indexOfSelectedItem];
    if ((_currentlyActiveLanguage != selection) || (_preferredLanguage != selection)) {
        _alreadyAskedOnce = NO;
    }
}

- (void)removePrefSheet {
    if ([[_outlineView itemAtRow:[_outlineView selectedRow]] isKindOfClass:[SKApplication class]]) {
        NSIndexSet *selection = [_tableView selectedRowIndexes];
        [_tableView reloadData];
        [_tableView selectRowIndexes:selection byExtendingSelection:NO];
    }
    [NSApp endSheet:_prefSheet];
    [_prefSheet orderOut:self];
}

- (IBAction)usePasswordClicked:(id)sender {
    if ([(NSButton *)sender state] == NO) {
        [_removePWSecureTextField setStringValue:@""];
        [_removePasswordWindow setTitle:NSLocalizedString(@"Remove Password", nil)];
        [_removePasswordWindow makeFirstResponder:_removePWSecureTextField];
        [_altMessageLabel setStringValue:@""];
        NSModalResponse theResult = [NSApp runModalForWindow:_removePasswordWindow];
        if (theResult == NSModalResponseCancel) {
            [_usePasswordCheckBox setState:YES];
            [_useEncryptionCheckBox setEnabled:YES];
            [_preventDetailDragCheckBox setEnabled:YES];
            [_preventDetailDragBlurb setEnabled:YES];
            [_preventQuickLookForAttachmentsCheckBox setEnabled:YES];
            [_preventQuickLookForAttachmentsBlurb setEnabled:YES];
            return;
        }
        if (theResult == NSModalResponseOK) {
            if ([[_removePWSecureTextField stringValue] isEqualToString:_password]) {
                _usePassword = NO;
                _useEncryption = NO;
                [_useEncryptionCheckBox setState:NO];
                [_useEncryptionCheckBox setEnabled:NO];
                _password = @"";
                [_outlineViewController saveDatabase];
                [_usePasswordCheckBox setState:NO];
                _preventDetailDrag = NO;
                [_preventDetailDragCheckBox setState:_preventDetailDrag];
                [_preventDetailDragCheckBox setEnabled:NO];
                [_preventDetailDragBlurb setEnabled:NO];
                _preventQuickLookForAttachments = NO;
                [_preventQuickLookForAttachmentsCheckBox setState:_preventQuickLookForAttachments];
                [_preventQuickLookForAttachmentsCheckBox setEnabled:NO];
                [_preventQuickLookForAttachmentsBlurb setEnabled:NO];
            } else {
                [_usePasswordCheckBox setState:YES];
                [_useEncryptionCheckBox setEnabled:YES];
                [_preventDetailDragCheckBox setEnabled:YES];
                [_preventDetailDragBlurb setEnabled:YES];
                [_preventQuickLookForAttachmentsCheckBox setEnabled:YES];
                [_preventQuickLookForAttachmentsBlurb setEnabled:YES];
            }
        }
    } else {
        [_secureTextFieldA setStringValue:@""];
        [_secureTextFieldB setStringValue:@""];
        [_messageLabel setStringValue:@""];
        [_passwordWindow makeFirstResponder:_secureTextFieldA];
        NSModalResponse theResponse = [NSApp runModalForWindow:_passwordWindow];
        if (theResponse == NSModalResponseCancel) {
            [_usePasswordCheckBox setState:NO];
            _useEncryption = NO;
            [_useEncryptionCheckBox setState:NO];
            [_useEncryptionCheckBox setEnabled:NO];
            _preventDetailDrag = NO;
            [_preventDetailDragCheckBox setState:_preventDetailDrag];
            [_preventDetailDragCheckBox setEnabled:NO];
            [_preventDetailDragBlurb setEnabled:NO];
            _preventQuickLookForAttachments = NO;
            [_preventQuickLookForAttachmentsCheckBox setState:_preventQuickLookForAttachments];
            [_preventQuickLookForAttachmentsCheckBox setEnabled:NO];
            [_preventQuickLookForAttachmentsBlurb setEnabled:NO];
        } else {
            _usePassword = YES;
            [_usePasswordCheckBox setState:YES];
            _useEncryption = NO;
            [_useEncryptionCheckBox setState:NO];
            [_useEncryptionCheckBox setEnabled:YES];
            [_preventDetailDragCheckBox setEnabled:YES];
            [_preventDetailDragBlurb setEnabled:YES];
            [_preventQuickLookForAttachmentsCheckBox setEnabled:YES];
            [_preventQuickLookForAttachmentsBlurb setEnabled:YES];
            _password = [_secureTextFieldA stringValue];
            [_outlineViewController saveDatabase];
        }
    }
}

- (IBAction)useEncryptionCheckBoxClicked:(id)sender {
    if ([(NSButton *)sender state] == YES) {
        _useEncryption = YES;
        [_outlineViewController saveDatabase];
    } else {
        [_removePWSecureTextField setStringValue:@""];
        [_removePasswordWindow setTitle:NSLocalizedString(@"Remove Encryption", nil)];
        [_removePasswordWindow makeFirstResponder:_removePWSecureTextField];
        [_altMessageLabel setStringValue:@""];
        NSModalResponse theResponse = [NSApp runModalForWindow:_removePasswordWindow];
        if (theResponse == NSModalResponseCancel) {
            [_useEncryptionCheckBox setState:YES];
            _useEncryption = YES;
        } else {
            [_useEncryptionCheckBox setState:NO];
            _useEncryption = NO;
            [_outlineViewController saveDatabase];
        }
    }
}

- (IBAction)preventDetailDragClicked:(id)sender {
    if ([(NSButton *)sender state] == NO) {
        [_removePWSecureTextField setStringValue:@""];
        [_removePasswordWindow setTitle:NSLocalizedString(@"Allow dragging out of application", nil)];
        [_removePasswordWindow makeFirstResponder:_removePWSecureTextField];
        [_altMessageLabel setStringValue:@""];
        NSModalResponse theResult = [NSApp runModalForWindow:_removePasswordWindow];
        if (theResult == NSModalResponseCancel) {
            [_preventDetailDragCheckBox setState:YES];
            return;
        }
        if (theResult == NSModalResponseOK) {
            if ([[_removePWSecureTextField stringValue] isEqualToString:_password]) {
                _preventDetailDrag = NO;
                [_outlineViewController saveDatabase];
                [_preventDetailDragCheckBox setState:NO];
            } else {
                [_preventDetailDragCheckBox setState:YES];
            }
        }
    }
}

- (IBAction)preventQuickLookClicked:(id)sender {
    if ([(NSButton *)sender state] == NO) {
        [_removePWSecureTextField setStringValue:@""];
        [_removePasswordWindow setTitle:NSLocalizedString(@"Allow use of QuickLook for documents", nil)];
        [_removePasswordWindow makeFirstResponder:_removePWSecureTextField];
        [_altMessageLabel setStringValue:@""];
        NSModalResponse theResult = [NSApp runModalForWindow:_removePasswordWindow];
        if (theResult == NSModalResponseCancel) {
            [_preventQuickLookForAttachmentsCheckBox setState:YES];
            return;
        }
        if (theResult == NSModalResponseOK) {
            if ([[_removePWSecureTextField stringValue] isEqualToString:_password]) {
                _preventQuickLookForAttachments = NO;
                [_preventQuickLookForAttachmentsCheckBox setState:NO];
            } else {
                [_preventQuickLookForAttachmentsCheckBox setState:NO];
            }
        }
    }
}
- (void)getPrefs {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *basicDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"{{0, 0}, {219, 511}}",     @"scrollviewState",
                                   @[@NO],                      @"outlineviewState",
                                   @0,                          @"selectedRow",
                                   @"0000",                     @"selectedUUID",
                                   @NO,                         @"usePassword",
                                   @NO,                         @"useEncryption",
                                   @NO,                         @"preventDetailDrag",
                                   @YES,                        @"userConfirmationForDelete",
                                   @NO,                         @"preventQuickLookForAttachments",
                                   @NO,                         @"saveAfterEveryChange",
                                   @"",                         @"customLabelRegkey",
                                   @"",                         @"customLabelName",
                                   @"",                         @"customLabelEmail",
                                   @"",                         @"customLabelSerial",
                                   @"",                         @"userHash",
                                   @0,                          @"preferredLanguage",
                                   nil];
    [userDefaults registerDefaults:basicDefaults];
    NSArray *outlineviewState = [userDefaults arrayForKey:@"outlineviewState"];
    NSInteger selectedRow = [userDefaults integerForKey:@"selectedRow"];
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:selectedRow] byExtendingSelection:NO];
    if ([outlineviewState count] > [[_outlineViewController softlist] count]) outlineviewState = [@[@YES] mutableCopy];
    for (int i = (int)[outlineviewState count]; i > 0; i--) {
        if ([[outlineviewState objectAtIndex:(i - 1)] boolValue]) {
            [_outlineView expandItem:[[_outlineViewController softlist] objectAtIndex:(i - 1)]];
        }
    }
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:selectedRow] byExtendingSelection:NO];
    if ([[_outlineView itemAtRow:[_outlineView selectedRow]] class] == [SKApplication class]) {
        [_duplicateMenuItem setEnabled:YES];
        [_changeIconMenuItem setEnabled:YES];
        [_printSetupMenuItem setEnabled:YES];
        [_printMenuItem setEnabled:YES];
    } else {
        [_duplicateMenuItem setEnabled:NO];
        [_changeIconMenuItem setEnabled:NO];
        [_printSetupMenuItem setEnabled:NO];
        [_printMenuItem setEnabled:NO];
    }
    _scrollViewState = NSRectFromString([userDefaults stringForKey:@"scrollviewState"]);
    _selectedUuid = [userDefaults stringForKey:@"selectedUUID"];
    _usePassword = [userDefaults boolForKey:@"usePassword"];
    _useEncryption = [userDefaults boolForKey:@"useEncryption"];
    _userHash = [userDefaults stringForKey:@"userHash"];
    if (_useEncryption && !_usePassword) _usePassword = YES;
    _preferredLanguage = (int)[userDefaults integerForKey:@"preferredLanguage"];
    _preventDetailDrag = [userDefaults boolForKey:@"preventDetailDrag"];
    _userConfirmationForDelete = [userDefaults boolForKey:@"userConfirmationForDelete"];
    _preventQuickLookForAttachments = [userDefaults boolForKey:@"preventQuickLookForAttachments"];
    _saveAfterEveryChange = [userDefaults boolForKey:@"saveAfterEveryChange"];
    _customLabelRegkey = [userDefaults stringForKey:@"customLabelRegkey"];
    _customLabelName = [userDefaults stringForKey:@"customLabelName"];
    _customLabelEmail = [userDefaults stringForKey:@"customLabelEmail"];
    _customLabelSerial = [userDefaults stringForKey:@"customLabelSerial"];
    [self applyPrefs];
}

- (void)getAndSetWindowSizePrefsOnly {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *windowDefaults = [NSDictionary dictionaryWithObjectsAndKeys: @"{{304, 117}, {832, 609}}", @"windowPos", nil];
    [userDefaults registerDefaults:windowDefaults];
    NSRect windowPos = NSRectFromString([userDefaults stringForKey:@"windowPos"]);
    [_window setFrame:windowPos display:YES];
}

- (void)setPrefs {
    _usePassword = [_usePasswordCheckBox state];
    _useEncryption = [_useEncryptionCheckBox state];
    _preventDetailDrag = [_preventDetailDragCheckBox state];
    _userConfirmationForDelete = [_userConfirmationForDeleteCheckBox state];
    _preventQuickLookForAttachments = [_preventQuickLookForAttachmentsCheckBox state];
    _saveAfterEveryChange = [_saveAfterEveryChangeCheckBox state];
    _customLabelRegkey = [_customLabelRegkeyTextField stringValue];
    _customLabelName = [_customLabelNameTextField stringValue];
    _customLabelEmail = [_customLabelEmailTextField stringValue];
    _customLabelSerial = [_customLabelSerialTextField stringValue];
    int prefLang = (int)[_languageSelectionPopUpButton indexOfSelectedItem];
    if (prefLang != (int)_preferredLanguage) {
        _preferredLanguage = prefLang;
        [self writePreferredLanguage];
    }
    if (_usePassword && !_useEncryption) {
        _userHash = [_password MD5String];
    } else {
        _userHash = @"";
    }
    [self applyPrefs];
}

- (void)applyPrefs {
    [_usePasswordCheckBox setState:_usePassword];
    if (!_usePassword) {
        _useEncryption = NO;
        [_useEncryptionCheckBox setState:NO];
        [_useEncryptionCheckBox setEnabled:NO];
    } else {
        [_useEncryptionCheckBox setEnabled:YES];
        [_useEncryptionCheckBox setState:_useEncryption];
    }
    [_preventDetailDragCheckBox setState:_preventDetailDrag];
    [_preventDetailDragCheckBox setEnabled:_usePassword];
    [_userConfirmationForDeleteCheckBox setState:_userConfirmationForDelete];
    [_preventQuickLookForAttachmentsCheckBox setState:_preventQuickLookForAttachments];
    [_preventQuickLookForAttachmentsCheckBox setEnabled:_usePassword];
    [_saveAfterEveryChangeCheckBox setState:_saveAfterEveryChange];
    [_languageSelectionPopUpButton selectItemAtIndex:_preferredLanguage];
    if ([_customLabelRegkey isEqualToString:@""]) {
        [_customLabelRegkeyLabel setStringValue:NSLocalizedString(@"Registration Key:", nil)];
        [_customLabelRegkeyTextField setStringValue:@""];
    } else {
        [_customLabelRegkeyLabel setStringValue:[NSString stringWithFormat:@"%@:",_customLabelRegkey]];
        [_customLabelRegkeyTextField setStringValue:_customLabelRegkey];
    }
    if ([_customLabelName isEqualToString:@""]) {
        [_customLabelNameLabel setStringValue:NSLocalizedString(@"Name:", nil)];
        [_customLabelNameTextField setStringValue:@""];
    } else {
        [_customLabelNameLabel setStringValue:[NSString stringWithFormat:@"%@:",_customLabelName]];
        [_customLabelNameTextField setStringValue:_customLabelName];
    }
    if ([_customLabelEmail isEqualToString:@""]) {
        [_customLabelEmailLabel setStringValue:NSLocalizedString(@"E-mail:", nil)];
        [_customLabelEmailTextField setStringValue:@""];
    } else {
        [_customLabelEmailLabel setStringValue:[NSString stringWithFormat:@"%@:",_customLabelEmail]];
        [_customLabelEmailTextField setStringValue:_customLabelEmail];
    }
    if ([_customLabelSerial isEqualToString:@""]) {
        [_customLabelSerialLabel setStringValue:NSLocalizedString(@"Serial Number:", nil)];
        [_customLabelSerialTextField setStringValue:@""];
    } else {
        [_customLabelSerialLabel setStringValue:[NSString stringWithFormat:@"%@:",_customLabelSerial]];
        [_customLabelSerialTextField setStringValue:_customLabelSerial];
    }
}

- (void)savePrefs {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *windowPos = NSStringFromRect([_window frame]);
    NSArray *outlineviewState;
    if ([_outlineViewController searchActive]) {
        outlineviewState = [_outlineViewController expansionState];
        if (outlineviewState == nil) outlineviewState = [[_outlineViewController getExpansionState] copy];
    } else {
        outlineviewState = [[_outlineViewController getExpansionState] copy];
    }
    NSInteger selectedRow = [_outlineView selectedRow];
    NSString *scrollviewState = NSStringFromRect([_outlineViewController getScrollviewState]);
    [userDefaults setObject:windowPos forKey:@"windowPos"];
    [userDefaults setObject:scrollviewState forKey:@"scrollviewState"];
    [userDefaults setObject:outlineviewState forKey:@"outlineviewState"];
    [userDefaults setInteger:selectedRow forKey:@"selectedRow"];
    [userDefaults setObject:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid] forKey:@"selectedUUID"];
    [userDefaults setBool:_usePassword forKey:@"usePassword"];
    [userDefaults setBool:_useEncryption forKey:@"useEncryption"];
    [userDefaults setBool:_preventDetailDrag forKey:@"preventDetailDrag"];
    [userDefaults setBool:_userConfirmationForDelete forKey:@"userConfirmationForDelete"];
    [userDefaults setBool:_preventQuickLookForAttachments forKey:@"preventQuickLookForAttachments"];
    [userDefaults setBool:_saveAfterEveryChange forKey:@"saveAfterEveryChange"];
    [userDefaults setObject:_customLabelRegkey forKey:@"customLabelRegkey"];
    [userDefaults setObject:_customLabelName forKey:@"customLabelName"];
    [userDefaults setObject:_customLabelEmail forKey:@"customLabelEmail"];
    [userDefaults setObject:_customLabelSerial forKey:@"customLabelSerial"];
    [userDefaults setObject:_userHash forKey:@"userHash"];
    [userDefaults setInteger:(NSInteger)_preferredLanguage forKey:@"preferredLanguage"];
}

- (void)writePreferredLanguage {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *appId = [bundle objectForInfoDictionaryKey:@"CFBundleIdentifier"];
    NSArray *languages = @[@"", @"(en)", @"(nl)", @"(ja)"];
    NSPipe *pipe = [NSPipe pipe];
    NSFileHandle *file = pipe.fileHandleForReading;
    NSTask *task = [[NSTask alloc] init];
    task.launchPath = @"/usr/bin/defaults";
    if (_preferredLanguage != 0) {
        task.arguments = @[@"write", appId, @"AppleLanguages", [languages objectAtIndex:_preferredLanguage]];
    } else {
        task.arguments = @[@"delete", appId, @"AppleLanguages"];
    }
    task.standardOutput = pipe;
    [task launch];
    NSData *data = [file readDataToEndOfFile];
    [file closeFile];
    NSString *output = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    if ([output isNotEqualTo:@""]) NSLog(@"SERIALKEEPER: Something (unexpected?) happened during language selection:\r\"%@\".\nMake of this what you will - it shouldn't be harmful, but the operation may have failed.", output);
}

- (void)restartNow {
    NSArray *appArray = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"nl.kinoko-house.SerialKeeper-Relauncher"];
    if ([appArray count] > 0) {
        [(NSRunningApplication *)[appArray objectAtIndex:0] terminate];
        sleep(1);
    }
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:@"SerialKeeper Relauncher" ofType:@"app"];
    [[NSWorkspace sharedWorkspace] launchApplication:path];
    [NSApp terminate:nil];
}


#pragma mark Add Item related methods

- (IBAction)showAddItemSheet:(id)sender {
    if (!_interactionAllowed) return;
    [_outlineViewController dealWithChanges];
    if (![_outlineViewController searchActive]) {
        [_addSingleAppRadioButton performClick:nil];
        [NSApp beginSheet:_addItemSheet modalForWindow:_window modalDelegate:nil didEndSelector:nil contextInfo:nil];
    }
}

- (IBAction)addItemOKClicked:(id)sender {
    [self removeAddItemSheet];
    [_outlineViewController unsetSearchMode];
    switch ([_selectedAddItemRadioButton integerValue]) {
        case 0:
            [_outlineViewController addSingleApplication:nil];
            break;
        case 1:
            [self addAllApplicationsInApplicationsFolder];
            break;
        case 2:
            [self addSomethingElse];
            break;
        default:
            break;
    }
}

- (IBAction) addItemCancelClicked:(id)sender {
    [self removeAddItemSheet];
}

- (IBAction) addItemRadioButtonClicked:(id)sender {
    _selectedAddItemRadioButton = [sender identifier];
    int buttonValue = [_selectedAddItemRadioButton intValue];
    
    switch(buttonValue) {
        case 0:
            [_addAllAppsRadioButton setState:NO];
            [_addAnotherItemRadioButton setState:NO];
            break;
            
        case 1:
            [_addSingleAppRadioButton setState:NO];
            [_addAnotherItemRadioButton setState:NO];
            break;
            
        case 2:
            [_addSingleAppRadioButton setState:NO];
            [_addAllAppsRadioButton setState:NO];
            break;
            
        default:
            [_addAllAppsRadioButton setState:NO];
            [_addAnotherItemRadioButton setState:NO];
    }
}

- (void)removeAddItemSheet {
    [NSApp endSheet:_addItemSheet];
    [_addItemSheet orderOut:self];
}

- (IBAction)softListSegmentedButtonClicked:(NSSegmentedControl *)sender {
    if (!_interactionAllowed) return;
    NSInteger clickedSegment = [sender selectedSegment];
    if (clickedSegment == 0) {
        [self addCategoryToolbarButtonClicked:nil];
    } else if (clickedSegment == 1) {
        [self showAddItemSheet:nil];
    } else {
        [_outlineViewController dealWithChanges];
        [_outlineViewController removeSelectedItem];
    }
}

- (IBAction)attachmentListSegmentedButtonClicked:(NSSegmentedControl *)sender {
    NSInteger clickedSegment = [sender selectedSegment];
    if (clickedSegment == 0) {
        [self addAttachment];
    } else if (clickedSegment == 1) {
        [self removeAttachment];
    } else {
        [_outlineViewController exportSelectedDocuments];
    }
}

- (void)setChangeDetected {
    [_outlineViewController setChangeDetected:YES];
}

- (void)addAttachment {
    NSString *filePath = [_outlineViewController showLoadDialogForFileExtensions:nil];
    if (!filePath) return;
    NSString *fileName = [filePath lastPathComponent];
    NSData *documentData = [NSData dataWithContentsOfFile:filePath];
    SKAttachment *theAttachment = [[SKAttachment alloc] initWithFilename:fileName document:documentData];
    [_tableViewController addAttachmentExternal:theAttachment];
    [_outlineViewController setChangeDetected:YES];
    [_outlineViewController dealWithChanges];
}

- (void)removeAttachment {
    [_tableViewController removeCurrentlySelectedAttachments];
    [_outlineViewController setChangeDetected:YES];
    [_outlineViewController dealWithChanges];
}


- (IBAction)addCategoryToolbarButtonClicked:(id)sender {
    if (!_interactionAllowed) return;
    [_outlineViewController dealWithChanges];
    if (![_outlineViewController searchActive]) {
        [_outlineViewController createNewCategory];
    }
}


#pragma mark Import related IBActions

- (IBAction) importOKClicked:(id)sender {
    [_outlineViewController unsetSearchMode];
    [_outlineViewController setImportDatabasePopUpIndex:[_importDatabasePopUpButton indexOfSelectedItem]];
    switch ([_selectedImportRadioButton integerValue]) {
        case 0:
            [_outlineViewController importSerialKeeperDatabase];
            break;
        case 1:
            [_outlineViewController importRapidoSerialDatabase];
            break;
        default:
            break;
    }
}

- (IBAction) importCancelClicked:(id)sender {
    [self removeImportSheet];
}

- (IBAction) importRadioButtonClicked:(id)sender {
    _selectedImportRadioButton = [sender identifier];
    int buttonValue = [_selectedImportRadioButton intValue];
    switch(buttonValue) {
        case 0:
            [_importRSDatabaseRadioButton setState:NO];
            break;
        case 1:
            [_importSKDatabaseRadioButton setState:NO];
            break;
        default:
            break;
    }
}

- (IBAction) exportRadioButtonClicked:(id)sender {
    _selectedExportRadioButton = [sender identifier];
    int buttonValue = [_selectedExportRadioButton intValue];
    switch(buttonValue) {
        case 0:
            [_exportCSVRadioButton setState:NO];
            break;
        case 1:
            [_exportSKDatabaseRadioButton setState:NO];
            break;
         default:
            break;
    }
}

- (IBAction)exportOKClicked:(id)sender {
    [NSApp endSheet:_exportSheet];
    [_exportSheet orderOut:self];
    if ([_selectedExportRadioButton integerValue] == 0) {
        [self exportSKDatabase];
    } else {
        [self exportCSVDatabase];
    }
}

- (IBAction)exportCancelClicked:(id)sender {
    [NSApp endSheet:_exportSheet];
    [_exportSheet orderOut:self];
}

- (IBAction)startButtonClicked:(id)sender {
    NSInteger categorySelected = [_categoryPopUpButton indexOfSelectedItem];
    [_outlineViewController setImportState:[_importFromSubfolders state]];
    [_importFromSubfolders setEnabled:NO];
    [_importStartButton setTitle:@"Stop"];
    [_importStartButton setTarget:self];
    [_importStartButton setAction:nil];
    [_importCancelButton setEnabled:NO];
    [_importProgressIndicator setIndeterminate:YES];
    [_categoryPopUpButton setEnabled:NO];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self.outlineViewController importAppFolderAppsWithCategoryIndex:categorySelected];
    });
}

- (IBAction)multiImportCancelButtonClicked:(id)sender {
    [self removeImportMultiSheet];
}

- (IBAction)getIndexOfCustomItemCategory:(id)sender {
    _selectedCategoryIndex = [_customItemCategoryPopUpButton indexOfSelectedItem];
}

- (void)removeImportMultiSheet {
    [NSApp endSheet:_importMultiSheet];
    [_importMultiSheet orderOut:self];
}

- (void)removeImportSheet {
    [NSApp endSheet:_importSheet];
    [_importSheet orderOut:self];
}

- (void)addAllApplicationsInApplicationsFolder {
    NSInteger i;
    NSString *title;
    NSInteger numberOfItems = [_categoryPopUpButton indexOfItem:[_categoryPopUpButton lastItem]];
    if (numberOfItems > 2) {
        for (i = numberOfItems; i > 1; i--) {
            [_categoryPopUpButton removeItemAtIndex:i];
        }
    }
    for (i = 0; i < [[_outlineViewController softlist] count]; i++) {
        title = [[[_outlineViewController softlist] objectAtIndex:i] category];
        [_categoryPopUpButton addItemWithTitle:title];
    }
    [_importFromSubfolders setEnabled:YES];
    [_importFromSubfolders setState:NO];
    [_categoryPopUpButton setEnabled:YES];
    [_categoryPopUpButton selectItemAtIndex:0];
    [_importStartButton setTitle:NSLocalizedString(@"Start", nil)];
    [_importStartButton setEnabled:YES];
    [_importStartButton setTarget:self];
    [_importStartButton setAction:@selector(startButtonClicked:)];
    [_importCancelButton setEnabled:YES];
    [_importProgressText setStringValue:NSLocalizedString(@"Progress:", nil)];
    [_importProgressIndicator setIndeterminate:NO];
    [_importProgressIndicator setMinValue:0.0f];
    [_importProgressIndicator setMinValue:2.0f];
    [_importProgressIndicator setDoubleValue:0.0f];
    [NSApp beginSheet:_importMultiSheet modalForWindow:_window modalDelegate:nil didEndSelector:nil contextInfo:nil];
}

- (IBAction)showImportSheet:(id)sender {
    [_importSKDatabaseRadioButton setEnabled:YES];
    [_importRSDatabaseRadioButton setEnabled:YES];
    [_importSKDatabaseRadioButton performClick:nil];
    [_importDatabasePopUpButton setEnabled:YES];
    [_importDatabasePopUpButton selectItemAtIndex:0];
    [_importDatabaseCancelButton setEnabled:YES];
    [_importDatabaseStartButton setTitle:NSLocalizedString(@"Start", nil)];
    [_importDatabaseStartButton setEnabled:YES];
    [_importDatabaseStartButton setTarget:self];
    [_importDatabaseStartButton setAction:@selector(importOKClicked:)];
    [_importDatabaseProgressText setStringValue:NSLocalizedString(@"Progress:", nil)];
    [_importDatabaseProgressIndicator setIndeterminate:NO];
    [_importDatabaseProgressIndicator setMinValue:0.0f];
    [_importDatabaseProgressIndicator setMaxValue:2.0f];
    [_importDatabaseProgressIndicator setDoubleValue:0.0f];
    [NSApp beginSheet:_importSheet modalForWindow:_window modalDelegate:nil didEndSelector:nil contextInfo:nil];
}

- (void)compileCategoryMenuForAddOtherItem {
    [_customItemCategoryPopUpButton removeAllItems];
    for (int i = 0; i < [[_outlineViewController softlist] count]; i++) {
        [[_customItemCategoryPopUpButton menu] addItemWithTitle:[[[_outlineViewController softlist] objectAtIndex:i] category] action:nil keyEquivalent:@""];
    }
    id selection = [_outlineViewController getCurrentlySelectedItem];
    if ([selection class] == [SKApplication class]) {
        selection = [_outlineView parentForItem:selection];
    }
    [_customItemCategoryPopUpButton selectItemAtIndex:[[_outlineViewController softlist] indexOfObject:selection]];
}

- (void)addSomethingElse {
    NSImage *newIcon;
    [self compileCategoryMenuForAddOtherItem];
    [_iconPopUpButton selectItemAtIndex:0];
    if (@available(macOS 10.9, *)) {
        newIcon = [[NSWorkspace sharedWorkspace] iconForFileType:@"app"];
    } else {
        newIcon = [SKUtilities iconWithSystemReference:@"GenericApplicationIcon"];
    }
    [newIcon setSize:NSMakeSize(128, 128)];
    [_customIconImageWell setImage:[newIcon copy]];
    [_customItemTextField setStringValue:NSLocalizedString(@"New Item", nil)];
    [NSApp beginSheet:_customItemSheet modalForWindow:_window modalDelegate:nil didEndSelector:nil contextInfo:nil];
    [_customIconImageWell setFocusRingType:NSFocusRingTypeNone];
    [_customItemSheet makeKeyAndOrderFront:nil];
    [_customItemSheet makeFirstResponder:_customItemTextField];
}

- (IBAction)removeAddCustomItemSheet:(id)sender {
    [NSApp endSheet:_customItemSheet];
    [_customItemSheet orderOut:self];
}

- (IBAction)addCustomItem:(id)sender {
    [self removeAddCustomItemSheet:nil];
    NSArray *reps = [[_customIconImageWell image] representations];
    NSImage *newImage = [[NSImage alloc] init];
    BOOL repFound = NO;
    for (int p = 0; p < [reps count]; p++) {
        if ([(NSImageRep *)[reps objectAtIndex:p] pixelsWide] == 256) {
            repFound = YES;
            [newImage addRepresentation:[reps objectAtIndex:p]];
            break;
        }
    }
    if (!repFound) {
        [newImage addRepresentation:[reps objectAtIndex:0]];
    }
    NSData *iconData = [newImage TIFFRepresentation];
    NSString *product = [_customItemTextField stringValue];
    SKApplication *customItem = [[SKApplication alloc] initWithProduct:product icon:iconData version:@"" company:@"" website:@""];
    [_outlineViewController addSomethingElse:customItem];
}

- (IBAction)findCustomIconImage:(id)sender {
    NSArray *extensions = @[@"tiff", @"tif", @"png", @"jpg", @"jpeg", @"icns", @"gif"];
    NSString *path = [_outlineViewController showLoadDialogForFileExtensions:extensions];
    if (path == nil) {
        [_iconPopUpButton selectItemAtIndex:0];
        return;
    }
    [_customIconImageWell setImage:[[NSImage alloc] initWithContentsOfFile:path]];
    [_iconPopUpButton selectItemAtIndex:0];
}

- (IBAction)findCustomIconFromPopUpChoice:(id)sender {
    NSImage *theImage = [[[_iconPopUpButton itemAtIndex:[_iconPopUpButton indexOfSelectedItem]] image] copy];
    [theImage setSize:NSMakeSize(128, 128)];
    [_customIconImageWell setImage:theImage];
}

- (IBAction)findChangedIconImage:(id)sender {
    NSArray *extensions = @[@"tiff", @"tif", @"png", @"jpg", @"jpeg", @"icns", @"gif"];
    NSString *path = [_outlineViewController showLoadDialogForFileExtensions:extensions];
    if (path == nil) {
        [_changeIconPopUpButton selectItemAtIndex:0];
        return;
    }
    [_changeIconImageWell setImage:[[NSImage alloc] initWithContentsOfFile:path]];
    [_changeIconPopUpButton selectItemAtIndex:0];
}

- (IBAction)findChangedIconFromPopUpChoice:(id)sender {
    NSImage *theImage = [[[_changeIconPopUpButton itemAtIndex:[_changeIconPopUpButton indexOfSelectedItem]] image] copy];
    [theImage setSize:NSMakeSize(128, 128)];
    [_changeIconImageWell setImage:theImage];
}

#pragma mark Tour Methods

- (void)disallowInteraction {
    _interactionAllowed = NO;
    [_skMainMenuItem setEnabled:NO];
    [_fileMainMenuItem setEnabled:NO];
    [_editMainMenuItem setEnabled:NO];
    [_windowMainMenuItem setEnabled:NO];
    [_helpMainMenuItem setEnabled:NO];
    [_toolBar setAllowsUserCustomization:NO];
    [_searchField setEnabled:NO];
    _window.styleMask &= ~NSWindowStyleMaskResizable;
}

- (void)allowInteraction {
    _interactionAllowed = YES;
    [_skMainMenuItem setEnabled:YES];
    [_fileMainMenuItem setEnabled:YES];
    [_editMainMenuItem setEnabled:YES];
    [_windowMainMenuItem setEnabled:YES];
    [_helpMainMenuItem setEnabled:YES];
    [_toolBar setAllowsUserCustomization:YES];
    [_searchField setEnabled:YES];
    _window.styleMask |= NSWindowStyleMaskResizable;
}

- (void)startTour {
    NSDictionary *boldAttrib = @{NSFontAttributeName:[NSFont boldSystemFontOfSize:[NSFont systemFontSize]]};
    NSDictionary *underlinedAttrib = @{NSFontAttributeName:[NSFont systemFontOfSize:[NSFont systemFontSize]], NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)};
    NSDictionary *obliqueAttrib = @{NSFontAttributeName:[NSFont systemFontOfSize:[NSFont systemFontSize]], NSObliquenessAttributeName:@0.25f};

    NSMutableAttributedString *theText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"This is where you can put your applications, but also other stuff like plug-ins. You can add applications by clicking on the 'Add Application' button in the toolbar of the main window, by clicking on the '+' button underneath the list, or by dragging and dropping them on the list from the Finder.", @"base sentence") attributes:nil];
    NSRange theRange = [[theText string] rangeOfString:NSLocalizedString(@"add applications", @"underlined in above sentence")];
    [theText setAttributes:underlinedAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"'Add Application'", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"'+'", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"dragging and dropping them", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    [_tour1text1 setAttributedStringValue:theText];

    theText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Applications are organised in so-called 'categories'. You can add categories by clicking on the 'Add Category' button in the toolbar of the main window above or by clicking on the Folder symbol underneath the list.", @"base sentence") attributes:nil];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"'categories'", @"underlined in above sentence")];
    [theText setAttributes:obliqueAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"add categories", @"underlined in above sentence")];
    [theText setAttributes:underlinedAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"'Add Category'", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"Folder symbol", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    [_tour1text2 setAttributedStringValue:theText];

    theText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"You can also move applications to other categories by dragging them there.", @"base sentence") attributes:nil];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"move applications", @"underlined in above sentence")];
    [theText setAttributes:underlinedAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"by dragging them there", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    [_tour1text3 setAttributedStringValue:theText];

    theText = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"You can add documents to the selected application by clicking the '+' button at the bottom of the document list and locate the document to add on your hard drive, or you can drag and drop them from the Finder. You can export them by using the pertinent button or by dragging them to the Finder.", @"base sentence") attributes:nil];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"add documents", @"underlined in above sentence")];
    [theText setAttributes:underlinedAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"'+'", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"drag and drop them", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"export", @"underlined in above sentence")];
    [theText setAttributes:underlinedAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"pertinent button", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    theRange = [[theText string] rangeOfString:NSLocalizedString(@"dragging them to the Finder", @"bold in above sentence")];
    [theText setAttributes:boldAttrib range:theRange];
    [_tour3text2 setAttributedStringValue:theText];

    NSRect scrollviewState = NSRectFromString(@"{{0, 0}, {219, 511}}");
    [[_scrollView contentView] scrollToPoint:NSMakePoint(0, scrollviewState.origin.y)];
    [_scrollView reflectScrolledClipView:[_scrollView contentView]];
    [_outlineView expandItem:[[_outlineViewController softlist] objectAtIndex:0]];
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
    [_window makeKeyAndOrderFront:self];
    [_introWindow makeKeyAndOrderFront:nil];
    [NSApp runModalForWindow:_introWindow];
}

- (IBAction)continueTour1:(id)sender {
    [NSApp stopModal];
    [_introWindow orderOut:nil];
    [_window makeKeyAndOrderFront:nil];
    [self disallowInteraction];
    [_tour1View showRelativeToRect:[_outlineView bounds] ofView:_outlineView preferredEdge:NSMaxXEdge];
}

- (IBAction)noThanks:(id)sender {
    [NSApp stopModal];
    [_introWindow orderOut:nil];
    [_window makeKeyAndOrderFront:nil];
}

- (IBAction)cancelTour1:(id)sender {
    [_tour1View close];
    [self allowInteraction];
    [_window makeKeyAndOrderFront:nil];
}

- (IBAction)continueTour2:(id)sender {
    [_tour1View close];
    [_outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:1] byExtendingSelection:NO];
    [_tour2View showRelativeToRect:[[_outlineViewController applicationProduct] bounds] ofView:[_outlineViewController applicationProduct] preferredEdge:NSMinYEdge];
}

- (IBAction)cancelTour2:(id)sender {
    [_tour2View close];
    [self allowInteraction];
    [_window makeKeyAndOrderFront:nil];
}

- (IBAction)continueTour3:(id)sender {
    [_tour2View close];
    [_tour3View showRelativeToRect:[[_outlineViewController applicationTableView] bounds] ofView:[_outlineViewController applicationTableView] preferredEdge:NSMaxYEdge];
}

- (IBAction)endOfTour3:(id)sender {
    [_tour3View close];
    [self allowInteraction];
    [_window makeKeyAndOrderFront:nil];
}


#pragma mark Menu IBActions

- (void)enableRemoveMenuItem {
    [_removeMenuItem setEnabled:YES];
}

- (void)disableRemoveMenuItem {
    [_removeMenuItem setEnabled:NO];
}


- (IBAction)addNewProduct:(id)sender {
    [self showAddItemSheet:nil];
}

- (IBAction)duplicateProduct:(id)sender {
    BOOL searchIsOn = [_outlineViewController searchActive];
    if (!searchIsOn) {
        NSInteger selection = [_outlineView selectedRow];
        SKApplication *application = [_outlineView itemAtRow:selection];
        SKCategory *category = [_outlineView parentForItem:application];
        application = [application copy];
        [application setUuid:[SKUtilities createUUID]];
        [application setProduct:[SKUtilities getNameForCopy:[application product]]];
        [category addApplication:[application copy]];
        [_outlineView reloadData];
        [_outlineViewController setSavedUUID:[application uuid]];
    } else {
        NSInteger selection = [_outlineView selectedRow];
        NSInteger indexOfActualCategory = [[_outlineView itemAtRow:selection] parentIndex];
        NSInteger indexOfActualApplication = [[_outlineView itemAtRow:selection] originalIndex];
        SKCategory *actualCategory = [[_outlineViewController softlist] objectAtIndex:indexOfActualCategory];
        SKApplication *actualApplication = [[[actualCategory applications] objectAtIndex:indexOfActualApplication] copy];
        [actualApplication setUuid:[SKUtilities createUUID]];
        [actualApplication setProduct:[SKUtilities getNameForCopy:[actualApplication product]]];
        [_outlineViewController setSavedUUID:[actualApplication uuid]];
        [actualCategory addApplication:actualApplication];
        [_outlineViewController buildFilteredSoftwareList];
    }
    [_outlineViewController reselectItemAfterUndo];
    [[_outlineView undoManager] removeAllActions];
}

- (IBAction)addCategory:(id)sender {
    [self addCategoryToolbarButtonClicked:nil];
}

- (IBAction)removeItem:(id)sender {
    if ([[_window firstResponder] isEqualTo:_outlineView]) {
        [_outlineViewController removeSelectedItem];
    }
    if ([[_window firstResponder] isEqualTo:_tableView]) {
        [_tableViewController removeCurrentlySelectedAttachments];
    }
}

- (IBAction)showChangeIconSheet:(id)sender {
    [_outlineViewController dealWithChanges];
    [_changeIconPopUpButton selectItemAtIndex:0];
    NSImage *newIcon = [[NSImage alloc] initWithData:[(SKApplication *)[_outlineView itemAtRow:[_outlineView selectedRow]] icon]];
    [newIcon setSize:NSMakeSize(128, 128)];
    [_changeIconImageWell setImage:[newIcon copy]];
    [NSApp beginSheet:_changeIconSheet modalForWindow:_window modalDelegate:nil didEndSelector:nil contextInfo:nil];
    [_changeIconImageWell setFocusRingType:NSFocusRingTypeNone];
    [_changeIconSheet makeKeyAndOrderFront:nil];
}

- (IBAction)changeIconCancelled:(id)sender {
    [NSApp endSheet:_changeIconSheet];
    [_changeIconSheet orderOut:self];
}

- (IBAction)changeIconOK:(id)sender {
    BOOL searchIsOn = [_outlineViewController searchActive];
    [self changeIconCancelled:nil];
    NSArray *reps = [[_changeIconImageWell image] representations];
    NSImage *newImage = [[NSImage alloc] init];
    BOOL repFound = NO;
    for (int p = 0; p < [reps count]; p++) {
        if ([(NSImageRep *)[reps objectAtIndex:p] pixelsWide] == 256) {
            repFound = YES;
            [newImage addRepresentation:[reps objectAtIndex:p]];
            break;
        }
    }
    if (!repFound) {
        [newImage addRepresentation:[reps objectAtIndex:0]];
    }
    NSData *iconData = [newImage TIFFRepresentation];
    if (!searchIsOn) {
        [(SKApplication *)[_outlineView itemAtRow:[_outlineView selectedRow]] setIcon:iconData];
        [_outlineViewController setChangeDetected:YES];
        [_outlineViewController dealWithChanges];
    } else {
        NSUInteger parentIndex = [(SKApplication *)[_outlineView itemAtRow:[_outlineView selectedRow]] parentIndex];
        NSUInteger originalIndex = [(SKApplication *)[_outlineView itemAtRow:[_outlineView selectedRow]] originalIndex];
        NSString *uuid = [(SKApplication *)[_outlineView itemAtRow:[_outlineView selectedRow]] uuid];
        [[[(SKCategory *)[[_outlineViewController softlist] objectAtIndex:parentIndex] applications] objectAtIndex:originalIndex] setIcon:iconData];
        [_outlineViewController setChangeDetected:YES];
        [_outlineViewController dealWithChanges];
        [_outlineViewController buildFilteredSoftwareList];
        [_outlineViewController setSavedUUID:uuid];
        [_outlineViewController reselectItemAfterUndo];
    }
}

- (void)iconDoubleClicked {
    [self showChangeIconSheet:nil];
}

#pragma mark Password Methods and IBActions


- (IBAction)prefPassAccepted:(id)sender {
    if ([[_secureTextFieldA stringValue] isEqualToString:@""] || [[_secureTextFieldB stringValue] isEqualToString:@""]) {
        [_messageLabel setStringValue:NSLocalizedString(@"Field(s) empty.", nil)];
        NSBeep();
        [_outlineViewController shakeThisWindow:_passwordWindow];
    } else if ([[_secureTextFieldA stringValue] isNotEqualTo:[_secureTextFieldB stringValue]]){
        [_messageLabel setStringValue:NSLocalizedString(@"Password mismatch.", nil)];
        NSBeep();
        [_outlineViewController shakeThisWindow:_passwordWindow];
    } else {
        [NSApp stopModalWithCode:NSModalResponseOK];
        [_passwordWindow orderOut:self];
    }
}

- (IBAction)prefPassCanceled:(id)sender {
    [NSApp stopModalWithCode:NSModalResponseCancel];
    [_passwordWindow orderOut:self];
}

- (IBAction)sheetPassAccepted:(id)sender {
    if ([[_secureTextField stringValue] isEqualToString:@""]) {
        NSBeep();
    } else {
        [_outlineViewController continueDeferredLoad];
    }
}

- (IBAction)sheetPassCanceled:(id)sender {
    [NSApp endSheet:_passwordSheet];
    [_passwordSheet orderOut:self];
    [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Cancelled decryption of database", nil) informativeText:NSLocalizedString(@"The database is protected with a password and the application cannot open without it. Please reopen SerialKeeper and enter the password you used when protecting the database. SerialKeeper will now close.", nil)];
    [[NSApplication sharedApplication] terminate:nil];
}

- (IBAction)removePassAccepted:(id)sender {
    if ([[_removePWSecureTextField stringValue] isEqualToString:@""] || [[_removePWSecureTextField stringValue] isNotEqualTo:_password]) {
        [_altMessageLabel setStringValue:NSLocalizedString(@"Password incorrect.", nil)];
        [_outlineViewController shakeThisWindow:_removePasswordWindow];
        NSBeep();
    } else {
        [NSApp stopModalWithCode:NSModalResponseOK];
        [_removePasswordWindow orderOut:self];
    }
}

- (IBAction)removePassCanceled:(id)sender {
    [NSApp stopModalWithCode:NSModalResponseCancel];
    [_removePasswordWindow orderOut:self];
}

#pragma mark Export related IBActions and methods

- (IBAction)showExportSheet:(id)sender {
    [_outlineViewController dealWithChanges];
    [_exportSKDatabaseRadioButton performClick:nil];
    [NSApp beginSheet:_exportSheet modalForWindow:_window modalDelegate:nil didEndSelector:nil contextInfo:nil];
}

- (void)exportSKDatabase {
    NSInteger theResult;
    if (_usePassword) {
        [_removePasswordWindow setTitle:NSLocalizedString(@"Export Database", nil)];
        [_altMessageLabel setStringValue:@""];
        [_removePWSecureTextField setStringValue:@""];
        theResult = [NSApp runModalForWindow:_removePasswordWindow];
        if (theResult == NSModalResponseCancel) return;
    }
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    [savePanel setTitle:NSLocalizedString(@"Please select location for export:", nil)];
    [savePanel setPrompt:NSLocalizedString(@"Export", nil)];
    [savePanel setCanCreateDirectories:YES];
    [savePanel setNameFieldLabel:NSLocalizedString(@"Export database as:", nil)];
    [savePanel setNameFieldStringValue:@"SoftwareList.serialkeeper"];
    theResult = [savePanel runModal];
    if (theResult == NSModalResponseCancel) return;
    NSString *exportPath = [[savePanel URL] path];
    BOOL success;
    success = [NSKeyedArchiver archiveRootObject:[_outlineViewController softlist] toFile:exportPath];
    if (!success) {
        [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Could not export the database", nil) informativeText:NSLocalizedString(@"There was an unspecified problem exporting the database. Please try to export it again to another location, or contact the developer for further help if the problem persists. We are very sorry for the inconvenience.", nil)];
    } else {
        [[NSWorkspace sharedWorkspace] setIcon:[NSImage imageNamed:@"SKDatabaseIcon"] forFile:exportPath options:0];
    }
}

- (void)exportCSVDatabase {
    if (_usePassword) {
        [_removePasswordWindow setTitle:NSLocalizedString(@"Export Database", nil)];
        [_altMessageLabel setStringValue:@""];
        [_removePWSecureTextField setStringValue:@""];
        NSUInteger theResult = [NSApp runModalForWindow:_removePasswordWindow];
        if (theResult == NSModalResponseCancel) return;
    }
    NSSavePanel *savePanel = [NSSavePanel savePanel];
    [savePanel setTitle:NSLocalizedString(@"Please select location for export:", nil)];
    [savePanel setPrompt:NSLocalizedString(@"Export", nil)];
    [savePanel setCanCreateDirectories:YES];
    [savePanel setNameFieldLabel:NSLocalizedString(@"Export database as:", nil)];
    [savePanel setNameFieldStringValue:@"SoftwareList.csv"];
    NSInteger theResult = [savePanel runModal];
    if (theResult == NSModalResponseCancel) return;
    NSString *exportPath = [[savePanel URL] path];
    NSString *csvExportString = NSLocalizedString(@"\"Product name\",\"Version\",\"Description\",\"Category\",\"Registration key\",\"Registration name\",\"Purchase date\",\"Registration e-mail\",\"Serial number\",\"Company\",\"Website\",\"Notes\"\r", nil);
    NSArray *mySoftwarelist = [_outlineViewController softlist];
    for (int i = 0; i < [mySoftwarelist count]; i++) {
        NSArray *currentApps = [[mySoftwarelist objectAtIndex:i] applications];
        for (int j = 0; j < [currentApps count]; j++) {
            SKApplication *currentApp = [currentApps objectAtIndex:j];
            NSString *nam = [self stripQuotes:[currentApp product]];
            NSString *ver = [self stripQuotes:[currentApp version]];
            NSString *dsc = [self stripQuotes:[currentApp descript]];
            NSString *cat = [[mySoftwarelist objectAtIndex:i] category];
            NSString *rgk = [self stripQuotes:[currentApp registration]];
            NSString *rgn = [self stripQuotes:[currentApp regname]];
            NSDate *purchaseDate = [currentApp purchasedate];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            NSString *prd = [formatter stringFromDate:purchaseDate];
            NSString *eml = [self stripQuotes:[currentApp email]];
            NSString *srn = [self stripQuotes:[currentApp serial]];
            NSString *cmp = [self stripQuotes:[currentApp company]];
            NSString *web = [self stripQuotes:[currentApp website]];
            NSString *nts = [self stripQuotes:[currentApp notes]];
            csvExportString = [NSString stringWithFormat:@"%@\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\"\r", csvExportString, nam, ver, dsc, cat, rgk, rgn, prd, eml, srn, cmp, web, nts];
        }
        NSError *error;
        BOOL success = [csvExportString writeToFile:exportPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        if (!success){
            [SKUtilities showAlertWithMessageText:NSLocalizedString(@"Could not export the CSV file", nil) informativeText:[NSString stringWithFormat:NSLocalizedString(@"There was a problem exporting the CSV file. Information follows below:\n\n%li: %@\n\nPlease try to export it again to another location, or contact the developer for further help if the problem persists. We are very sorry for the inconvenience.", nil), (long)[error code], [error localizedDescription]]];
        }
    }
}

- (NSString *)stripQuotes:(NSString *)input {
    NSString *newString = [[input componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    return [newString stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
}

- (void)scrollAndReselect {
    [[_scrollView contentView] scrollToPoint:NSMakePoint(0, _scrollViewState.origin.y)];
    [_scrollView reflectScrolledClipView:[_scrollView contentView]];
    if ([_selectedUuid isNotEqualTo:@"0000"]) {
        [_outlineViewController setUuidOfLastSelectedItem:_selectedUuid];
        [_outlineViewController reselectOriginalItem];
    } else {
        [_outlineViewController setUuidOfLastSelectedItem:[[_outlineView itemAtRow:[_outlineView selectedRow]] uuid]];
    }
}


#pragma mark SplitView resizing

- (BOOL)splitView:(NSSplitView *)splitView shouldAdjustSizeOfSubview:(NSView *)view {
    if ([view isEqual:_doNotAdjust]) {
        return NO;
    } else {
        return YES;
    }
}

- (void)splitViewWasResized {
    if (_isResizing) return;
    _isResizing = YES;
    NSRect newFrame = [_softwareListSegmentedControl frame];
    NSRect newWindowFrame = [_window frame];
    newWindowFrame.size.width = newFrame.size.width + 610;
    if (newWindowFrame.size.width < 831) newWindowFrame.size.width = 831;
    if (newWindowFrame.size.width > 961) newWindowFrame.size.width = 961;
    [_window setFrame:newWindowFrame display:NO];
    newFrame.size.width = [_pleaseDoAdjust frame].size.width + 0;
    [_softwareListSegmentedControl setWidth:([_pleaseDoAdjust frame].size.width - 89) forSegment:3];
    [_softwareListSegmentedControl setFrame:newFrame];
    _isResizing = NO;
}

- (CGFloat)splitView:(NSSplitView *)splitView constrainSplitPosition:(CGFloat)proposedPosition ofSubviewAt:(NSInteger)dividerIndex {
    if (proposedPosition < 220.0f) {
        return 220.0f;
    }
    if (proposedPosition > 350.0f) {
        return 350.0f;
    }
    return proposedPosition;
}

@end
