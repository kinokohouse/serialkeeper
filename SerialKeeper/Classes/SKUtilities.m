//
//  SKUtilities.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 08-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKUtilities.h"

@implementation SKUtilities

+ (NSString *)createUUID {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge_transfer NSString *)string;
}

+ (void)showAlertWithMessageText:(NSString *)topText informativeText:(NSString *)mainText {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:topText];
    [alert setInformativeText:mainText];
    [alert addButtonWithTitle:@"OK"];
    [alert setAlertStyle:NSCriticalAlertStyle];
    [alert runModal];
}

+ (NSImage *)iconWithSystemReference:(NSString *)string {
    NSImage *newImage = [[NSImage alloc] init];
    NSImage *intermediate = [[NSImage alloc] initWithContentsOfFile:[NSString stringWithFormat:@"/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/%@.icns", string]];
    NSArray *reps = [intermediate representations];
    BOOL repFound = NO;
    for (int p = 0; p < [reps count]; p++) {
        if ([(NSImageRep *)[reps objectAtIndex:p] pixelsWide] == 256) {
            repFound = YES;
            [newImage addRepresentation:[reps objectAtIndex:p]];
            break;
        }
    }
    if (!repFound) {
        [newImage addRepresentation:[reps objectAtIndex:0]];
    }
    return newImage;
}

+ (NSString *)getNameForCopy:(NSString *)theName {
    NSMutableArray *theParts = [[theName componentsSeparatedByString:@" "] mutableCopy];
    NSUInteger currentCount = [[theParts lastObject] integerValue];
    if (currentCount < 2 || [theParts count] == 1) {
        return [NSString stringWithFormat:@"%@ 2", theName];
    } else {
        [theParts removeLastObject];
        return [NSString stringWithFormat:@"%@ %lu", [theParts componentsJoinedByString:@" "], (currentCount + 1)];
    }
}

@end
