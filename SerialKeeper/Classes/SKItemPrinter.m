//
//  SKItemPrinter.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 23-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKItemPrinter.h"

@implementation SKItemPrinter

- (IBAction)print:(id)sender {
    BOOL result = [_outlineViewController preparePrintViewWithCurrentSelection];
    if (!result) return;
    [_bottomLine setStringValue:NSLocalizedString(@"Printed from SerialKeeper by Kinoko House", nil)];
    NSPrintOperation* printOperation = [NSPrintOperation printOperationWithView:_printView];
    [printOperation setCanSpawnSeparateThread:YES];
    [printOperation runOperationModalForWindow:_window delegate:_window didRunSelector:nil contextInfo:nil];
}

- (IBAction)pagesetup:(id)sender {
    NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
    NSPageLayout *pageLayout = [NSPageLayout pageLayout];
    [pageLayout beginSheetWithPrintInfo:printInfo modalForWindow:_window delegate:self didEndSelector:@selector(pageLayoutDidEnd:returnCode:contextInfo:) contextInfo:nil];
}

- (void)pageLayoutDidEnd:(NSPageLayout *)pageLayout returnCode:(int)returnCode contextInfo:(void *)contextInfo {
    if (returnCode == NSOKButton) {
        // do something here (or not)
    }
}


@end
