//
//  AppDelegate.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 25-12-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "SKBox.h"
#import "SKIconView.h"
#import "SKAccessoryView.h"
#import "SKOutlineView.h"
#import "SKOutlineViewController.h"
#import "SKUtilities.h"


@interface AppDelegate : NSObject <NSApplicationDelegate>


#pragma mark Shareable IBOutlets


@property __unsafe_unretained IBOutlet NSWindow *window;
@property __unsafe_unretained IBOutlet NSView *doNotAdjust;
@property __unsafe_unretained IBOutlet NSView *pleaseDoAdjust;
@property __unsafe_unretained IBOutlet NSWindow *customItemSheet;
@property __unsafe_unretained IBOutlet NSTextField *customItemTextField;
@property __unsafe_unretained IBOutlet NSPopUpButton *iconPopUpButton;
@property __unsafe_unretained IBOutlet NSPopUpButton *changeIconPopUpButton;


#pragma mark Shareable Properties

@property (assign) NSModalResponse pwWindowResponse;
@property BOOL interactionAllowed;


#pragma mark Prefs related IBOutlets

@property __unsafe_unretained IBOutlet NSWindow *prefSheet;
@property __unsafe_unretained IBOutlet NSButton *usePasswordCheckBox;
@property __unsafe_unretained IBOutlet NSButton *useEncryptionCheckBox;
@property __unsafe_unretained IBOutlet NSButton *preventDetailDragCheckBox;
@property __unsafe_unretained IBOutlet NSTextField *preventDetailDragBlurb;
@property __unsafe_unretained IBOutlet NSButton *userConfirmationForDeleteCheckBox;
@property __unsafe_unretained IBOutlet NSButton *preventQuickLookForAttachmentsCheckBox;
@property __unsafe_unretained IBOutlet NSTextField *preventQuickLookForAttachmentsBlurb;
@property __unsafe_unretained IBOutlet NSButton *saveAfterEveryChangeCheckBox;
@property __unsafe_unretained IBOutlet NSTextField *customLabelRegkeyTextField;
@property __unsafe_unretained IBOutlet NSTextField *customLabelNameTextField;
@property __unsafe_unretained IBOutlet NSTextField *customLabelEmailTextField;
@property __unsafe_unretained IBOutlet NSTextField *customLabelSerialTextField;
@property __unsafe_unretained IBOutlet NSTextField *customLabelRegkeyLabel;
@property __unsafe_unretained IBOutlet NSTextField *customLabelNameLabel;
@property __unsafe_unretained IBOutlet NSTextField *customLabelEmailLabel;
@property __unsafe_unretained IBOutlet NSTextField *customLabelSerialLabel;
@property __unsafe_unretained IBOutlet NSPopUpButton *languageSelectionPopUpButton;


#pragma mark Prefs related properties

@property (assign) BOOL usePassword;
@property (assign) BOOL useEncryption;
@property (assign) BOOL preventDetailDrag;
@property (assign) BOOL userConfirmationForDelete;
@property (assign) BOOL preventQuickLookForAttachments;
@property (assign) BOOL saveAfterEveryChange;
@property (assign) NSRect scrollViewState;
@property (strong) NSString *customLabelRegkey;
@property (strong) NSString *customLabelName;
@property (strong) NSString *customLabelEmail;
@property (strong) NSString *customLabelSerial;
@property (strong) NSMutableDictionary *previousPrefs;
@property (strong) NSString *selectedUuid;
@property (assign) int preferredLanguage;
@property (assign) int currentlyActiveLanguage;
@property (assign) BOOL alreadyAskedOnce;


#pragma mark Password Sheet IBOutlets

@property __unsafe_unretained IBOutlet NSWindow *passwordSheet;
@property __unsafe_unretained IBOutlet NSSecureTextField *secureTextField;


#pragma mark Password Window IBOutlets

@property __unsafe_unretained IBOutlet NSWindow *passwordWindow;
@property __unsafe_unretained IBOutlet NSTextField *messageLabel;
@property __unsafe_unretained IBOutlet NSSecureTextField *secureTextFieldA;
@property __unsafe_unretained IBOutlet NSSecureTextField *secureTextFieldB;


#pragma mark Remove Password Window IBOutlets

@property __unsafe_unretained IBOutlet NSWindow *removePasswordWindow;
@property __unsafe_unretained IBOutlet NSTextField *altMessageLabel;
@property __unsafe_unretained IBOutlet NSSecureTextField *removePWSecureTextField;


#pragma mark Password related properties

@property (strong) NSString *password;
@property (strong) NSString *userHash;
@property __unsafe_unretained NSSecureTextField *currentPasswordField;



- (void)continueFinishLaunchingFromDeferredLoad;
- (void)continueFinishLaunching;
- (void)getPrefs;
- (void)applyPrefs;
- (void)removeImportSheet;
- (void)removeImportMultiSheet;
- (void)setChangeDetected;
- (void)iconDoubleClicked;
- (void)enableRemoveMenuItem;
- (void)disableRemoveMenuItem;

@end

