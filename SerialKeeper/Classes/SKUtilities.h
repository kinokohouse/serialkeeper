//
//  SKUtilities.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 08-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>


@interface SKUtilities : NSObject


#pragma mark SKUtilities Class Methods

+ (NSString *) createUUID;
+ (void)showAlertWithMessageText:(NSString *)topText informativeText:(NSString *)mainText;
+ (NSImage *)iconWithSystemReference:(NSString *)string;
+ (NSString *)getNameForCopy:(NSString *)theName;

@end
