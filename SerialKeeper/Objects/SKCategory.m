//
//  Category.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKCategory.h"

@implementation SKCategory

- (id)init {
    if (self = [super init]) {
        [self setUuid:[SKUtilities createUUID]];
        [self setCategory:@""];
        [self setDescript:@""];
        [self setApplications:[[NSMutableArray alloc] init]];
    }
    return self;
}

- (id)initWithCategory:(NSString *)category {
    if (self = [super init]) {
        [self setUuid:[SKUtilities createUUID]];
        [self setCategory:category];
        [self setDescript:@""];
        [self setApplications:[[NSMutableArray alloc] init]];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    SKCategory *newCategory = [[[self class] allocWithZone:zone] init];
    if (newCategory) {
        [newCategory setUuid:[self uuid]];
        [newCategory setCategory:[self category]];
        [newCategory setDescript:[self descript]];
        [newCategory setApplications:[self applications]];
        [newCategory setOriginalIndex:[self originalIndex]];
    }
    return newCategory;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self setUuid:[aDecoder decodeObjectForKey:@"uuid"]];
        [self setCategory:[aDecoder decodeObjectForKey:@"category"]];
        [self setDescript:[aDecoder decodeObjectForKey:@"descript"]];
        [self setApplications:[aDecoder decodeObjectForKey:@"applications"]];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[self uuid] forKey:@"uuid"];
    [aCoder encodeObject:[self category] forKey:@"category"];
    [aCoder encodeObject:[self descript] forKey:@"descript"];
    [aCoder encodeObject:[self applications] forKey:@"applications"];
}

- (void)addApplication:(SKApplication *)application {
    [_applications addObject:application];
    NSArray *sortArray = [_applications copy];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"product" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    _applications = [[sortArray sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
}

- (void)sortApplications {
    NSArray *sortArray = [_applications copy];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"product" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    _applications = [[sortArray sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
}

- (void)addMultipleApplications:(NSArray <SKApplication *> *)apps {
    [_applications addObjectsFromArray:apps];
    NSArray *sortArray = [_applications copy];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"product" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    _applications = [[sortArray sortedArrayUsingDescriptors:@[sortDescriptor]] mutableCopy];
}


- (void)removeApplication:(SKApplication *)application {
    if ([_applications count] < 1) return;
    [_applications removeObject:application];
}

- (void)removeApplicationWithUUID:(NSString *)uuid {
    if ([_applications count] < 1) return;
    for (SKApplication *app in _applications) {
        if ([[app uuid] isEqualToString:uuid]) {
            [_applications removeObject:app];
            break;
        }
    }
}

@end
