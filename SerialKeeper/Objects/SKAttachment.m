//
//  SKAttachment.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKAttachment.h"

@implementation SKAttachment

- (id)init {
    if (self = [super init]) {
        [self setFilename:@""];
        [self setDocument:[[NSData alloc] init]];
    }
    return self;
}

- (id)initWithFilename:(NSString *)filename document:(NSData *)document {
    if (self = [super init]) {
        [self setFilename:filename];
        [self setDocument:document];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    SKAttachment *newAttachment = [[[self class] allocWithZone:zone] init];
    if (newAttachment) {
        [newAttachment setFilename:[self filename]];
        [newAttachment setDocument:[self document]];
    }
    return newAttachment;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self setFilename:[aDecoder decodeObjectForKey:@"filename"]];
        [self setDocument:[aDecoder decodeObjectForKey:@"document"]];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[self filename] forKey:@"filename"];
    [aCoder encodeObject:[self document] forKey:@"document"];
}

@end
