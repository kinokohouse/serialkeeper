//
//  SKApplication.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKApplication.h"

@implementation SKApplication

- (id)init {
    if (self = [super init]) {
        [self setUuid:[SKUtilities createUUID]];
        [self setProduct:@""];
        [self setVersion:@""];
        [self setIcon:[[NSImage imageNamed:@"NSApplicationIcon"] TIFFRepresentation]];
        [self setDescript:@""];
        [self setRegistration:@""];
        [self setRegname:@""];
        [self setPurchasedate:[NSDate dateWithTimeIntervalSinceNow:0.0]];
        [self setEmail:@""];
        [self setSerial:@""];
        [self setCompany:@""];
        [self setWebsite:@""];
        [self setNotes:@""];
        [self setAttachments:[@[] mutableCopy]];
    }
    return self;
}

- (id)initWithProduct:(NSString *)product icon:(NSData *)icon version:(NSString *)version company:(NSString *)company website:(NSString *)website {
    if (self = [super init]) {
        [self setUuid:[SKUtilities createUUID]];
        [self setProduct:product];
        [self setVersion:version];
        [self setIcon:icon];
        [self setDescript:@""];
        [self setRegistration:@""];
        [self setRegname:@""];
        [self setPurchasedate:[NSDate dateWithTimeIntervalSinceNow:0.0]];
        [self setEmail:@""];
        [self setSerial:@""];
        [self setCompany:company];
        [self setWebsite:website];
        [self setNotes:@""];
        [self setAttachments:[@[] mutableCopy]];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    SKApplication *newApplication = [[[self class] allocWithZone:zone] init];
    if (newApplication) {
        [newApplication setUuid:[self uuid]];
        [newApplication setProduct:[self product]];
        [newApplication setVersion:[self version]];
        [newApplication setIcon:[self icon]];
        [newApplication setDescript:[self descript]];
        [newApplication setRegistration:[self registration]];
        [newApplication setRegname:[self regname]];
        [newApplication setPurchasedate:[self purchasedate]];
        [newApplication setEmail:[self email]];
        [newApplication setSerial:[self serial]];
        [newApplication setCompany:[self company]];
        [newApplication setWebsite:[self website]];
        [newApplication setNotes:[self notes]];
        [newApplication setAttachments:[self attachments]];
        [newApplication setOriginalIndex:[self originalIndex]];
        [newApplication setParentIndex:[self parentIndex]];
    }
    return newApplication;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self setUuid:[aDecoder decodeObjectForKey:@"uuid"]];
        [self setProduct:[aDecoder decodeObjectForKey:@"product"]];
        [self setVersion:[aDecoder decodeObjectForKey:@"version"]];
        [self setIcon:[aDecoder decodeObjectForKey:@"icon"]];
        [self setDescript:[aDecoder decodeObjectForKey:@"descript"]];
        [self setRegistration:[aDecoder decodeObjectForKey:@"registration"]];
        [self setRegname:[aDecoder decodeObjectForKey:@"regname"]];
        [self setPurchasedate:[aDecoder decodeObjectForKey:@"purchasedate"]];
        [self setEmail:[aDecoder decodeObjectForKey:@"email"]];
        [self setSerial:[aDecoder decodeObjectForKey:@"serial"]];
        [self setCompany:[aDecoder decodeObjectForKey:@"company"]];
        [self setWebsite:[aDecoder decodeObjectForKey:@"website"]];
        [self setNotes:[aDecoder decodeObjectForKey:@"notes"]];
        [self setAttachments:[aDecoder decodeObjectForKey:@"attachments"]];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[self uuid] forKey:@"uuid"];
    [aCoder encodeObject:[self product] forKey:@"product"];
    [aCoder encodeObject:[self version] forKey:@"version"];
    [aCoder encodeObject:[self icon] forKey:@"icon"];
    [aCoder encodeObject:[self descript] forKey:@"descript"];
    [aCoder encodeObject:[self registration] forKey:@"registration"];
    [aCoder encodeObject:[self regname] forKey:@"regname"];
    [aCoder encodeObject:[self purchasedate] forKey:@"purchasedate"];
    [aCoder encodeObject:[self email] forKey:@"email"];
    [aCoder encodeObject:[self serial] forKey:@"serial"];
    [aCoder encodeObject:[self company] forKey:@"company"];
    [aCoder encodeObject:[self website] forKey:@"website"];
    [aCoder encodeObject:[self notes] forKey:@"notes"];
    [aCoder encodeObject:[self attachments] forKey:@"attachments"];
}

- (void)addAttachment:(SKAttachment *)attachment {
    [_attachments addObject:attachment];
}

- (void)removeAttachment:(SKAttachment *)attachment {
    [_attachments removeObject:attachment];
}

@end

