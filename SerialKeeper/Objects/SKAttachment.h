//
//  SKAttachment.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SKUtilities.h"

@interface SKAttachment : NSObject <NSCoding>

@property (copy) NSString *filename;
@property (copy) NSData *document;

- (id)init;
- (id)initWithFilename:(NSString *)filename document:(NSData *)document;
- (id)copyWithZone:(NSZone *)zone;
- (instancetype)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

@end
