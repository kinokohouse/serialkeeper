//
//  Category.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SKApplication.h"
#import "SKUtilities.h"

@interface SKCategory : NSObject <NSCoding>

@property (copy) NSString *uuid;
@property (copy) NSString *category;
@property (copy) NSString *descript;
@property (retain) NSMutableArray <SKApplication *> *applications;
@property (assign) NSUInteger originalIndex;

- (id)init;
- (id)initWithCategory:(NSString *)category;
- (id)copyWithZone:(NSZone *)zone;
- (instancetype)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;
- (void)addApplication:(SKApplication *)application;
- (void)sortApplications;
- (void)addMultipleApplications:(NSArray <SKApplication *> *)apps;
- (void)removeApplication:(SKApplication *)application;
- (void)removeApplicationWithUUID:(NSString *)uuid;

@end
