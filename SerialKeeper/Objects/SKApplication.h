//
//  SKApplication.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import "SKAttachment.h"
#import "SKUtilities.h"

@interface SKApplication : NSObject <NSCoding>

@property (copy) NSString *uuid;
@property (copy) NSString *product;
@property (copy) NSString *version;
@property (copy) NSData *icon;
@property (copy) NSString *descript;
@property (copy) NSString *registration;
@property (copy) NSString *regname;
@property (copy) NSDate *purchasedate;
@property (copy) NSString *email;
@property (copy) NSString *serial;
@property (copy) NSString *company;
@property (copy) NSString *website;
@property (copy) NSString *notes;
@property (retain) NSMutableArray <SKAttachment *> *attachments;
@property (assign) NSUInteger originalIndex;
@property (assign) NSUInteger parentIndex;

- (id)init;
- (id)initWithProduct:(NSString *)product icon:(NSData *)icon version:(NSString *)version company:(NSString *)company website:(NSString *)website;
- (id)copyWithZone:(NSZone *)zone;
- (instancetype)initWithCoder:(NSCoder *)aDecoder;
- (void)encodeWithCoder:(NSCoder *)aCoder;

- (void)addAttachment:(SKAttachment *)attachment;
- (void)removeAttachment:(SKAttachment *)attachment;

@end
