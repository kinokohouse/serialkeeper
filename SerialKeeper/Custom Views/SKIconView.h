//
//  SKIconView.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  This subclass is based on the work of Bithaus:
//  https://github.com/gnyrd/Flected
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SKIconView : NSView

@property __unsafe_unretained IBOutlet NSImageView *iconImage;
@property __unsafe_unretained IBOutlet NSImageView *reflectedImage;

- (void)setImage:(NSImage *)image;

@end
