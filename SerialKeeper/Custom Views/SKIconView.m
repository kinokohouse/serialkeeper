//
//  SKIconView.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  This subclass is based on the work of Bithaus:
//  https://github.com/gnyrd/Flected
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKIconView.h"

@implementation SKIconView

- (void)setImage:(NSImage *)image {
    if (image != nil) {
        NSImage *reflection = [[NSImage alloc] initWithSize:[image size]];
        [reflection lockFocusFlipped:YES];
        NSGradient *fade = [[NSGradient alloc] initWithStartingColor:[NSColor colorWithCalibratedWhite:1.0 alpha:0.5] endingColor:[NSColor clearColor]];
        [fade drawInRect:NSMakeRect(0, 0, [image size].width, [image size].height*0.6) angle:90.0];
        [image drawAtPoint:NSMakePoint(0,0) fromRect:NSZeroRect operation:NSCompositingOperationSourceIn fraction:1.0];
        [reflection unlockFocus];
        [_iconImage setImage:image];
        [_reflectedImage setImage:reflection];
    }
}

@end
