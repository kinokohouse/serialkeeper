//
//  SKAccessoryView.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 31-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SKAccessoryView : NSView

@property __unsafe_unretained IBOutlet NSPopUpButton *accessoryViewPopUpButton;

@end
