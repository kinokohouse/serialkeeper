//
//  NSString+StringSearch.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "NSString+StringSearch.h"

@implementation NSString (StringSearch)

- (BOOL)contains:(NSString *)string {
    NSRange foundRange = [self rangeOfString:string options:NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch | NSWidthInsensitiveSearch];
    return (foundRange.location != NSNotFound);
}

- (NSString *)truncate {
    NSUInteger theLength = 40;
    if ([self length] < (theLength + 4)) return self;
    NSRange firstHalfRange = NSMakeRange(0,(theLength / 2));
    NSRange secondHalfRange = NSMakeRange([self length] - (theLength / 2), (theLength / 2));
    firstHalfRange = [self rangeOfComposedCharacterSequencesForRange:firstHalfRange];
    secondHalfRange = [self rangeOfComposedCharacterSequencesForRange:secondHalfRange];
    NSString *firstHalf = [self substringWithRange:firstHalfRange];
    NSString *secondHalf = [self substringWithRange:secondHalfRange];
    return [NSString stringWithFormat:@"%@...%@", firstHalf, secondHalf];
}


@end
