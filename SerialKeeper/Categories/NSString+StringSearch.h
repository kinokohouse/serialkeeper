//
//  NSString+StringSearch.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (StringSearch)

- (BOOL)contains:(NSString *)string;
- (NSString *)truncate;

@end
