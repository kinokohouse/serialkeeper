//
//  NSString+MD5.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 05/11/2021.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (MD5)

- (NSString *)MD5String;

@end

NS_ASSUME_NONNULL_END
