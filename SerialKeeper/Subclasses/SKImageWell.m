//
//  SKImageWell.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 04-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKImageWell.h"
#import "AppDelegate.h"

@implementation SKImageWell

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender {
    return NSDragOperationCopy;
}

- (void)concludeDragOperation:(id<NSDraggingInfo>)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [super concludeDragOperation:sender];
    if ([[self window] isEqual:[appDelegate customItemSheet]]) {
        [[appDelegate iconPopUpButton] selectItemAtIndex:0];
    } else {
        [[appDelegate changeIconPopUpButton] selectItemAtIndex:0];
    }
}

@end
