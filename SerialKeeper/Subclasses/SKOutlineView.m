//
//  SKOutlineView.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 19-03-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKOutlineView.h"

@implementation SKOutlineView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (NSView *)hitTest:(NSPoint)aPoint {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (![appDelegate interactionAllowed]) return nil;
    else return [super hitTest:aPoint];
}

- (BOOL)becomeFirstResponder {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate enableRemoveMenuItem];
    return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate disableRemoveMenuItem];
    return [super resignFirstResponder];
}

- (NSRect)frameOfOutlineCellAtRow:(NSInteger)row {
    return [super frameOfOutlineCellAtRow:row];
}

- (NSMenu *)menuForEvent:(NSEvent *)theEvent {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSPoint pt = [self convertPoint:[theEvent locationInWindow] fromView:nil];
    _rowForMenu = [self rowAtPoint:pt];
    id item = [self itemAtRow: _rowForMenu];
    if([self selectedRow] < 0) return nil;
    NSString *theName;
    NSString *theType;
    NSString *thePoint = @"";
    if ([[appDelegate valueForKey:@"userConfirmationForDelete"] boolValue]) thePoint = @"...";
    NSMenuItem *menuItem2;
    NSImage *theImage = [SKUtilities iconWithSystemReference:@"GenericFolderIcon"];
    if ([[item class] isEqual:[SKCategory class]]) {
        theName = [item category];
        theType = NSLocalizedString(@"category", nil);
    } else {
        theName = [item product];
        theType = NSLocalizedString(@"item", nil);
        theImage = [[NSImage alloc] initWithData:[(SKApplication *)item icon]];
    }
    [theImage setSize:NSMakeSize(24, 24)];
    NSMenu *theMenu = [[NSMenu alloc] init];
    NSMenuItem *menuItem_1 = [[NSMenuItem alloc] initWithTitle:theName action:@selector(selectItemAtRow) keyEquivalent:@""];
    [menuItem_1 setImage:theImage];
    NSMenuItem *menuItem0 = [NSMenuItem separatorItem];
    NSMenuItem *menuItem1 = [[NSMenuItem alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Delete %@%@", nil), theType, thePoint] action:@selector(removeItemAtRow) keyEquivalent:@""];
    if ([theType isEqualToString:NSLocalizedString(@"item", nil)]) {
        menuItem2 = [[NSMenuItem alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Duplicate %@", nil), theType] action:@selector(duplicateItemAtRow) keyEquivalent:@""];
    }    [theMenu addItem:menuItem_1];
    NSMenuItem *menuItem3 = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Edit name...", nil) action:@selector(editItemAtRow) keyEquivalent:@""];
    [theMenu addItem:menuItem0];
    [theMenu addItem:menuItem1];
    if ([theType isEqualToString:NSLocalizedString(@"item", nil)]) {
        [theMenu addItem:menuItem2];
    }
    [theMenu addItem:menuItem3];
    return theMenu;
}

- (BOOL)shouldCollapseAutoExpandedItemsForDeposited:(BOOL)deposited {
    return [_outlineViewController searchActive];
}

- (void)removeItemAtRow {
    [_outlineViewController removeItemAtRow:_rowForMenu];
}

- (void)duplicateItemAtRow {
    [_outlineViewController duplicateItemAtRow:_rowForMenu];
}

- (void)selectItemAtRow {
    [_outlineViewController selectItemAtRow:_rowForMenu];
}

- (void)editItemAtRow {
    [_outlineViewController editItemAtRow:_rowForMenu];
}
@end
