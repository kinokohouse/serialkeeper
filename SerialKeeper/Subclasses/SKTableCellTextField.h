//
//  SKTableCellTextField.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 05/02/2021.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SKOutlineViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SKTableCellTextField : NSTextField
@property __unsafe_unretained IBOutlet SKOutlineViewController *outlineViewController;

@end

NS_ASSUME_NONNULL_END
