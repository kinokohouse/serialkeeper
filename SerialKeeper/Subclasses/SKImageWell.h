//
//  SKImageWell.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 04-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SKImageWell : NSImageView

@end
