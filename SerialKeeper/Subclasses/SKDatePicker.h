//
//  SKDatePicker.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 21-03-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SKOutlineViewController.h"

@interface SKDatePicker : NSDatePicker

@property __unsafe_unretained IBOutlet SKOutlineViewController *outlineViewController;

@end
