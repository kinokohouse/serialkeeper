//
//  SKTableView.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 01-04-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "SKTableViewController.h"
#import "SKAttachment.h"
#import "NSString+StringSearch.h"
#import "AppDelegate.h"

@interface SKTableView : NSTableView <QLPreviewPanelDelegate>

@property __unsafe_unretained IBOutlet SKTableViewController *tableViewController;

@property (assign) NSInteger rowForMenu;

- (void)setQLPanelDelegateAndDataSource;
- (IBAction)togglePreviewPanel:(id)sender;
- (void)updateQLPanelIfNecessary;

@end
