//
//  SKBox.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 21/01/2020.
//  Copyright © 2020 Kinoko House. All rights reserved.
//

#import "SKBox.h"

@implementation SKBox

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (NSView *)hitTest:(NSPoint)aPoint {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    if (![appDelegate interactionAllowed]) return nil;
    else return [super hitTest:aPoint];
}

@end
