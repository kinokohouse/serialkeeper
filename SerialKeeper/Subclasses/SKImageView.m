//
//  SKImageView.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 21-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKImageView.h"
#import "AppDelegate.h"

@implementation SKImageView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (void)mouseDown:(NSEvent *)event {
    if ([event clickCount] == 2) {
        AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
        [appDelegate iconDoubleClicked];
    }
}

@end
