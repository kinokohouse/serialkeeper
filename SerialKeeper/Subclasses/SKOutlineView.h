//
//  SKOutlineView.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 19-03-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SKCategory.h"
#import "SKApplication.h"
#import "SKOutlineViewController.h"
#import "AppDelegate.h"

@interface SKOutlineView : NSOutlineView

@property __unsafe_unretained IBOutlet SKOutlineViewController *outlineViewController;
@property __unsafe_unretained IBOutlet NSScrollView *scrollView;
@property (assign) NSInteger rowForMenu;
@property (strong) NSTimer *altTimer;

@end
