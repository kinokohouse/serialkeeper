//
//  SKSecureTextField.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 21/01/2020.
//  Copyright © 2020 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface SKSecureTextField : NSSecureTextField

@end

NS_ASSUME_NONNULL_END
