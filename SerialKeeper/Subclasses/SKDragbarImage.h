//
//  SKDragbarImage.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 26/04/2020.
//  Copyright © 2020 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface SKDragbarImage : NSImageView

@end

NS_ASSUME_NONNULL_END
