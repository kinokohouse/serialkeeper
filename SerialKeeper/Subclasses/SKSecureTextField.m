//
//  SKSecureTextField.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 21/01/2020.
//  Copyright © 2020 Kinoko House. All rights reserved.
//

#import "SKSecureTextField.h"
#import "AppDelegate.h"

@implementation SKSecureTextField

- (BOOL)becomeFirstResponder {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate setCurrentPasswordField:self];
    return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    return [super resignFirstResponder];
}
@end
