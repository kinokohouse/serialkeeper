//
//  SKPasswordRevealButton.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 20/10/2019.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKPasswordRevealButton.h"

@interface SKPasswordRevealButton()

@property __unsafe_unretained IBOutlet NSSecureTextField *passwordField;
@property __unsafe_unretained IBOutlet NSTextField *clearField;

@property (assign) NSRange selection;

@end

@implementation SKPasswordRevealButton

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (void)mouseDown:(NSEvent *)event {
    [self setEnabled:YES];
    NSText *fieldEditor = [_passwordField currentEditor];
    _selection = [fieldEditor selectedRange];
    [_clearField setStringValue:[_passwordField stringValue]];
    [_passwordField setHidden:YES];
    [_clearField setHidden:NO];
    [[_clearField window] makeFirstResponder:_clearField];
}

- (void)mouseUp:(NSEvent *)event {
    [self setEnabled:NO];
    [_passwordField setHidden:NO];
    [_clearField setHidden:YES];
    [[_passwordField window] makeFirstResponder:_passwordField];
    NSText *fieldEditor = [_passwordField currentEditor];
    [fieldEditor setSelectedRange:_selection];
}

@end
