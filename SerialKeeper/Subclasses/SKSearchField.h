//
//  SKSearchField.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 05-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SKOutlineViewController.h"

@interface SKSearchField : NSSearchField

@property __unsafe_unretained IBOutlet SKOutlineViewController *outlineViewController;

@end
