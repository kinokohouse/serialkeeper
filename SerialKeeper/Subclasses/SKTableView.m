//
//  SKTableView.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 01-04-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Quartz/Quartz.h>
#import <QuickLook/QuickLook.h>

#import "SKTableView.h"

@implementation SKTableView

- (NSMenu *)menuForEvent:(NSEvent *)event {
    id appDelegate = [NSApp delegate];
    BOOL confirmationNeeded = [[appDelegate valueForKey:@"userConfirmationForDelete"] boolValue];
    NSPoint pt = [self convertPoint:[event locationInWindow] toView:nil];
    pt.x = 1;
    _rowForMenu = [self rowAtPoint:pt];
    if (_rowForMenu == -1) return nil;
    SKAttachment *attachment = [[_tableViewController attachmentList] objectAtIndex:_rowForMenu];
    NSString *thePoint = @"";
    if (confirmationNeeded) thePoint = @"...";
    NSString *theName = [attachment filename];
    NSImage *theImage;
    NSMenuItem *theMenuItem;
    NSMenu *theMenu = [[NSMenu alloc] init];
    if (!([[self selectedRowIndexes] count] > 1) || ![[self selectedRowIndexes] containsIndex:_rowForMenu]) {
        theName = [attachment filename];
        theImage = [[NSWorkspace sharedWorkspace] iconForFileType:[theName pathExtension]];
        [theImage setSize:NSMakeSize(24, 24)];
        theMenuItem = [[NSMenuItem alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Remove '%@'%@", nil), [theName truncate], thePoint] action:@selector(deleteItemAtRow) keyEquivalent:@""];
        [theMenuItem setImage:theImage];
        [theMenu addItem:theMenuItem];
    } else {
        theMenuItem = [[NSMenuItem alloc] initWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Remove selected documents%@", nil), thePoint] action:@selector(deleteMultiSelection) keyEquivalent:@""];
        [theMenu addItem:theMenuItem];
    }
    return theMenu;
}

- (BOOL)becomeFirstResponder {
    BOOL result = [super becomeFirstResponder];
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    NSUInteger theSelection = [[self selectedRowIndexes] firstIndex];
    if (theSelection == NSNotFound) {
        [appDelegate disableRemoveMenuItem];
    } else {
        [appDelegate enableRemoveMenuItem];
    }
    return result;
}

- (BOOL)resignFirstResponder {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    [appDelegate disableRemoveMenuItem];
    return [super resignFirstResponder];
}

- (void)deleteItemAtRow {
    id appDelegate = [NSApp delegate];
    SKAttachment *attachment = [[_tableViewController attachmentList] objectAtIndex:_rowForMenu];
    NSString *theFullName = [attachment filename];
    NSString *theShortName = [theFullName truncate];
    NSImage *theImage = [[NSImage alloc] init];
    theImage = [[NSWorkspace sharedWorkspace] iconForFileType:[theFullName pathExtension]];
    NSString *msgText = [NSString stringWithFormat:NSLocalizedString(@"Remove document '%@'", nil), theShortName];
    NSString *infText = [NSString stringWithFormat:NSLocalizedString(@"This will remove document '%@'. Are you sure you want to continue?", nil), theFullName];
    if ([[appDelegate valueForKey:@"userConfirmationForDelete"] boolValue] == YES) {
        NSAlert *theAlert = [NSAlert alertWithMessageText:msgText defaultButton:NSLocalizedString(@"Cancel", nil) alternateButton:nil otherButton:NSLocalizedString(@"OK", nil) informativeTextWithFormat:@"%@", infText];
        [theAlert setIcon:theImage];
        NSModalResponse theResponse = [theAlert runModal];
        if (theResponse == NSAlertDefaultReturn) {
            return;
        }
    }
    NSMutableArray *newArray = [[_tableViewController attachmentList] mutableCopy];
    [newArray removeObjectAtIndex:_rowForMenu];
    [_tableViewController setAttachmentList:[newArray mutableCopy]];
    [self reloadData];
}

- (void)deleteMultiSelection {
    [_tableViewController removeCurrentlySelectedAttachments];
}

#pragma mark QLPreviewPanel Delegate

- (BOOL)acceptsPreviewPanelControl:(QLPreviewPanel *)panel {
    AppDelegate *appDelegate = [NSApp delegate];
    return ![appDelegate preventQuickLookForAttachments];
}

- (void)beginPreviewPanelControl:(QLPreviewPanel *)panel {
    [self setQLPanelDelegateAndDataSource];
    [_tableViewController initiateQLPanel];
}

- (void)setQLPanelDelegateAndDataSource {
    [[QLPreviewPanel sharedPreviewPanel] setDelegate:self];
    [[QLPreviewPanel sharedPreviewPanel] setDataSource:_tableViewController];
}

- (void)endPreviewPanelControl:(QLPreviewPanel *)panel {
    [[QLPreviewPanel sharedPreviewPanel] setDelegate:nil];
    [[QLPreviewPanel sharedPreviewPanel] setDataSource:nil];
    [_tableViewController cleanUpQLPanel];
}

- (BOOL)previewPanel:(QLPreviewPanel *)panel handleEvent:(NSEvent *)event {
    if ([event type] == NSKeyDown) {
        [self keyDown:event];
        return YES;
    }
    return NO;
}


#pragma mark QLPreviewPanel Utilities

- (void)keyDown:(NSEvent *)theEvent {
    NSString *key = [theEvent charactersIgnoringModifiers];
    if ([key isEqual:@" "]) {
        if ([[self selectedRowIndexes] count] == 0) {
            [super keyDown:theEvent];
        } else {
            [self togglePreviewPanel:self];
        }
    } else {
        [super keyDown:theEvent];
    }
}

- (void)updateQLPanelIfNecessary {
    if ([QLPreviewPanel sharedPreviewPanelExists] && [[QLPreviewPanel sharedPreviewPanel] isVisible]) {
        [self endPreviewPanelControl:[QLPreviewPanel sharedPreviewPanel]];
        [self beginPreviewPanelControl:[QLPreviewPanel sharedPreviewPanel]];
        [[QLPreviewPanel sharedPreviewPanel] reloadData];
    }
}


#pragma mark QLPreviewPanel IBActions

- (IBAction)togglePreviewPanel:(id)sender {
    AppDelegate *appDelegate = [NSApp delegate];
    if ([appDelegate preventQuickLookForAttachments]) return;
    if ([QLPreviewPanel sharedPreviewPanelExists] && [[QLPreviewPanel sharedPreviewPanel] isVisible]) {
        [[QLPreviewPanel sharedPreviewPanel] orderOut:nil];
    } else {
        [[QLPreviewPanel sharedPreviewPanel] makeKeyAndOrderFront:nil];
    }
}

@end
