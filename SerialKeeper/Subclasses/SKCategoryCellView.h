//
//  SKCategoryCellView.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 06-01-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SKCategoryCellView : NSTableCellView

@property __unsafe_unretained IBOutlet NSButton *countButton;

@end
