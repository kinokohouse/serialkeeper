//
//  SKPasswordDoubleRevealButton.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 20/10/2019.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKPasswordDoubleRevealButton.h"
#import "SKSecureTextField.h"
#import "AppDelegate.h"

@interface SKPasswordDoubleRevealButton()

@property __unsafe_unretained IBOutlet NSSecureTextField *ownPasswordField;
@property __unsafe_unretained IBOutlet NSTextField *ownClearField;
@property __unsafe_unretained IBOutlet NSSecureTextField *otherPasswordField;
@property __unsafe_unretained IBOutlet NSTextField *otherClearField;
@property __unsafe_unretained IBOutlet NSTextField *warningField;

@property (assign) id currentPasswordField;
@property (assign) NSRange currentSelection;

@end


@implementation SKPasswordDoubleRevealButton

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (void)mouseDown:(NSEvent *)event {
    AppDelegate *appDelegate = (AppDelegate *)[[NSApplication sharedApplication] delegate];
    _currentPasswordField = [appDelegate currentPasswordField];
    if ((_currentPasswordField == nil) || ([[_currentPasswordField class] isEqualTo:[SKSecureTextField class]])) {
        [appDelegate setCurrentPasswordField:_ownPasswordField];
    }
    [self setEnabled:YES];
    NSText *fieldEditor = [_currentPasswordField currentEditor];
    _currentSelection = [fieldEditor selectedRange];
    fieldEditor = [_currentPasswordField currentEditor];
    [_ownClearField setStringValue:[_ownPasswordField stringValue]];
    [_otherClearField setStringValue:[_otherPasswordField stringValue]];
    if ([[_ownClearField stringValue] isEqualToString:@""] || [[_otherClearField stringValue] isEqualToString:@""]) {
        [_ownClearField setTextColor:[NSColor controlTextColor]];
        [_otherClearField setTextColor:[NSColor controlTextColor]];
        [_warningField setStringValue:NSLocalizedString(@"Field(s) empty.", nil)];
    } else if ([[_ownClearField stringValue] isNotEqualTo:[_otherClearField stringValue]]) {
        [_ownClearField setTextColor:[NSColor redColor]];
        [_otherClearField setTextColor:[NSColor redColor]];
        [_warningField setStringValue:NSLocalizedString(@"Password mismatch.", nil)];
    } else {
        [_ownClearField setTextColor:[NSColor controlTextColor]];
        [_otherClearField setTextColor:[NSColor controlTextColor]];
    }
    [_ownPasswordField setHidden:YES];
    [_otherPasswordField setHidden:YES];
    [_ownClearField setHidden:NO];
    [_otherClearField setHidden:NO];
}

- (void)mouseUp:(NSEvent *)event {
    [self setEnabled:NO];
    [_ownPasswordField setHidden:NO];
    [_otherPasswordField setHidden:NO];
    [_ownClearField setHidden:YES];
    [_otherClearField setHidden:YES];
    [[_ownPasswordField window] makeFirstResponder:_currentPasswordField];
    NSText *fieldEditor = [_currentPasswordField currentEditor];
    [fieldEditor setSelectedRange:_currentSelection];
    [_warningField setStringValue:@""];
}

- (id)getFirstResponder {
    return [[self window] firstResponder];
}

@end
