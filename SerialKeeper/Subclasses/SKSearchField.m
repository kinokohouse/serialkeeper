//
//  SKSearchField.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 05-02-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKSearchField.h"

@implementation SKSearchField

- (void)textDidChange:(NSNotification *)notification {
    [_outlineViewController searchFieldHasChanged];
}

@end
