//
//  SKDatePicker.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 21-03-19.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import "SKDatePicker.h"

@implementation SKDatePicker

- (BOOL)resignFirstResponder {
    if ([_outlineViewController changeDetected]) {
        [_outlineViewController storeUUIDOfLastSelectedItem];
        [_outlineViewController updateIfChanged];
        [_outlineViewController reselectOriginalItem];
        [_outlineViewController setChangeDetected:NO];
    }
    return YES;
}

@end
