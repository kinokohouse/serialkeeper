//
//  SKPasswordRevealButton.h
//  SerialKeeper
//
//  Created by Petros Loukareas on 20/10/2019.
//  Copyright © 2019 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface SKPasswordRevealButton : NSButton

@end

NS_ASSUME_NONNULL_END
