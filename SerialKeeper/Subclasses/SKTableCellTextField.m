//
//  SKTableCellTextField.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 05/02/2021.
//  Copyright © 2021 Kinoko House. All rights reserved.
//

#import "SKTableCellTextField.h"

@implementation SKTableCellTextField

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (BOOL)becomeFirstResponder {
    [_outlineViewController setPreviousItemName:[self stringValue]];
    return [super becomeFirstResponder];
}

@end
