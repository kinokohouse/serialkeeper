//
//  SKDragbarImage.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 26/04/2020.
//  Copyright © 2020 Kinoko House. All rights reserved.
//

#import "SKDragbarImage.h"

@interface SKDragbarImage()

@property (strong) IBOutlet NSSplitView *splitView;
@property (strong) IBOutlet NSView *pleaseDoAdjust;

@property (assign) NSPoint firstCoords;
@property (assign) float difference;
@property (strong) NSCursor *cursor;

@end

@implementation SKDragbarImage

- (void)awakeFromNib {
    _cursor = [NSCursor resizeLeftRightCursor];
}

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
}

- (void)mouseDragged:(NSEvent *)event {
    NSRect frame = [_pleaseDoAdjust frame];
    frame.size.width = [event locationInWindow].x + _difference;
    [_splitView setPosition:frame.size.width ofDividerAtIndex:0];
}

- (void)mouseDown:(NSEvent *)event {
    _firstCoords = [event locationInWindow];
    _difference = [_pleaseDoAdjust frame].size.width - _firstCoords.x;
}

- (void)resetCursorRects {
    if (_cursor) {
        [self addCursorRect:[self bounds] cursor:_cursor];
    } else {
        [super resetCursorRects];
    }
}

@end
