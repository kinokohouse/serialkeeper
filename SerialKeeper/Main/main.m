//
//  main.m
//  SerialKeeper
//
//  Created by Petros Loukareas on 25-12-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
