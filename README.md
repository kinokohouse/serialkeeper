# SerialKeeper #
A fully functional 64-bit clone of the old RapidoSerial app from ADNX (formerly app4mac), with some changes and (hopefully) improvements. Store your (shareware) serial numbers and registration information securely. App is mostly self-explanatory, also gives the choice to do a short tour at first launch. Requires 10.7+. Ready to use binary (unsigned, so launch it with right-click-open when running it for the first time) from the 'Downloads' directory. MIT License.

Here are some of the additions compared to RapidoSerial:

* Importing items is more verbose and versatile (you can decide to import them to a new or an existing category).
* Icons can be changed for all items except categories (not just items you added manually yourself); choose from the 'File' menu or double click on the item's icon on the overview.
* You can add software or items to the software list by dragging them there...
* ...and conversely, items from your software list can be dragged to the Finder; this will give you a PDF with an overview of all the data you entered for that item (as if you printed it from the application). You can disable this feature in the preferences if your database is password protected.
* Documents can be dragged and dropped to the Documents (attachments) list from the Finder, and also the other way around, which will give you copies of the pertinent documents (if this feature has not been disabled in the preferences).
* You can use QuickLook to 'look inside' the included documents (if this feature has not been disabled in the preferences).
* Items can be removed (or in the case of items in the main list, also duplicated) with contextual menus.
* Undo for all actions.


Known limitations
-----------------
* `Export` will always output an unencrypted, non-password-protected SerialKeeper database. If you need to keep the file encrypted offsite, please consider putting it in an encrypted ZIP file.
* Once you upgrade to 2.0 and decide to roll back to 1.95, the preferences for passwords etc. will not be in sync, as password-protected unencrypted databases were not supported in that version. If you decide to roll back, please first remove all forms of password protection and encryption (your preference files are shared between versions). 
* Not really a limitation, but if you forget your password, or if you accidentally mistype it twice the same way when entering a new password (happened to me **twice**, flaky keys :/), YOU HAVE NO WAY OF RETRIEVING YOUR DATA! There are no backdoors, no secret keys to hold during application startup, nothing. Please take appropriate precautions (like watching your keyboard really well while you are typing your new password, keeping an offsite backup of your database file, etc.). Please make good use of the 'password reveal' functionality added from 1.9 on.


Building
--------
Project is made in Xcode 10.3. The sources to RNCryptor by Rob Napier have been included (partially - only the bits that were used).


Version history
---------------
### 2.01 (build 51), current version: ####
* Document list did not update correctly when adding an item by dragging from the Finder - corrected.


### 2.00 (build 50): ####
* Implemented `undo` for **all** actions, destructive *and* constructive.
* Added localization and translations for Dutch and Japanese. By default, it will pick one based on your system preferences, but you can change the application language at any time in the preferences, and this selection will be stored for next time. Changing the language of the application only takes effect after a relaunch (optionally done from within the application); if you choose to relauch the application, you will be guided through the process.
* You now have the option to merely set a password to control access to the database, *without* encryption. This is useful for older machines, for which the burden of encryption may be a bit too high. Of course, you should keep in mind that just controlling access with a password without encryption is a very weak protection of your valuable data, and is only useful as a first line defense.
* The window can now be resized in width as well as in height, to provide for long file and/or folder names.
* Some changes to the source:
    * The structure descriptions for the original RapidoSerial file were removed because, ultimately, who cares?
    * The bits of the RNCryptor sources that are used in this application have now been included for your convenience.

Since everything that I wanted to include is now in the app, I will consider it _feature complete_ and will make no further updates to it, bar bug fixes. I was considering adding help books, but come on now - there is not that much to explain, imagine explaining nothing in three languages.


### 1.95 (build 42): ####
* Implemented `undo` for most destructive actions; please check **Known limitations** above for caveats [EDIT: not anymore, see at 2.0 above].
* The short tour of the app on first launch is now more modal-like (i.e. does not allow any interaction outside of the popovers).
* Confirmation dialogs no longer have the destructive option under the default button. Sorry about that.
* The info in the about box had become unreadable (black on black text when in dark mode). For that I am also sorry.
* The `Remove` menu action and keyboard shortcut are now context-sensitive, and do not do anything unless an item from either list (applications/categories and documents/attachments) is selected.
* Databases (exported or saved) now have a nice icon.
* Some more bugs quashed, of which the most important are:
    * fixed a potential crash when changing the name of a software list item when a search is active;
        * ...and as a consequence, changing the name of a software list item will have immediate effect (e.g. resorting the software list)
    * you couldn't duplicate items during a search by alt-dragging, now you can (you could always duplicate items during a search in any other way, however);
    * assigning new names to duplicates is much more consistent now;
    * assigning a custom icon in search mode now works correctly.

### 1.92 (build 41): ####
* Improvements to the 'double password' dialog when password protecting your database.

### 1.91 (build 40): ####
* Accidentally broke compatibility with 10.7, now fixed again; also used this opportunity to make some improvements to the password entry dialogue.

### 1.9 (build 39): ####
* Every password field now has a 'reveal' button (using the QuickLook icon) that is actuated on mouse button press. This should go some way towards preventing erroneously entering the wrong password twice and thereby making your database inaccessible.

### 1.8 (build 38): ####
* Fixed a bug that could cause items freshly selected  in the outline to be overwritten with the last edited item on slower machines on macOS 10.7 (and possibly other OS versions as well). An earlier fix for this (in 1.7) caused changes to the attachment list to not be copied correctly - this has also been fixed.

### 1.7 (build 35): ####
* Restored (or actually attained) backwards compatibility with 10.7, functionally and esthetically (diverse UI tweaks so the app looks good on every system).

### 1.65 (build 33): ####
* Added a touch of vibrancy for users on macOS 10.10 and above.

### 1.6 (build 31): ####
* Finally succeeded in fixing an obscure bug (more like, found a workaround) that crashed the application when you tried to drag a product into a collapsed category in search mode when that category contained no items at that time (which is why I blocked collapsing categories in search mode). So, now you can freely collapse and expand your categories in search mode again. :)

### 1.5 (build 30): ####
A ton of bug fixes and improvements for this one:

* Visual feedback for wrong password now extends to _all_ places where you can enter a password;
* Bug fixed where you couldn't enter a subfolder when you wanted to add an application;
* Exporting the database now requires password entry if the database is password protected;
* CSV export now contains a column for the application category;
* Duplicating an item when a search is active now works correctly;
* Duplicating an item when a search is _not_ active now also works correctly :);
* You can now duplicate items by alt-dragging them inside the list...
* ...and edit the names of items and categories in the list itself by clicking on their labels.
* If your software database is password protected, you can choose to prevent dragging of entries from the software list to the Finder as an extra safety measure.
* User interface quirks fixed. You can now select an item in the software list from the contextual menu also if that item wasn't already selected - stupid, but it saves you a click, and it was weird you could delete and duplicate items from that spot, but not _select_ them. Also, for an application that goes out of its way to hang on to the currently selected item for as long as it can, it quite easily relinquished that selection if you contextual-menu-delete an item above it, so that's fixed as well.
* Creating duplicate files with the same name in Finder at the same location (e.g. by dragging items to the desktop) now produces sane file name sequences.

### 1.11 (build 27): ###
* Added visual feedback for when wrong password is entered.

### 1.1 (build 26): ###
* Added support for 10.14+ dark mode.

### 1.0 (build 25): ###
* Initial release.
